---
title: 'ZŠ Košická'
process:
    markdown: true
    twig: false
cache_enable: true
visible: true
---

![Logo základnej školy](kosicka.png?cropResize=120,120){.notfull}

Riaditeľka školy: Mgr. Renáta Karácsonyová - **Tel.:** 02/210 28 301<br>
Zástupkyňa riaditeľky školy pre ZŠ: RNDr. Cecília Gunišová - **Tel.:** 02/210 28 347<br>
Štat. zást. riad. školy pre národné programy: RNDr. Alena Oravcová - **Tel.:** 02/210 28 330

Fotogaleria z akcií ZŠ

[Vianočné vystúpenie v Ružinovskom domove seniorov](/o-skole/fotogalerie/rok-2015-2016/vianocne-vystupenie-v-ruzinovskom-domove-seniorov)

#### [Stránka prvého stupňa ZŠ - PYP SJ + fotogalérie](/aktivity-na-skole/prvy-stupen-zs-kosicka-pyp-sj)

[![](pypsjstrip.jpg)](/aktivity-na-skole/prvy-stupen-zs-kosicka-pyp-sj)

<hr>

[Zoznam potrebných pomôcok pre budúcich prváčikov](/o-skole/zs-kosicka/zoznam-potrebnych-pomocok-pre-buducich-prvacikov)<br>
[Stránka knižnice PYP](/aktivity-na-skole/kniznica-pyp)<br>
[Stránka Džbánu z Košickej](/aktivity-na-skole/dzban-jura-hronca)
