---
title: 'Zoznam potrebných pomôcok pre budúcich prváčikov'
---

![](tn02.jpg)
Vaše dieťa bude navštevovať 1.ročník na našej ZŠ. Pre jeho úspešnosť v práci prosíme zabezpečiť tieto pomôcky:

-	peračník, pastelky, strúhadlo, dve obyčajné ceruzky č.2, dve plniace perá, školské dosky na zošity, obaly na zošity (6 ks malé, 3 ks veľké, 1 ks na slovníček) 

Telesná výchova - športová obuv s bielou podrážkou, ponožky, cvičebný úbor podľa vlastného uváženia, švihadlo. Uložiť najlepšie do vaku so zaťahovacím otvorom. 

Výtvarná výchova – ochranné oblečenie /staré tričko/, igelitová podložka pod výkres na ochranu lavice, krycie vodové farby, temperové farby (malé 6 ks), voskové pastelky, plochý a okrúhly štetec č.12, nádoba na vodu so širokým dnom (napr. obal z rastlinného masla), lep biely-vysúvací, plastelína v igelitovom sáčku označ. menom, nožnice so zaobleným hrotom 

-	prezúvky pevné s bielou podrážkou (nie šľapky)
-	tekuté mydlo, uterák s uškom podpísaný, toaletný papier
-	textilný obrúsok na desiatovanie, fľaša na nápoj

Potrebné zošity a výkresy na celý školský rok zabezpečí triedna učiteľka Anna Vorelová.