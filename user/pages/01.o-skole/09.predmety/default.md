---
title: Predmety
process:
    markdown: true
    twig: false
cache_enable: true
visible: true
---

### Stránky predmetov

* [Matematika](/o-skole/predmety/matematika) - prijímacie pohovory
* [Slovenský jazyk](/o-skole/predmety/slovensky-jazyk) - slovenský jazyk na GJH a IB, materiály a pomôcky pre študentov IB, download
* [Anglický jazyk](/o-skole/predmety/anglicky-jazyk) - vstupný test, učebnice, doplnkový materiál, maturita z anglického jazyka, súťaže
* [Chémia](/o-skole/predmety/chemia) - periodická tabuľka, domáce pokusy, chémia každodenná, pre pokročilých, otestujte sa
* [Počítačové systémy](/o-skole/predmety/pocitacove-systemy) - seminár pre študentov 4. ročníka
* [Dejepis](/o-skole/predmety/dejepis) - stránky dejepisu
* [Náuka o spoločnosti](/o-skole/predmety/nauka-o-spolocnosti) - stránky náuky o spoločnosti
* [Fyzika](/o-skole/predmety/fyzika) - stránky fyziky

</br>
Stránky jednotlivých predmetov tvoria väčšinou žiaci a majú **samostatnú štruktúru**.
