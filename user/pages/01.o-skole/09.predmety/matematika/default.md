---
title: Matematika
---

## Matematika - prijímacie pohovory

Všetky testy sú uložené vo formáte PDF.

**Testy prijímacích pohovorov na štvorročné štúdium:**

* [Test z matematiky 2007](ps07.pdf)
* V rokoch 2008 až 2012 sa písomné prijímacie pohovory na štvorročné štúdium nekonali.
* [Test z matematiky 2013](ps13.pdf)
* [Test z matematiky 2014](PS_4rocne_2014.pdf)
* [Test z matematiky 2015](ps_15.pdf)


**Testy prijímacích pohovorov na päťročné bilingválne štúdium:**

* [Test z matematiky 2011](bs11.pdf)
* [Test z matematiky 2012](bs12.pdf)
* [Test z matematiky 2013](bs13.pdf)
* [Test z matematiky 2014](BS_5rocne_2014.pdf)
* [Test z matematiky 2015](ps_15.pdf)

**Testy prijímacích pohovorov do Prímy osemročného gymnázia:**

* [Test z matematiky 2011](prima11.pdf)
* [Test z matematiky 2012](prima12.pdf)
* [Test z matematiky 2013](prima13.pdf)
* [Test z matematiky 2014](Prima_8rocne_2014.pdf)
* [Test z matematiky 2015](prima_15.pdf)

Termíny a podrobnosti k tohtoročným pohovorom sa dozviete v časti [Pre záujemcov o štúdium](/o-skole/pre-zaujemcov-o-studium)
