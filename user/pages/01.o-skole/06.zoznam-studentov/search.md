---
title: 'Zoznam študentov'
process:
    markdown: true
    twig: false
cache_enable: true
visible: true
---

|Meno študenta|Trieda|
|--- |--- |
|Abdikhan Layla|IV.PYP AJ|
|Adam Dominik|II.B|
|Adamčík Daniel|Kvinta A|
|Adamčíková Katarína|IV.C|
|Adilipour Ariana|III.PYP AJ|
|Afanasyev Sergey|VI.A|
|Achberger Martin|Príma A|
|Alarzon Salas Maria José|IV.B|
|Alhuraibi Hesham|II.MYP|
|Alner Rachel|V.PYP AJ|
|Andráši Juraj|I.BC|
|Antalicová Viktória|II.C|
|Antoš Elizabeth|II.PYP AJ|
|Arthur Sophia|III.PYP AJ|
|Ashford Nicholas|II.PYP AJ|
|Ashford Jennifer|IV.PYP AJ|
|Austenová Nina|VII.A|
|Bab Dominika|III.PYP AJ|
|Babic Lukáš|III.MYP|
|Babic Darius|VI.A|
|Babušíková Zuzana|Kvarta A|
|Bača Richard|VI.A|
|Bača Richard|Príma A|
|Bačová Sofia|III.PYP SJ|
|Bachárová Júlia|II.A|
|Bacho Alexander|IV.D|
|Balaj Henrich Peter|I.BC|
|Balážik Jerguš|VI.A|
|Balažovič Matej|III.C|
|Baldovič Alexandra|III.PYP SJ|
|Baldovič Samuel|II.MYP|
|Balnová Nataša|II.PYP SJ|
|Balog Richard|III.PYP AJ|
|Balogová Tereza|I.MYP|
|Balogová Gabriela|VII.A|
|Balucha Filip|III.A|
|Balušíková Tereza|I.PYP AJ|
|Banetková Sara|I.MYP|
|Barčík Tobiáš|Kvarta A|
|Baričičová Jana|IV.A|
|Barilla Arne|VI.A|
|Barilla Arne|Príma A|
|Bartek Filip|Kvarta A|
|Bašťovanský Tomáš|Príma A|
|Baťka Gabriel|IV.IBDB|
|Batueva Erzhena|IV.B|
|Bdžochová Laura Tereza|IV.PYP SJ|
|Bečková Magdaléna|Septima A|
|Bednarčík Zvonimír|II.MYP|
|Bednarčíková Janka|III.IBDAB|
|Bednáriková Katarína|IV.D|
|Beliansky Patrik|III.C|
|Beličková Ema|Príma A|
|Belošičová Karolína|IV.C|
|Belovičová Karin|III.B|
|Beňa Ondrej|II.PYP SJ|
|Benáková Alexia Lucia|II.A|
|Benčík Benjamín|I.A|
|Beneš Ondrej|IV.D|
|Benetin Matúš Ján|IV.PYP SJ|
|Benkovičová Karolína|IV.IBDA|
|Bennett Kristína|I.PYP AJ|
|Beňo Adrian|II.A|
|Beňo Roman|IV.IBDA|
|Beňová Kristína|III.PYP SJ|
|Beňová Annamária|Sekunda A|
|Bernaťák Andrej|II.PYP AJ|
|Bertovičová Martina|II.A|
|Bezák Richard|Sekunda A|
|Bezák Miroslav|Septima A|
|Bezek Šimon|Septima A|
|Bezek Joshua Jan|III.PYP AJ|
|Bezeková Yumi Kela|V.PYP AJ|
|Bielik Filip|I.PYP SJ|
|Bieliková Alexandra|IX.A|
|Bichler Patricia Maria|I.MYP|
|Bichler Natalie|IV.MYP|
|Bíla Laura|VI.A|
|Bilavská Tereza|V.PYP SJ|
|Blahová Barbora|IV.MYP|
|Blanárik Juraj|VII.A|
|Blentič Sabina|V.A|
|Bobáľ Adam Jakub|IV.B|
|Boďa Tomáš|I.A|
|Bodnár Maxim|Tercia A|
|Boháčová Barbora|IV.B|
|Bojnanská Júlia|III.PYP SJ|
|Bojnanský Jakub|VI.A|
|Bokes Jakub|IV.IBDA|
|Boledovičová Nina|Sekunda A|
|Bolerác Dominik|V.A|
|Borbély Henrich|I.A|
|Bordácsová Viktória|IV.A|
|Borisová Nina|VI.A|
|Borovská Simona|III.A|
|Borovská Adriana|Sekunda A|
|Borovský Matej|III.PYP SJ|
|Borská Fiiona|III.IBDAB|
|Borská Adina|II.PYP SJ|
|Borská Sára|I.PYP SJ|
