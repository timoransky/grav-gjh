---
title: 'Zmluvy, objednávky, faktúry'
process:
    markdown: true
    twig: false
cache_enable: true
visible: true
---

## Faktúry, objednávky, výzvy

### Výzvy na predkladanie ponúk - podprahové zákazky

* [Kancelárske potreby](zakazka10kancpotreby.pdf) (.pdf)
* [Čistiace prostriedky](zakazka9cistprostr.pdf) (.pdf)
* [Špeciálne čistiace prostriedky](zakazka8cistprostr.pdf) (.pdf)
* [Oprava kanalizácie](zakazka7kanalizacia.pdf) (.pdf)
* [Vstavané skrine v triedach](zakazka6skrine.pdf) (.pdf)
* [Výmena filtrov vo vzduchotechnike](zakazka5filtre.pdf) (.pdf)
* [Schránky](zakazka3schranky.pdf) (.pdf)
* [Plávajúca podlaha](zakazka3podlaha.pdf) (.pdf)
* [Jedálenské stoly](zakazka2.pdf) (.pdf)
* [Oprava strechy](vyzva.pdf) (.pdf)

* [Kniha došlých faktúr za rok 2016](KDF2016.xlsx) (.xlsx)
* [Kniha objednávok za rok 2016](KO2016.xlsx) (.xlsx)

* [Kniha došlých faktúr za rok 2015](KDF2015.xlsx) (.xlsx)
* [Kniha objednávok za rok 2015](KO2015.xlsx) (.xlsx)

* [Faktúry - jedáleň](faktury-jedalen.xls) (.xls)</br></br>


### Súhrnné správy - zákazky s nízkou hodnotou nad 1000,- €

* [Prvý kvartál 2014](sprava1314-1q.pdf) (.pdf)
* [Druhý kvartál 2014](sprava1314-2q.pdf) (.pdf)
* [Tretí kvartál 2014](zakazky2014-3q.pdf) (.pdf)
* [Štvrtý kvartál 2014](zakazky2014-4q.pdf) (.pdf)</br></br>


### Staršie

* [Kniha došlých faktúr za rok 2014](KDF2014.pdf) (.pdf)
* [Kniha objednávok za rok 2014](KO2014.pdf) (.pdf)
* [Kniha došlých faktúr za rok 2013](KDF2013.pdf) (.pdf)
* [Kniha objednávok za rok 2013](KO2013.pdf) (.pdf)
* [Kniha došlých faktúr za rok 2012](kdf2012.pdf) (.pdf)</br></br>

## Zmluvy
Všetky novšie zmluvy sú zverejnené v [centrálnom registri zmlúv](http://http://www.crz.gov.sk/).

* [Zmluvy z roku 2011](/o-skole/zmluvy-objednavky-faktury/zmluvy)
