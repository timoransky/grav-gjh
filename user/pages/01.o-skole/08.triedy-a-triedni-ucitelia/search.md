---
title: 'Triedy a triedni učitelia'
process:
    markdown: true
    twig: false
cache_enable: true
visible: true
---

Prvá cifra čísla učebne znamená poschodie, na ktorom sa daná učebňa nachádza (Pozrite si [plán budovy](/informacie/plan-budovy)).<br>
Rozvrhy jednotlivých tried, ale aj učební nájdete v časti [Rozvrhy](http://ssnovohradska.edupage.org/timetable/?).
