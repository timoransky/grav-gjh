---
title: 'Vianočné vystúpenie v Ružinovskom domove seniorov'
date: '01-12-2015 21:59'
---

Dňa 17. decembra 2015 boli naši žiaci vystupovať pre obyvateľov Ružinovského domova seniorov na Pivonkovej ulici. Predviedli veľmi pekný a milý, vianočne ladený program. Prednášali, spievali, tancovali .. zožali veľký potlesk a niektoré babičky i deduškovia si s našimi deťmi aj zaspievali a pri záverečnom čísle - keď sa spievala Tichá noc, ktorá bola doprevádzaná na flautách, padali od dojatia aj slzy.