---
title: Fotogalérie
dateformat: 'd-m-Y H:i'
process:
    markdown: true
    twig: false
child_type: default
cache_enable: true
visible: true
---

## Zo života našej školy

### Fotogalérie z podujatí a exkurzií, ktorými žije GJH

**Ďalšie stránky z fotogalériami zo školy:**

+ [Akcie PYP SJ (1.stupeň)](/aktivity-na-skole/prvy-stupen-zs-kosicka-pyp-sj)
+ [Fotogaléria PYP AJ (na PYP stránke)](https://www.gjh.sk/pyp/?page_id=155)
+ [Fotogaléria MYP (na MYP stránke)](https://www.gjh.sk/myp/index.php?page=albumy)
+ [Fotogaléria IBD (na stránke IBD)](https://ib.gjh.sk/en/fotogaleria/)
</br>

