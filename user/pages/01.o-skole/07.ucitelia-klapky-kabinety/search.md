---
title: 'Učitelia, klapky, kabinety'
body_classes: ucitelia
process:
    markdown: true
    twig: false
cache_enable: true
visible: true
---

* Každý učiteľ má e-mailovú adresu v tvare priezviskoučiteľa@gjh.sk
* [Telefónny zoznam 2015/2016 na stiahnutie vo formáte PDF](klapky1516.pdf)
* Priame čísla do kabinetov: 02/210 28 xxx, kde xxx je klapka

| Meno                 | Kabinet              | Poschodie | Klapka |
|----------------------|----------------------|-----------|--------|
| Ambrózy Radko        | GEG                  | 3p-GJH    | 342    |
| Bakoš Igor           | TV                   | Príz-ZŠ   | 317    |
| Barančoková Zuzana   | AJ3-ŠVL              | 2p-GJH    | 329    |
| Belajová Andrea      | PYP SJ               | 2p-ZŠ     | 332    |
| Bobáková Tamara      | IBD-Extern.          | 3p-GJH    | 338    |
| Boboková Ivana       | NEJ-Extern.          | 1p-GJH    | 321    |
| Boboš Alena          | SJ                   | 3p-ZŠ     | 339    |
| Bodová Beáta         | Recepcia             | Príz.     | 300    |
| Bojnanská Ivana      | FYZ                  | 4p-ZŠ     | 354    |
| Bučková Alena        | CHE                  | 1p-GJH    | 320    |
| Butler Zuzana        | PYP                  | 1p-ZŠ     | 325    |
| Carter Alec          | AJ3-ŠVL              | 2p-GJH    | 329    |
| Cikatricisová Hana   | CHE                  | 1p-ZŠ     | 322    |
| Crmoman Vladimír     | Music                | Príz-ZŠ   | 361    |
| Crmomanová Jana      | PYP-knižnica         | Príz-ZŠ   | 316    |
| Čanigová Nikola      | BIO-Extern.          | 4p-GJH    | 351    |
| Čapuchová Andrea     | PSYCH                | 2p-GJH    | 318    |
| Čarnoká Andrea       | NEJ                  | 1p-GJH    | 321    |
| Černá Miriam         | INF-ŠVL              | 2p-GJH    | 327    |
| Čisláková Helena     | AJ1                  | 2p-ZŠ     | 331    |
| Deák Radoslav        | MYP                  | 3p-GJH    | 337    |
| Deáková Zuzana       | AJ1                  | 2p-ZŠ     | 331    |
| Demáček Ondrej       | INF-ŠVL              | 2p-GJH    | 327    |
| Dielne DT            | Dielne               | Príz-ZŠ   | 348    |
| Dirbáková Anna       | SJ1                  | 4p-ZŠ     | 352    |
| Ditte Lenka          | BIO                  | 4p-GJH    | 351    |
| Dugovičová Andrea    | Tajomníčka           | 2p-GJH    | 303    |
| Eližerová Viera      | FYZ                  | 3p-GJH    | 335    |
| Filová Mária         | FYZ                  | 4p-ZŠ     | 354    |
| Fischerová Katarína  | NEJ                  | 1p-GJH    | 321    |
| Fraasová Jana        | INF-ŠVL              | 2p-GJH    | 327    |
| Frandelová Dagmar    | ŠKD                  | Príz-ZŠ   | 315    |
| Gabíková Jana        | Zástup. ekonom.      | 2p-GJH    | 304    |
| Gonda Matej          | IBD                  | 3p-GJH    | 338    |
| Grečnerová Klára     | MAT                  | 4p-GJH    | 349    |
| Gunišová Cecília     | Zástupcovňa          | 2p-ZŠ     | 347    |
| Gunišová Katarína    | PYP-druž.            | Príz-ZŠ   | 309    |
| Hajdúchová Katarína  | Ekonom.              | 2p-GJH    | 302    |
| Hanulová Eva         | INF-ŠVL              | 2p-GJH    | 327    |
| Haško Milan          | TV                   | Príz-GJH  | 312    |
| Helebrandtová Viera  | Tajomn.              | 2p-ZŠ     | 308    |
| Horváthová Renáta    | TV                   | Príz-GJH  | 312    |
| Hrapková Marcela     | SJ1                  | 4p-ZŠ     | 352    |
| Hrbatá Alena         | PYP SJ               | 2p-ZŠ     | 332    |
| Hrobár Michal        | Strojovňa-ŠVL-Extern | 2p-GJH    | 346    |
| Hrušovská Zuzana     | AJ1                  | 2p-ZŠ     | 331    |
| Chalupková Soňa      | FYZ                  | 3p-GJH    | 336    |
| Chlebová Zuzana      | VV-ZŠ-Extern         | 3p-ZŠ     | 344    |
| Jamborová Jarmila    | AJ2                  | 3p-GJH    | 340    |
| Jonnerová Simona     | NEJ                  | 1p-GJH    | 321    |
| Joyce Chris          | PYP                  | 1p-ZŠ     | 325    |
| Joyce Zita           | PYP-koor.            | 1p-ZŠ     | 323    |
| Justus Marián        | AJ1                  | 2p-ZŠ     | 331    |
| Kabátová Žakelína    | AJ2                  | 3p-GJH    | 340    |
| Kalašová Elena       | MAT                  | 4p-GJH    | 349    |
| Karácsonyová Renáta  | Zástupcovňa          | 2p-ZŠ     | 341    |
| Kasáková Alžbeta     | vedúca ŠJ            | Príz-GJH  | 324    |
| Kaščáková Aneta      | BIO                  | 4p-GJH    | 351    |
| Keim Michal          | IBD-Extern.          | 3p-GJH    | 338    |
| Kmeť Miloš           | ART                  | Príz-ZŠ   | 334    |
| Kolníková Blanka     | IBD                  | 3p-GJH    | 338    |
| Komada Andrej        | BIO                  | 4p-GJH    | 351    |
| Komadel Ján          | IBD-Extern.          | 3p-GJH    | 338    |
| Koňanová Monika      | Tajomníčka           | 2p-GJH    | 303    |
| Koplíková Lenka      | Strojovňa-ŠVL        | 2p-GJH    | 328    |
| Kosper František     | MAT                  | 4p-GJH    | 349    |
| Košút Ľuboš          | CHE                  | 1p-ZŠ     | 362    |
| Kraicová Xénia       | ŠKD                  | Príz-ZŠ   | 315    |
| Králiková Ľubica     | DEJ                  | 2p-GJH    | 326    |
| Križanová Mária      | BIO                  | 4p-GJH    | 351    |
| Kubla Tomáš          | Strojovňa-ŠVL        | 2p-GJH    | 328    |
| Kurian Matúš         | IBD-Extern.          | 3p-GJH    | 338    |
| Kusá Daniela         | IBD-Extern.          | 3p-GJH    | 338    |
| Ľahká Dagmar         | NEJ                  | 1p-GJH    | 321    |
| Lampartová Terézia   | SJ1                  | 4p-ZŠ     | 352    |
| Lanátor Ľubomír      | IBD                  | 3p-GJH    | 338    |
| Lauček Roman         | Hospodár             | 2p-ZŠ     | 314    |
| Laučeková Danuša     | Recepcia             | Príz.     | 300    |
| Letanovská Ľubica    | FYZ                  | 3p-GJH    | 336    |
| Lindbloom Tor        | AJ3-ŠVL              | 2p-GJH    | 329    |
| Lysá Bronislava      | PYP-druž.            | Príz-ZŠ   | 309    |
| Malík Michal         | CHE                  | 1p-ZŠ     | 322    |
| Markusová Gabriela   | MYP-koor.            | 3p-GJH    | 337    |
| Mayer Ján            | NEJ                  | 1p-GJH    | 321    |
| Michalka Vladimír    | INF-ŠVL-Extern.      | 2p-GJH    | 327    |
| Milán Patrik         | Strojovňa-ŠVL        | 2p-GJH    | 328    |
| Miler Sonja          | AJ3-ŠVL              | 2p-GJH    | 329    |
| Mináriková Zuzana    | AJ2                  | 3p-GJH    | 340    |
| Mlynarčíková Hana    | DEJ                  | 2p-GJH    | 326    |
| Molnárová Barbara    | MYP                  | 3p-GJH    | 337    |
| Molnárová Ľudmila    | AJ2                  | 3p-GJH    | 340    |
| Monošíková Renáta    | CHE                  | 1p-GJH    | 320    |
| Munková Zuzana       | Riaditeľka           | 2p-GJH    | 301    |
| Navrátil Miroslav    | INF-ŠVL-Extern.      | 2p-GJH    | 327    |
| Nemčoková Renáta     | PSYCH                | 2p-GJH    | 333    |
| Okasová Barbora      | ART                  | Príz-ZŠ   | 334    |
| Olosová Gabriela     | TEV                  | Príz-GJH  | 312    |
| Oravcová Alena       | Zástupcovňa          | 2p-ZŠ     | 330    |
| Oreničová Hana       | PYP-druž.            | Príz-ZŠ   | 309    |
| Pätoprstá Nadežda    | FYZ-ZŠ               | 4p-ZŠ     | 354    |
| Patúcová Katarína    | PYP                  | 1p-ZŠ     | 325    |
| Pecková Beáta        | SJ                   | 3p-GJH    | 339    |
| Pochová Soňa         | PYP SJ               | 2p-ZŠ     | 332    |
| Polonyová Tatiana    | SJ2                  | 4p-ZŠ     | 353    |
| Pravotiak Klement    | TV                   | Príz-GJH  | 312    |
| Púdelková Monika     | SJ                   | 3p-GJH    | 339    |
| Pumová Jana          | ŠKD                  | Príz-ZŠ   | 315    |
| Rázusová Zuzana      | TV                   | Príz-ZŠ   | 317    |
| Salíniová Soňa       | Knižnica             | Príz-GJH  | 310    |
| Sapák Matej          | IBD-Extern.          | 3p-GJH    | 338    |
| Seitsinger Travis    | PYP                  | 1p-ZŠ     | 325    |
| Simonová Diana       | FYZ                  | 3p-GJH    | 336    |
| Sirotná Darja        | SJ2                  | 4p-ZŠ     | 353    |
| Slezák Tomáš         | DEJ                  | 2p-GJH    | 326    |
| Slušný Marián        | BIO                  | 4p-GJH    | 350    |
| Sontágová Jana       | PYP-druž.            | Príz-ZŠ   | 309    |
| Starovičová Katarína | PSYCH-Extern.        | 2p-GJH    | 333    |
| Steyne Lynda         | IBD                  | 3p-GJH    | 338    |
| Surgošová Lucia      | PYP                  | 1p-ZŠ     | 325    |
| Sviteková Michaela   | PYP                  | 1p-ZŠ     | 325    |
| Špačeková Anna       | CHE                  | 1p-GJH    | 320    |
| Šťastný Eduard       | Školník              | Príz-ZŠ   | 319    |
| Štefániková Dana     | ŠKD                  | Príz-ZŠ   | 315    |
| Tarasovič Zdenko     | FYZ-Extern.          | 3p-GJH    | 336    |
| Trebatická Mária     | VV-HV                | 3p-ZŠ     | 344    |
| Trupová Inéz         | PYP SJ               | 2p-ZŠ     | 332    |
| Uhríková Simona      | AJ1                  | 2p-ZŠ     | 331    |
| Urbašíková Miroslava | FYZ                  | 3p-GJH    | 335    |
| Vajdová Eva          | ŠJ                   | Príz-GJH  | 313    |
| Valášková Zuzana     | MAT                  | 3p-ZŠ     | 343    |
| Vališová Michaela    | FYZ                  | 3p-GJH    | 336    |
| Varga Tomáš          | AJ3-ŠVL              | 2p-GJH    | 329    |
| Veselý Jakub         | Strojovňa-ŠVL        | 2p-GJH    | 328    |
| Vicenová Alena       | SJ                   | 3p-GJH    | 339    |
| Vorelová Anna        | PYP SJ               | 2p-ZŠ     | 332    |
| Výbošťok Jaroslav    | INF-ŠVL              | 2p-GJH    | 327    |
| Záhorská Andrea      | FYZ                  | 4p-ZŠ     | 354    |
| Zajacová Ivana       | NEJ                  | 1p-GJH    | 321    |
| Zátorský Marek       | ekonóm               | 2p-ZŠ     | 307    |
| Zorkócy Dušan        | MAT                  | 4p-GJH    | 349    |
| Zverinová Alena      | PYP-druž.            | Príz-ZŠ   | 309    |
| Žingorová Iva        | PYP                  | 1p-ZŠ     | 325    |
| Žitná Eva            | zást. medzinár. šk.  | 2p-GJH    | 305    |