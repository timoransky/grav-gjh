---
title: Kontakt
process:
    markdown: true
    twig: false
cache_enable: true
visible: true
---

**Spojená škola** </br> 
Novohradská 3 </br>
821 09 Bratislava </br>

**Tel**: 02/210 28 300 (vrátnica - spojovateľ)

### Fakturačné údaje
**Obchodné meno**: Spojená škola </br>
**IČO**: 36075213 </br>
**DIČ**: 2021922452 </br>

[Ako sa dostať na SŠ Novohradská?](/informacie/ako-sa-k-nam-dostat)

### Vedenie
Riaditeľka spojenej školy Mgr. Renáta Karácsonyová - 02/210 28 301 - karacsonyova@gjh.sk </br>

Štatutárna zástupkyňa riaditeľky pre programy v slovenskom jazyku a zástupkyňa pre osemročné štúdium RNDr. Alena Oravcová - 02/210 28 330 - oravcova@gjh.sk </br>

Zástupkyňa riaditeľky pre základnú školu RNDr. Cecília Gunišová - 02/210 28 347 - gunisova@gjh.sk </br>

Štatutárna zástupkyňa riaditeľky pre programy v anglickom jazyku PhDr. Eva Žitná - 02/210 28 305 - zitna@gjh.sk </br>

Zástupkyňa pre štvorročné a päťročné štúdium Mgr. Renáta Karácsonyová - 02/210 28 301 - karacsonyova@gjh.sk </br>

### Úradné hodiny pre verejnosť

Riaditeľka školy: pondelok 8:00 – 9:30; streda 15:00 – 16:30 </br>
Zástupkyňa pre programy v anglickom jazyku: utorok 15:00 – 16:30 </br>

### Učitelia

Do jednotlivých kabinetov sa dá dovolať priamo na číslo 02/210 28 xxx, kde xxx je klapka. Pozrite si [zoznam klapiek](/o-skole/ucitelia-klapky-kabinety). </br>
Každý učiteľ má e-mailovú adresu v tvare priezviskoucitela@gjh.sk

### Ďalšie kontakty

Spojovateľ - 02/210 28 300</br>

[Školské psychologičky Andrea Čapuchová a Renata Nemčoková](/informacie/sluzby-psychologov)</br>
Tajomníčka Andrea Dugovičová - 02/210 28 303</br>
Správca [Nadácie Novohradská](/o-skole/nadacia-novohradska) František Kosper - kosper@gjh.sk, 02 / 210 28 349</br>
Odhlasovanie z obedov - Eva Vajdová - 02/210 28 313</br>
Vedúca školskej jedálne Alžbeta Kasáková - 02/210 28 324</br>

Správca webstránky GJH Jaroslav Výbošťok - webstranka@gjh.sk</br>
Správca [Internetovej Žiackej Knižky](https://ssnovohradska.edupage.org/login/?) Ľubomír Košút - kosut@gjh.sk</br>