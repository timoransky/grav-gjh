---
title: 'Nadácia Novohradská'
process:
    markdown: true
    twig: false
cache_enable: true
visible: true
---

## Nadácia Novohradská

* [Ihrisko](#chapter1)
* [Informácia pre rodičov na začiatku školského roka 2015/2016](#chapter2)
* [Údaje na poukázanie 2% z Vašich daní](#chapter3)
* [Dokumenty na poukázanie 2% z Vašich daní](#chapter4)
* [Špecifikácia použitia (2%) podielu zaplatenej dane](#chapter5)
* [Dokumenty týkajúce sa Nadácie Novohradská](#chapter6)
* [Kontakt a bankové spojenie na Nadáciu Novohradská](#chapter7)

### IHRISKO ### {#chapter1}

**Verejná zbierka**

Názov zbierky: ,,Školský dvor Novohradská“</br>
Organizátor: Nadácia Novohradská</br>
Registrové číslo zbierky: 101-2016-051214</br>
Spôsoby vykonávania:</br>
11.05.2016 - 15.10.2016 zasielaním príspevkov na osobitný účet</br>
11.05.2016 - 24.05.2016 zbieraním do prenosných pokladničiek</br>
11.05.2016 - 15.10.2016 predajom predmetov, ak je príspevok zahrnutý v ich cene</br>
11.05.2016 - 15.10.2016 predajom vstupeniek na kultúrne podujatia, športové podujatia alebo iné verejné podujatia usporadúvané na získanie príspevkov, ak je príspevok zahrnutý v cene vstupeniek</br>
Bankové spojenie:</br>
Číslo účtu IBAN: SK17 0900 0000 0051 1229 9628</br>

---

**Výberové konanie - Školský dvor**
(zverejnené 5.5.2016)

Vážení uchádzači, na žiadosť viacerých potenciálnych uchádzačov sme lehotu predkladania ponúk predĺžili do stredy 18. mája 2016 24:00. Nadácia Novohradská

[Zadanie výberového konania](tender-zadanie.pdf) (.pdf)</br>
[Materiály k zadaniu na stiahnutie](zadanie.rar) (.rar)
___

Bratislava, 5. 11. 2015</br>
Vážení rodičia, milí žiaci, absolventi a priatelia GJH a ZŠ Košická,</br>

náš školský dvor dlhodobo chátra, jeho stav je nebezpečný, používania neschopný. Žiaci našej školy nemajú kde pestovať atletické disciplíny, deťom v školskom klube detí (školskej družine) chýba priestor na pohybové hry.</br>

Stav areálu korešponduje s dlhodobým súdnym sporom o vlastníctvo pozemku školského dvora medzi štátom a súčasným majiteľom. V tejto situácii škola nemôže do úpravy dvora investovať zo svojho rozpočtu ani cent.</br>

Vďaka iniciatívnej pomoci rodičov sa Nadácii Novohradská podarilo podpísať so súčasným majiteľom zmluvu o prenájme pozemku školského dvora na dobu 14 rokov za symbolické 1 €. To Nadácii otvorilo možnosť začať plánovať jeho rekonštrukciu. Náklady na finálny projekt a prvú etapu rekonštrukcie (atletická dráha, multifunkčné ihrisko a doskočisko) budú podľa predbežného prieskumu trhu cca 196 000 €.</br>

Ak máte záujem a možnosť podporiť finančne túto iniciatívu, budú Vám žiaci aj učitelia školy veľmi vďační. Na tejto stránke máte možnosť priamo prispieť na rekonštrukciu dvora bezhotovostným prevodom sumy, ktorej výšku si sami zvolíte.</br>

Vopred Vám ďakujeme za akýkoľvek finančný dar, účelovo viazaný na rekonštrukciu školského dvora.</br>

RNDr. Kosper František</br>
správca nadácie</br>
kosper@gjh.sk</br>
02 / 210 28 349</br>
___

Absolventi, rodičia, študenti: 95 288 € (spolu 640 darcov, priemerná výška daru: 149 €)</br>
Vianočné a Veľkonočné detské trhy: 2 003 € </br>
Zbierka: Školský dvor Novohradská: 24 604 € </br>
Právnické osoby:	75 470 € (spolu 21 darcov) </br>
Nadácia Novohradská:	15 000 € (prostriedky zo základného imania nadácie z roku 2004) </br>
**Pomohli ste už sumou: 212 365 €         Ďakujeme! **</br>

15.8.2016</br>
___

Pre darcov, ktorí sa rozhodli prispieť priamo na účet nadácie poskytujeme nasledujúce údaje:

|                                     |                               |
|------------------------------------|------------------------------|
| Bankové spojenie       | Slovenská sporiteľňa, a. s.      |
| Názov účtu             | Nadácia Novohradská              |
| Číslo účtu             | 1146 9293 / 0900                 |
| IBAN                   | SK21 0900 0000 0000 1146 9293    |
| KS                     | 0308                             |
| Var. symbol            | 2016577                          |
| Správa pre prijímateľa | Ihrisko, Meno a Priezvisko darcu |

---

### Informácia pre rodičov na začiatku školského roka 2015/2016 ### {#chapter2}
Bratislava, 17. 9. 2015</br>

Vážení rodičia,</br>
v prvom rade ďakujeme všetkým rodičom za všetky doteraz poskytnuté príspevky a dary pre našu školu prostredníctvom nadácie. Tak isto ďakujeme aj všetkým absolventom, sponzorom a nadšencom Gymnázia Jura Hronca a ZŠ Košická, ktorí tiež podporili Nadáciu Novohradská. Teší nás, že celkovo minulý rok nám darovalo viacej rodičov ako v predchádzajúcich rokoch. Celkový prehľad nákladov a výnosov nadácie za kalendárny rok 2013 si môžete pozrieť vo Výročnej správe za rok 2014 na webovej stránke nadácie. Výdavky nadácie tvoria priemerne okolo 7 % z celkových nákladov školy (v prílohe je uvedený graf). Napriek tomu získané prostriedky nadácie sú potrebné na udržanie kvality vzdelávania a nadštandardných aktivít žiakov školy.</br>

Na akékoľvek otázky, nejasnosti Vám na uvedených kontaktoch ochotne odpovieme.</br>

Na začiatku školského roku Vám prajeme, aby sa Vaša dcéra, syn cítil(a) na našej škole dobre a hlavne prvákom prajeme úspešný štart na našej škole.</br>

[Informácia pre rodičov na začiatku školského roka 2015/2016](inforodicia1516.pdf) (.pdf)</br>

RNDr. Kosper František</br> 
správca nadácie</br>
kosper@gjh.sk</br>
02 / 210 28 349</br>
</br>
Mgr. Karácsonyová Renáta</br>
riaditeľka školy</br>
riaditel@gjh.sk </br>
02 / 210 28 301</br>

|                                     |                               |
|:------------------------------------|:------------------------------|
| Bankové spojenie:                   | Slovenská sporiteľňa, a. s.      |
| Názov účtu:                         | Nadácia Novohradská              |
| Číslo účtu (národné programy):      | 1146 9293 / 0900                 |
| IBAN:                               | SK21 0900 0000 0000 1146 9293    |
| Číslo účtu (medzinárodné programy): | 6322 05012 / 0900                |
| IBAN:                               | SK84 0900 0000 0006 3220 5012    |
| KS:                                 | 0308                             |
| Var. symbol:                        | ddmmrrrr (dátum narodenia žiaka) |
| Správa pre prijímateľa:             | Priezvisko a trieda žiaka        |

Dokumenty nadácie: Výročnú správu, Rozpočet nadácie, Použitie 2%, Audit učtovníctva, Audit použitia 2% a Nadačnú listinu si môžete pozrieť nižšie na tejto stránke.

---

### Údaje na poukázanie 2% z Vašich daní ### {#chapter3}

Vážení rodičia a priatelia GJH a ZŠ Košická!</br>

Oznamujeme Vám, že dňa 15. 1. 2016 bola Nadácia Novohradská zaradená do zoznamu prijímateľov 2% na rok 2015. Ďakujeme všetkým, ktorí ste v predchádzajúcich obdobiach pomohli našej nadácii podielom zo svojej zaplatenej dane.
Všetci naši žiaci a učitelia budú veľmi radi, keď aj v tomto roku pomôžete našej škole poukázaním 2% Nadácii Novohradská.</br>

_Údaje pre právnické osoby a fyzické osoby, ktoré si sami podávajú daňové priznanie. Termín zvyčajne do **31.3.2016**:_</br>

IČO: **30812941**</br>
Právna forma: **nadácia**</br>
Obchodné meno: **Nadácia Novohradská** </br>

Sídlo:</br>
Ulica: **Novohradská**</br> 
Číslo: **3**</br>
PSČ: **821 09**	</br>
Obec: **Bratislava**</br>

Všetky **tlačivá** a informácie sú k dispozícií na tejto stránke. Na požiadanie (kosper@gjh.sk) Vám ich ochotne pošleme mailom alebo poštou. </br>

---

### Dokumenty na poukázanie 2% z Vašich daní ### {#chapter4}

_Údaje pre zamestnancov, ktorí si **nepodávajú** daňové priznanie. Termín do **30.4.2016**._

Na poukázanie 2 % sú potrebné tieto dve tlačivá:

[Vyhlásenie](vyhlasenie2015.pdf) (.pdf) zverejnené finančnou správou </br>
[Vyhlásenie](vyhlasenie2015_stare.pdf) (.pdf) editovateľné 2015 verzia 1</br>
[Potvrdenie o zaplatení dane](potvrdenie2015.doc) (.doc)

Kompletné Informácie ohľadne 2% Vám úplne poskytne aj: [www.rozhodni.sk](http://www.rozhodni.sk) ,  [www.dvepercenta.sk](http://www.dvepercenta.sk)

---

### Špecifikácia použitia (2%) podielu zaplatenej dane ### {#chapter5}

Špecifikácia použitia podielu zaplatenej dane z príjmov fyzických a právnických osôb, ktoré Nadácia Novohradská prijala je uverejnená v:
* [Obchodnom vestníku OV 98 / 2015](OV_98_2015.pdf) (.pdf)
* [Obchodnom vestníku OV 94 / 2014](OV_94_2014.pdf) (.pdf)
* [Obchodnom vestníku OV 101 / 2013](OV_101_2013.pdf) (.pdf)
* [Obchodnom vestníku OV 93 / 2012](OV93_2012.pdf) (.pdf)
* [Obchodnom vestníku OV 102C / 2011](OV_102C_2011.pdf) (.pdf)
* [Obchodnom vestníku OV 102A / 2010](OV-102A-2010.pdf) (.pdf)

---

### Dokumenty týkajúce sa Nadácie Novohradská ### {chapter6}

[Nadačná listina Nadácie Novohradská](Nadacna_listina_2013.pdf) (.pdf)</br>
[Rozpočet nadácie na rok 2015](rozpocet2015.pdf) (.pdf)

[Výročná správa nadácie za rok 2014](vyrocnasprava2014.pdf) (.pdf)</br>
[Výrok audítora k účtovnej závierke](vyrokauditoraUZ2014.pdf) (.pdf)

[Výročná správa nadácie za rok 2013](vyrocnasprava2013.pdf) (.pdf) </br>
[Výrok audítora k účtovnej závierke uverejnenej v OV 99/2014](OV_99_2014.pdf) (.pdf) </br>
[Výročná správa nadácie za rok 2012](vyrocnasprava2012.pdf) (.pdf) </br>
[Výrok audítora k účtovnej závierke uverejnenej v OV 100/2013](OV_100_2013.pdf) (.pdf) </br>
[Výročná správa nadácie za rok 2011](vyrocnasprava2011.pdf) (.pdf) </br>
[Výrok audítora k účtovnej závierke uverejnenej v OV 103/2012](OV103_2012.pdf) (.pdf) </br>
[Výročná správa nadácie za rok 2010](vyrocnasprava2010.pdf) (.pdf) </br>
[Výrok audítora k účtovnej závierke uverejnenej v OV 103D/2011](OV_103D_2011.pdf) (.pdf) </br>

---

### Kontakt a bankové spojenie na Nadáciu Novohradská ### {#chapter7}

**Názov nadácie:** Nadácia Novohradská </br>
**Sídlo nadácie:** Novohradská 3, 821 09 Bratislava </br>
**IČO:** 30812941 </br>
**DIČ:** 2020858455 </br>
**Tel.:** 02 / 210 28 349 </br>
**E-mail:** kosper@gjh.sk </br>

|                                     |                               |
|:------------------------------------|:------------------------------|
| Bankové spojenie:                   | Slovenská sporiteľňa, a. s.   |
| Názov účtu:                         | Nadácia Novohradská           |
| Číslo účtu (národné programy):      | 1146 9293 / 0900              |
| Číslo účtu (medzinárodné programy): | 6322 05012 / 0900             |
| SWIFT:                              | GIBASKBX                      |
| IBAN:                               | SK21 0900 0000 0000 1146 9293 |






