---
title: 'Pre záujemcov o štúdium'
process:
    markdown: true
    twig: false
cache_enable: true
visible: true
---

### INFORMÁCIE O PRIJÍMANÍ NA ŠTÚDIUM

[Informácie o prijímaní na Spojenú školu na školský rok 2016/2017](SSinfo2015.pdf) (.pdf)

Dokument obsahuje počty žiakov, ktoré plánujeme prijať na jednotlivé sekcie medzinárodného a národného programu a termíny prijímacích a výberových konaní.

[Prijímanie do prvého ročníka PYP AJ (anglický program PYP)](https://www.gjh.sk/pyp/?page_id=23)

Ukážky testov z prijímacích skúšok na gymnázium z minulých rokov: [MAT](/o-skole/predmety/matematika), [SJ](/o-skole/predmety/slovensky-jazyk)