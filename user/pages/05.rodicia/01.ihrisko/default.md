---
title: Ihrisko
process:
    markdown: true
    twig: false
cache_enable: true
visible: true
---

## Školské ihrisko

### GJH a ZŠ Košická potrebujú ihrisko!


[Leták s informáciami pre rodičov ohľadom zbierky na školské ihrisko](ihrisko-letak-rodicia.pdf) (.pdf)

[Sedem slajdov o stave ihriska, zbierke, plánoch, pomoci ](ihrisko-prezent.pdf) (pdf prezentácia)

![](ihrisko.png)
</br>
___
</br>
[Často kladené otázky](ihrisko-faq.pdf) (.pdf)

Prečo to nezaplatí mesto? Prečo prispieť práve teraz? Kedy bude ihrisko použiteľné? Kam presne pôjdu peniaze? Budú peniaze na údržbu? Bude tam aj plot? Bude tam aj parkovisko? Kto bude majiteľom?
</br>
___
</br>
Dňa 5. novembra 2015 sme začali zbierku peňazí na rekonštrukciu ihriska, ktoré dlhodobo chátra kvôli sporom o vlastníctvo pozemku. Prekonali sme už dva míľniky – máme peniaze na architekta, stavebné povolenie a prípravné práce na ihrisku. Ihrisková komisia z predložených architektonických ponúk výberovým procesom vybrala ateliér renomovaného architekta pána ing. arch. Petra Moravčíka (PMarchitekti). Nadácia s ním podpísala zmluvu o dielo. Ďakujeme všetkým doterajším darcom – rodičom, absolventom a firmám.

Ešte stále sme však na začiatku, a preto Vás prosíme o peňažný príspevok, aby sme mohli v máji vybrať stavebnú firmu a v lete sa pustiť do stavby. Škola nemôže do úpravy dvora investovať zo svojho rozpočtu žiadne peniaze. Zatiaľ sa podarilo získať dvoch firemných sponzorov a my dúfame, že sa nám podarí získať aj ďalších. ak máte firmu, ktorá sa môže stať sponzorom, veľmi to uvítame. Veríme ale, že najviac to leží na srdci Vám, rodičom našich detí, a preto – Pomôžte!

Náklady na projekt a prvú etapu rekonštrukcie (atletická dráha, multifunkčné ihrisko a doskočisko) budú podľa predbežného prieskumu trhu cca 196000 €. **Peniaze môžete venovať cez internetovú pokladničku na stránke školy [(www.gjh.sk/nadacia)](/o-skole/nadacia-novohradska).** Na tejto stránke máte možnosť priamo prispieť na rekonštrukciu dvora bezhotovostným prevodom sumy, ktorej výšku si sami zvolíte. Vopred Vám ďakujeme za akýkoľvek finančný dar, účelovo viazaný na rekonštrukciu školského dvora.

RNDr. František Kosper, správca Nadácie Novohradská</br>
Mgr. Renáta Karácsonyová, riaditeľka Spojenej školy Novohradská</br>
Štefan Dobák, vedúci ihriskovej komisie, člen Správnej rady Nadácie Novohradská</br>
RNDr. Zuzana Munková, členka Správnej rady Nadácie Novohradská</br>
