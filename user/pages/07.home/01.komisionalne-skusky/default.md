---
title: 'Komisionálne skúšky'
date: 12.08.2016
publish_date: 11.08.2016
---

<a href=files/komis1516.xls>Zobraziť rozpis komisionálnych skúšok</a> (.xls)
<br>
Prípadné pripomienky posielajte na oravcova@gjh.sk
<br>
Učitelia nájdu kompletný zoznam aj s komisiami na intranete.
<br>
(aktualizované 5.8.2016)