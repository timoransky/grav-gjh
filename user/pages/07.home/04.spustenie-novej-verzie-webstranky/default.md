---
title: 'Spustenie novej verzie webstránky'
published: true
date: '23-09-2016 12:02'
publish_date: '23-09-2016 12:02'
---

Vitajte na novej verzii webstránky Spojenej školy Novohradská!<br>
Stránka je optimalizovaná aj pre mobilné zariadenia. V najbližších dňoch budú na nej ešte vykonávané drobné úpravy a doplnenie chýbajúcich informácií.
V prípade, že nájdete nejaké chyby, zle aktualizované/nepreklopené informácie, pošlite prosím o tom správu na webstranka@gjh.sk.