---
title: 'Zmena organizácie prevádzky školy'
published: true
date: '28-09-2016 14:34'
publish_date: '28-09-2016 14:34'
---

Časť učiteľov Spojenej školy Novohradská vstupuje do štrajku dňa 29.9.2016 od začiatku pracovnej doby do konca tretej vyučovacej hodiny.
Preto vedenie školy rozhodlo, že  prvé tri  vyučovacie hodiny nebude prebiehať vyučovanie v triedach: V.PYP SJ; II.A; sexta; IV.B

Vyučovanie v ostatných triedach sa riadi suplovaním.

Z dôvodu konania IB workshopu na GJH v programe PYP udeľuje riaditeľka riaditeľské voľno v anglickej sekcii PYP ( triedy I. – V. PYP AJ )
 
V Bratislave dňa 28.9.2016, 		Mgr. Renáta Karácsonyová, riaditeľka školy
