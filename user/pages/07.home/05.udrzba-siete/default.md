---
title: 'Údržba Siete'
published: false
---

V noci z 23 na 24.09.2016 bude prebiehať údržba servera. Môžu sa vyskynúť dlhšie výpadky niektorých služieb.