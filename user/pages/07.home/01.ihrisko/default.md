---
title: Ihrisko
published: true
date: '21-09-2016 16:49'
publish_date: '21-09-2016 16:49'
---

Vážení rodičia, milí žiaci, absolventi a priatelia GJH a ZŠ Košická,

aj keď trvalo vybavenie všetkých povolení na rekonštrukciu nášho ihriska dlhšie, ako sme očakávali, máme pre Vás dobrú správu. Dňa 22. augusta 2016 Nadácia Novohradská podpísala zmluvu na realizáciu športovísk s víťazom výberu na dodávateľa stavby, ktorý ponúkol najnižšiu cenu dodávky diela podľa projektu, so spoločnosťou Maro, s.r.o zo Sučian. Výberu sa zúčastnilo päť uchádzačov a v záverečnom kole predložili cenovú ponuku tri spoločnosti. Dodávateľ sa v zmluve zaviazal k ukončeniu a odovzdaniu stavby do 15. októbra 2016. Aj keď pripočítame "stavbársku rezervu" ihrisko, jeho prvá časť, bude dokončené na jeseň tohoto roku.