---
title: 'Sparkling Science'
published: true
date: '28-09-2016 11:14'
publish_date: '28-09-2016 11:14'
---

Dňa 22. septembra 2016 sa na pozvanie p. prof. Gejzu Wimmera, absolventa GJH, skupina IBD študentov našej školy zúčastnila konferencie Sparkling Science, ktorú spoločne organizovali Breath Research Institute, University of Innsbruck a Ústav meraní Slovenskej akadémie vied v Bratislave 
Nás aj skupinku študentov z GAMČA privítal p. Prof. Ing. Milan Tyšler, CSc., riaditeľ Ústavu meraní a predstavil nám oblasti vedeckého bádania tohto ústavu.

![](sparkling-science-ba_2016.jpg)

Potom sme si vypočuli prezentácie študentov gymnázií v Insbrucku, ktorí sa venujú študentskej vedeckej činnosti v spolupráci s Breath Research Institute. Študenti z Bundesrealgymnasium in der AU predstavili svoju prácu o meraní obsahu prchavýh organických látok vylúhovaných z plastových obalov do nápoja pomocou hmotnostnej spektrometrie, ďalší z Gymnasium Adolf-Pichler Platz využívali spektrometriu mobilných iónov na meranie povrchu pleti a študentky z 
 Akademisches Gymnasium predstavili svoju spoluprácu na výskumnej úlohe, ktorej riešením by mohla byť úprava dávkovania chemoterapeutík na základe metabolizmu pacientov stanoveného pomocou uhlíka 13C neinvazívne - testovaním dychu.
 
Potom nám pracovníci BR Inštitútu Dr. Veronika Ruzsanyi a Martin Klieber MSc. predviedli testovanie dychu na prítomnosť metánu prakticky – čiže sme si dali analyzovať metabolizmus, nasledoval spoločný obed a návšteva pracovísk SAV (NMR tomografia, RTG mikrotomografia, RTG difraktometria, FIB mikroskopia, Magnetometria a meranie EEG).
Bola to výborná možnosť vidieť takéto zariadenia v praxi – využívanie moderných prístrojových analytických metód je dnes už samozrejmosťou a je aj súčasťou kurikula chémie v IB Diploma programe. Črtá sa aj možnosť budúcej spolupráce s BRI Insbruck, tak nám držte palce!

Anna Špačeková

![](sparkling1.png)
