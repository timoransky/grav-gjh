---
title: DofE
---

Informácie k výberu študentov do programu Medzinárodná cena vojvodu z Edinburghu (DofE) v šk. roku 2016/2017.


Gymnázium Jura Hronca štartuje 2. ročník programu DofE pre študentov:
	 2. – 4. ročníka 4-ročného gymnázia
	 2. – 5. ročníka 5-ročného bilingv. gymnázia
	 sexty, septimy a oktávy
	 MYP 4
	 IB programu


Ak sa chceš stať súčasťou DofE, prvým krokom je napísanie motivačného listu.

Tvoj motivačný list musí obsahovať tvoje meno, priezvisko, triedu a vyjadrenie sa k nasledujúcim bodom:
	predstavenie sa,
	dôvody záujmu pre zapojenie sa,
	čo očakávaš od programu DofE,
	tvoje doterajšie skúsenosti s dlhodobými aktivitami v rámci športu, dobrovoľníctva a rozvoja talentu – ak nejaké máš, tak aké a ako si bol/a zatiaľ spokojný/á so svojim napredovaním,
	prečo by si mal/a byť vybraný/á práve Ty.

Motivačný list v rozsahu max. 1 normostrana A4 pošli na adresu dofe@gjh.sk do 16.9.2016.

Zoznam študentov, ktorí budú do programu vybraní, bude vyvesený na DofE nástenke na 2. poschodí gymnázia vedľa učebne 208 najneskôr v utorok, 27.9.2016.
