---
title: 'Jún 2016: Michal Gramblička'
prednaska: '„Prestup tepla v chemickom inžinierstve: Kedy zabili tetu Helgu alebo ako dlho variť pštrosie vajce?“'
---

„Prestup tepla v chemickom inžinierstve: Kedy zabili tetu Helgu alebo ako dlho variť pštrosie vajce?“

Michal Gramblička

FAKULTA CHEMICKEJ A POTRAVINÁRSKEJ TECHNOLÓGIE, SLOVENSKA TECHNICKA UNIVERZITA V BRATISLAVE

Obéznu tetu Helgu našli o deviatej ráno bez známky života. Zato sa vyznačovala povrchovou teplotou 28°C. Na akú dobu si má jej podarený manžel vybaviť alibi? Čo by s odhadom doby vraždy narobilo, keby ju zavrel do chladničky? V tejto krátkej prednáške by som Vám rád ukázal, že ak raz porozumiete prestupu tepla, môžete rovnaké príncípy použiť nielen v chemických reaktoroch, ale trebárs aj vo forenzných vedách. Nemálo našich absolventov sa právom uplatnilo aj ako slovenskí/ zahraniční CSI. Alebo i v gastronómii: viete si nasimulovať, koľko variť pštrosie vajce? Natvrdo i namäkko.

V oblasti chemických technológií (rafinérie ropy, papierne, hlinikárne, výroba plastov, liečiv, cementu atď.) sa uplatnia aj ľudia, ktorým veľmi nevoňali orbitály a hybridizácie. Ako mne. Ak však chcete občas vymeniť sako za montérky a zbehnúť z velína či laboratória skontrolovať výrobu, je tu pre Vás technická chémia so širokým uplatnením na Slovensku i vo svete. Pretože doprava tekutín, výmena tepla, úprava vzduchu či separačné operácie (destilácia, rektifikácia, extrakcia, kryštalizácia, atď.) bežia rovnako v slovenčine, angličtine ba i svahilčine.

