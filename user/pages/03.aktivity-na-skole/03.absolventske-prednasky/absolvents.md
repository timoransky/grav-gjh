---
title: 'Absolventské prednášky'
process:
    markdown: true
    twig: false
cache_enable: true
visible: true
---


* [O absolventských prednáškach](#chapter1)
* [O minulej prednáške](#chapter2)
* [Staršie prednášky](#chapter3)


### O absolventských prednáškach ### {#chapter1}

V októbri 2008 sme na GJH založili tradíciu absolventských prednášok. V nasledujúcom roku oslavovalo Gymnázium Jura Hronca polstoročie svojej existencie, počas ktorej ho absolvovalo množstvo dnes verejne známych aj neznámych osobností. Medzi tých známejších istotne patria ľudia pohybujúci sa v oblasti kultúry, športu alebo politiky. 

Sú však aj takí absolventi GJH, ktorých práca nie je možno známa širokej verejnosti, avšak to, čomu sa venujú je nemenej zaujímavé a ich prácou sa možno nechať inšpirovať. Veď aj sám Jur Hronec bol matematikom, ale verejnosť zrejme vie viac o jeho úspechoch v oblasti zakladaní slovenského školstva, ako o matematických problémoch, ktorým sa venoval v rámci svojej vedeckej kariéry. 

Preto sme sa rozhodli zorganizovať rad (dúfajme že nekonečný :-) prezentácii, na ktorých by vám absolventi nášho gymnázia predstavili svoje odvetvie fyziky, biológie, chémie, matematiky alebo informatiky. Na začiatku sa prednášky zameriavali na prírodné vedy a im príbuzné disciplíny, no časom sa prednášky budú viac či menej rozširovať aj do iných oblastí. 

Prednášky sa konajú približne raz mesačne a informácie o nich nájdete na tejto stránke, na plagátoch rozvešaných po škole a, ak ste facebookovým priateľom GJH, aj na facebooku. Nižšie nájdete aj archív prednášok, ktoré sa už konali, s pozvánkami, fotkami a prezentáciou z prednášky (ak nejaká existuje). V prípade, že máte nápad, ako naše prednášky vylepšiť, dajte mi vedieť, či už priamo na prednáškach alebo aj mimo nich. A, samozrejme, ak máte tip, koho by sme mohli privítať ako hosťa, dajte mi tiež vedieť. 

Ak chcete byť mailom vopred informovaní o najbližších prednáškach, pošlite mi e-mail (na štandardnú adresu priezvisko@gjh.sk), zaradím vás do mailing-listu. 

Teším sa na stretnutie. 

Ľubo Lanátor


### O minulej prednáške ### {#chapter2}

