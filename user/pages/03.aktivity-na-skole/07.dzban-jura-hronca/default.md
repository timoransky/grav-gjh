---
title: 'DŽBÁN JURA HRONCA'
process:
    markdown: true
    twig: false
cache_enable: true
visible: true
---

![DŽBÁN JURA HRONCA](dzban.jpg)  Milí GJHáci, aj počas tohto školského roku sa pomaly ale isto rozbieha medzitriedne zápolenie o trofej najcennejšiu – Džbán Jura Hronca. 
Džbán je medzitriedna súťaž s dlhoročnou tradíciou, do ktorej sú zapojené všetky triedy veľkého GJH (triedy štvorročného, päťročného aj osemročného gymnázia od kvinty a triedy MYP 4, 5 a 3IB A a B). Do súťaže nie sú zapojení maturanti, o. i. aj preto, že súčasťou Džbánu je aj Športový deň, ktorý sa koná vždy koncom júna, kedy sú maturanti už dávno fuč :-)

Základným pravidlom Džbánu je, že sa súťaží len v takých disciplínach, do ktorých sa môžu zapojiť všetky triedy s rovnakou šancou. Vo všetkých disciplínach sa do Džbánu zarátavajú body za účasť, resp. umiestnenie v najnižšom leveli súťaže (nie umiestnenia v celoštátnom kole a pod.)

Tri triedy s najvyšším počtom bodov vyhrajú deň voľna na usporiadanie školskej akcie a finančný príspevok na ňu od Nadácie Novohradská. Aj tento rok súťažíme o **Skokana roka** a **Najlepšieho nováčika**.

[Zoznam všetkých súťaží](dzban-sutaze1516.pdf) (.pdf) </br>
[Podmienky prideľovania bodov za jednotlivé súťaže](dzban-body1516.pdf) (.pdf)

Informácie o súťažiach jednotlivých predmetov sa môžte dozvedieť od učiteľov daných predmetov. 
O ostatných súťažiach informujú plagátiky po škole a táto stránka.


**Akékoľvek návrhy na nové sútaže, konštruktívne pripomienky a rady privítame!** </br>
**Zuzana Barančoková - koordinátorka súťaže** </br>
barancokova@gjh.sk, kabinet anglického jazyka v ŠVL na 2.posch. </br>