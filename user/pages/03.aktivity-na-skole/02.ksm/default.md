---
title: KSM
process:
    markdown: true
    twig: false
cache_enable: true
visible: true
---

## Korešpondenčný seminár z matematiky

[Úlohy prvého kola KSM9 2015/2016](1ksm9_16.pdf) pre ôsmakov a deviatakov ZŠ (.pdf) </br>
[Úlohy prvého kola KSM5 2015/2016](1ksm5_16.pdf) pre piatakov ZŠ (.pdf)

[Úlohy druhého kola KSM9 2015/2016](2ksm9_16.pdf) pre ôsmakov a deviatakov ZŠ (.pdf) </br>
[Úlohy druhého kola KSM5 2015/2016](2ksm5_16.pdf) pre piatakov ZŠ (.pdf)

[Konečné poradie KSM5](KSM5konecneporadie2016.pdf) (.pdf) </br>
[Konečné poradie KSM8-9](KSM9konecneporadie2016.pdf) (.pdf)