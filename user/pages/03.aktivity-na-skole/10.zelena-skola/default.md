---
title: 'Zelená škola'
---

Spojená škola Novohradská je patrí odteraz oficiálne medzi Zelené školy!

S veľkou radosťou oznamujeme, že záverečnom hodnotení programu Zelená škola v šk. roku 2015/2016 sme získali titul, certifikát a vlajku Zelená škola.

O udelení certifikátu rozhodla Rada Zelenej školy na základe správy hodnotiacej návštevy. Ocenenie sme získali za realizáciu metodiky 7 krokov v našej prvej prioritnej téme – Životné prostredie - v práve končiacom dvojročnom certifikačnom období.

K získaniu certifikátu prispeli významnou mierou študenti IIIMYP triedy, ktorí v rámci svojho Komunitného projektu skrášlili vnútorné prostredie školy, popresádzali, identifikovali a označili interiérové rastliny na strane gymnázia, vymysleli olepenie schodov malou násobilkou na strane ZŠ a frázami z jazykov, ktoré sa na škole učia, na strane gymnázia; opravili dvierka na školskom kompostéri a osadili ho, vypleli pás trávnika pri budove školy, zmontovali a osadili domčeky pre drobné vtáctvo a urobili dendrologický prieskum stromov na dvore. Študenti VMYP ešte počas minulého školského roka počas svojho Komunitného projektu navrhli a vlastnoručne zhotovili kompostér, posadili rastliny na školský balkón, analyzovali pH pôdy a založili skalku. 
Veľký dojem urobili aj žiaci oboch PYP programov svojimi environmentálnymi aktivitami a študenti IB Diploma programu, ktorí v rámci Creativity, Action, Service vymysleli, propagovali, realizovali a menežovali systém kompostovania biologického odpadu.

Ocenenie má platnosť na 2 nasledujúce školské roky (2016/2017, 2017/2018)
V programe Zelená škola by sme chceli pokračovať aj v nasledujúcom období, našou prioritnou témou budú tentoraz odpady. 

Chcem veľmi poďakovať všetkým žiakom, kolegom a rodičom za vynaložené úsilie a podporu!
Anna Špačeková
Učiteľka chémie a koordinátor environmentálneho vzdelávania 
