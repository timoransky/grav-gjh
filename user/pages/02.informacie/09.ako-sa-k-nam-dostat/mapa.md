---
title: 'Ako sa k nám dostať'
process:
    markdown: true
    twig: false
cache_enable: true
visible: true
---

### Ako sa dostať na SŠ Novohradská.

Spojená škola Novohradská (GJH a ZŠ Košická) je ohraničená ulicami Prievozská, Košická a Novohradská. 

Pri škole sú zastávky liniek MHD 68, 70, 202, 207 a 208 Košická a Svätoplukova (v smere z centra mesta a naopak)
Na zastávke Svätoplukova zastavujú obojsmerne aj linky 50 a 205.

Cestovné poriadky a informácie o prípadných zmenách a výlukách MHD nájdete na www.imhd.sk alebo www.dpb.sk

**Ako sa dostať do školy z autobusovej stanice:**

Autobusová stanica Mlynské Nivy je vzdialená asi 5 minút pešo od budovy školy. Zo stanice pôjdete po ulici Mlynské Nivy v smere z centra do Ružinova. 

**Ako sa dostať do školy zo železničnej stanice (Bratislava - Hlavná stanica):**

Trolejbusom 210, na konečnú zastávku - Autobusová stanica Mlynské Nivy. Odtiaľ viď odstavec vyššie.
