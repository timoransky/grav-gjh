---
title: 'Klub matematikov'
process:
    markdown: true
    twig: false
cache_enable: true
visible: true
---

## Pozvánka do klubu učiteľov matematiky </br>

** Deliteľnosť**

Prvé poprázdninové stretnutie bude hlavne o dohode, čo chceme robiť tento rok. Ale mám pripravenú aktivitu o deliteľnosti súčtov vhodnú pre ZŠ aj SŠ.

Stretneme sa 27. 9. 2016 o 15:00 na Gymnáziu Jura Hronca na Novohradskej ulici v učebni 411 na 4. poschodí.

**V prípade otázok neváhajte písať na jana.fraasova@gmail.com.**

Teším sa na všetkých.</br>
Jana Fraasová</br></br>

Už niekoľko rokov uvažujem nad obnovením tradície stretávania sa učiteľov matematiky, ktorú si pamätám ešte zo svojich školských čias. Moje rozhodnutie začať podporili informácie o úspešnom stretávaní sa učiteľov informatiky, vy – učitelia matematiky, ktorí ste vyjadrili svoje priania stretávať sa, ako aj podpora zo strany oddelenia didaktiky matematiky KAGDM FMFI UK byť spoluorganizátorom týchto stretnutí.

Milí kolegovia, pozývame vás do klubu učiteľov matematiky, ktorého stretnutia sa budú konať približne každý tretí utorok v čase od 14.30 hod. do cca 16.00 hod. na Gymnáziu Jura Hronca v Bratislave. Na stretnutiach radi uvidíme učiteľov matematiky zo ZŠ a SŠ, ale vítaní sú aj budúci učitelia, vedúci krúžkov, či ďalší priaznivci vyučovania matematiky.

Cieľom klubu je umožniť učiteľom matematiky rozširovať si svoje profesionálne obzory v pedagogickej, didaktickej či matematickej oblasti. Venovať sa chceme aktuálnym problémom vo vyučovaní matematiky, kurikulu, učebniciam, veríme, že sa nám podarí inšpirovať sa navzájom – zlepšovať naše učenie. Akákoľvek téma bude vítaná a po odsúhlasení väčšinou zaradená do programu. V prípade záujmu sa pokúsime pozvať aj odborníkov z VŠ, resp. z praxe.

Ak máte záujem navštevovať klub v tomto školskom roku, napíšte nám na jana.fraasova@gmail.com


**Jana Fraasová, GJH Bratislava**