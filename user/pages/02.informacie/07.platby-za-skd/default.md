---
title: 'Platby za ŠKD'
---

Na základe §114 ods. 3 zákona 245/2008 Z. z. (školský zákon), ktorý stanovuje výšku úhrady za pobyt detí v školskom klube detí na sumu do 7,5% životného minima neplnoletého dieťaťa mesačne (od 1. 7. 2013 je životné minimum pre neplnoleté dieťa v zmysle Opatrenia MPSVR č. 186/2013 Z. z. 90,42 € mesačne), je mesačný príspevok za jedno dieťa v školskom roku 2016/2017 s účinnosťou od 1. 9. 2016 nasledovný:
2,10 € za dieťa, ktorého rodičia sú v hmotnej núdzi,
3,70 € za dieťa zamestnancov Spojenej školy Novohradská
6,78 € za ostatné deti.
Úhradu za daný mesiac treba vykonať do 10. dňa v mesiaci. Taktiež je možné uhradiť príspevok jednorazovo za mesiace september – december 2016 (najneskôr do 10. 11. 2016) a za január – jún 2017 (najskôr 1. 1. 2017).
Úhradu možno vykonať bankovým prevodom na účet v Štátnej pokladnici
SK48 8180 0000 0070 0019 9963 
konštantný symbol: 0308
variabilný symbol: uvádzate ten istý, ktorý používate pri platbe za stravu v ŠJ
správa pre prijímateľa: priezvisko a meno dieťaťa
Poplatok za dieťa pravidelne navštevujúce ŠKD sa platí nezávisle na počte dní, ktoré dieťa trávi v školskom klube. Dieťa možno odhlásiť zo školského klubu na celý mesiac písomne pred začiatkom mesiaca.