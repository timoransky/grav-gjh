---
title: 'Vyhodnocovacia správa'
process:
    markdown: true
    twig: false
cache_enable: true
visible: true
---

[Vyhodnocovacia sprava 2014/2015 ](sprava1415.pdf)(.pdf)</br>

Príloha 1:
* [Výchovnovzdelávacie výsledky - štvor. a päťročné gymnázium](klasif-gjh1415.xls) (.xls)  
* [Výchovnovzdelávacie výsledky - osemročné gymnázium ](klasif-8g1415.xlsx) (.xlsx)
* [Výchovnovzdelávacie výsledky - ZŠ](klasif-zs1415.xls) (.xls)
* [Výchovnovzdelávacie výsledky - podľa predmetov](klasif-priemery1415.xlsx) (.xlsx)

* Príloha 2: [Pedagogickí zamestnanci](pr2-ucitelia1415.pdf) (.pdf)
* Príloha 3: [Finančné a hmotné zabezpečenie](pr3-financie1415.pdf) (.pdf)
* Príloha 4: [Absolventi GJH na VŠ](pr4-absolventi1415.pdf) (.pdf)