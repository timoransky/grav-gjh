---
title: 'IT návody'
process:
    markdown: true
    twig: false
cache_enable: true
visible: true
---

## Počítačová sieť, školské výpočtové laboratórium (ŠVL) a IT služby na škole.

Nájdete tu návody pre riešenie problémov a informácie o IT službách na našej škole.

[**Poriadok ŠVL**](poriadokSVL.pdf) (.pdf)

[O vašom konte](#)</br>
		O tom čo je konto, heslo, server, H:, profil, mailova adresa, webalias...</br>

[O vzdialenom prihlásení](#)</br>
		Ako sa prihlásiť na školské konto z pohodlia domova? Ako dostať súbory na školský server alebo domov?</br>

[O mailovaní](#)</br>
		Ako si čítať a posielať e-maily pomocou vašej školskej mailovej schránky?</br>

[O Wifi](#)</br>
		Ako sa pripojit na wifi?</br>

[Webkalendár](#)</br>
		Ako si nastaviť zobrazenie a automatickú aktualizáciu kalendára udalostí zo školskej webstránkyna vašom 		mailovom klientovi (obsahuje podrobné návody s ukážkami v najpoužívanejších programoch)