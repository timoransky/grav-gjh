---
title: 'Pre maturantov'
process:
    markdown: true
    twig: false
cache_enable: true
visible: true
---

### Pre maturantov

V tejto časti budeme zverejňovať ponuky pre maturantov, ktoré prichádzajú na našu školu.
Ponuky sa týkajú štúdia na vysokých školách na Slovensku i v zahraničí a obsahujú informácie o študijných odboroch a pod.

