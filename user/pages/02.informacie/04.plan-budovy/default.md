---
title: 'Plán budovy'
process:
    markdown: true
    twig: false
cache_enable: true
visible: true
---

[Plán budovy školy s triedami na rok 2016/2017](planbudovy1617.xlsx)  (.xlsx)