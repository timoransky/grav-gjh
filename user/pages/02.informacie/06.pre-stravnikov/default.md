---
title: 'Pre stravníkov'
process:
    markdown: true
    twig: false
cache_enable: true
visible: true
---

## Informácie pre stravníkov v školskej jedálni

* [Platby a ceny](#chapter1){.list-menu}
* [Prihlasovanie, odhlasovanie](#chapter2)
* [Stravovacia komisia, pripomienky](#chapter3)
* [Čipy](#chapter4)
* [Alergici](#chapter5)
* [Kontakt](#chapter6)

</br>
Všetky záležitosti ohľadom stravníkov vybavuje p. Vajdová (vajdova@gjh.sk, 02/210 28 313)
</br>

### Platby a ceny ### {#chapter1}

Ceny stravného:

I.stupeň. (PYP1-4 SJ/AJ) - 1,09€ + 0,20€ (réžia) = 1,29€
II.stupeň. (ZŠ 5-9 ročník, PYP5 SJ/AJ, PreMYP-Myp3, Prima - Kvarta ) - 1,16€ + 0,20€ (réžia) = 1,36€
III.stupeň. (4ročné. a 5ročné GJH, Kvinta - Oktáva, MYP4, MYP5, IBD ) - 1,26€ + 0,20€ (réžia) = 1,46€

Za každý odhlásený obed sa automaticky odpočítava aj réžia, teda ak si stravník odhlási obed, odčíta sa mu aj 0,20€ za réžiu.

Dôležité: Medzibankový prevod a jeho automatizované spracovanie môže spôsobiť, že kredit sa do systému nahodí až v priebehu 5 pracovných dní. Ak necháte platbu na poslednú chvíľu, môže sa stať, že stravník nedostane prvý deň v novom mesiaci jedlo.

Číslo účtu: 7000200039/8180 (účet je vedený v štátnej pokladnici)
Číslo účtu v tvare IBAN: SK33 8180 0000 0070 0020 0039.
Variabilný symbol: dozviete sa v časti platby na edupage alebo v mobilnej aplikácii
Konštantný symbol: 0308

Do správy pre prijímateľa uveďte prosím meno stravníka a triedu, do ktorej chodí. Uľahčíte nám tým identifikáciu vašej platby a predíde sa tak mnohým nedorozumeniam.
<!--
**Ceny stravného:**

**I.stupeň.** (PYP1-4 SJ/AJ) - 1,09€ + 0,20€ (réžia) = **1,29€** </br>
**II.stupeň.** (ZŠ 5-9 ročník, PYP5 SJ/AJ, PreMYP-Myp3, Prima - Kvarta ) - 1,16€ + 0,20€ (réžia) = **1,36€**</br>
**III.stupeň.** (4ročné. a 5ročné GJH, Kvinta - Oktáva, MYP4, MYP5, IBD ) - 1,26€ + 0,20€ (réžia) = **1,46€**</br>

**Za každý odhlásený obed sa automaticky odpočítava aj réžia, teda ak si stravník odhlási obed, odčíta sa mu aj 0,20€ za réžiu.**

V prípade, že stravné platíte v banke resp. cez internet banking, nie je potrebné priniesť potvrdenie o zaplatení, teda automaticky ste na nový mesiac prihlasení na stravu. Ak platíte šekom, je potrebné doniesť potvrdenie o zaplatení.

**Dôležité:** **Ak platíte cez internet-banking, realizujte platbu najneskôr 5 PRACOVNÝCH dní pred začiatkom nového mesiaca!**
Spracovanie údajov bankou totiž istý čas trvá a ak necháte platbu na poslednú chvíľu môže sa stať, že platba ešte nebude nabehnutá a stravník nedostane prvý deň v novom mesiaci svoje jedlo!

**Pri platbe cez Internet banking vyplňte údaje nasledovne:**

**Číslo účtu:** 7000200039/8180 (účet je vedený v štátnej pokladnici)</br>
**Číslo účtu v tvare IBAN:** SK33 8180 0000 0070 0020 0039</br>
**Variabilný symbol:** mesiac a rok (napr: 0208)</br>
**Špecifický symbol:** uveďte triedu, do ktorej chodí stravník</br>
**Konštantný symbol:** 0308</br>

Do poznámky, prosíme, uveďte meno stravníka a triedu, do ktorej chodí. Uľahčíte nám tým identifikáciu vašej platby a predíde sa tak mnohým nedorozumeniam.
-->
### Prihlasovanie na stravu, odhlasovanie ### {#chapter2}

Ak máte uhradenú stravu, tak ste automaticky prihlásený na obed A, takže objednávať si treba iba ak si prosíte obed B. Zmeniť jedlo, alebo odhlásiť zo stravy sa môžete pomocou čipu alebo karty jedným z nasledujúcich spôsobov: 

• na objednávacom termináli v jedálni pri okienku v rohu 
•	cez webovú stránku ssnovohradska.edupage.org 
•	pomocou mobilnej aplikácie Edupage. 

Prehlásiť a prihlásiť obed je možné vo vyučovací deň vopred do 14:30. Odhlásiť sa možno do 7:45 ráno v daný deň osobne, telefonicky, mailom, cez web alebo mobilnú aplikáciu.

### Stravovacia komisia, pripomienky ### {#chapter3}

Pripomienky k strave môžete písať mailom stravovacej komisii na stravkom@gjh.sk

### Čipy ### {#chapter4}

Prvý čip dostáva stravník zadarmo pri nahlásení sa na obedy. Za stratený čip sa platí 1€ na účet Nadácie Novohradská - SK21 0900 0000 0000 1146 9293 (Nie na stravný účet!) Po zaplatení treba priniesť potvrdenie, na základe ktorého stravník dostane nový čip. Nefunkčný čip, ktorý nejaví známky mechanického poškodenia, sa vymieňa bezplatne.

### Informácie pre alergikov ### {#chapter5}

Na základe výnosu Ministerstva pôdohospodárstva a Ministerstva zdravotníctva SR č. 1187/2004-100 o označovaní potravín v znení výnosu č 1761/2005-100 a výnosu č. 3069/2005-100 je povinnosťou školských jedální informovať rodičov o obsahu alergénov v potravinách.

Zoznam alergénnych zložiek

1. obilniny obsahujúce lepok (t.j. pšenica, raž, jačmeň, špalda, kamut) a výrobky z nich
2. kôrovce a výrobky z nich
3. vajcia a výrobky z nich
4. ryby a výrobky z rýb
5. arašidy a výrobky z nich
6. sója a výrobky zo sóje
7. mlieko a výrobky z mlieka
8. orechy a výrobky z orechov, ktorými sú mandle, lieskové orechy, vlašské orechy, kešu, pekanové orechy, para orechy, pistácie, makadamové orechy a queenslandské orechy
9. zeler a výrobky zo zeleru
10. horčica a výrobky z horčice
11. sezamové semená a výrobky z nich
12. oxid siričitý a siričitany (o koncentrácií viac ako 10 mg/l )
13. vlčí bôb a výrobky z neho
14. mäkkýše a výrobky z nich

Geneticky modifikované suroviny sa pri príprave pokrmov nepoužívajú

### Kontakt ### {#chapter6}


Telefón: 02/210 28 313 - Eva Vajdová - všetky záležitosti ohľadom stravníkov (odhlasovanie...)</br>
Telefón: 02/210 28 324 - Alžbeta Kasáková - vedúca školskej jedálne</br>

### Poďakovanie - súťaž Strieborná žufanka

Kolektív kuchárok školskej jedálne na našej škole ďakuje všetkým podporovateľov v súťaži [Strieborná žufanka](http://kucharkysutazia.webnode.sk) o najsympatickejší kuchársky tím v školských jedálňach v Bratislave. Vďaka nim naše kuchárky dosiahli **prvé miesto** s počtom hlasov 666.

![Strieborná žufanka](zufanka.jpg){.notfull}
