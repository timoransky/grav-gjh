---
title: 'Logo školy'
process:
    markdown: true
    twig: false
cache_enable: true
visible: true
---

## Logo školy
[Rastrový obrázok loga GJH](gjhlogo.jpg) (.jpg; 1000x1145)</br>
[Rastrový obrázok loga ZŠ Košická](kosicka.png) (.png)

[Vektorový obrázok loga GJH](gjhlogo.svg)(.svg)