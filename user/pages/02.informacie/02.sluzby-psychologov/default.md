---
title: 'Služby psychológov'
process:
    markdown: true
    twig: false
cache_enable: true
visible: true
---

### Služby psychológov na SŠ Novohradská

#### PhDr. Renáta Nemčoková
Psychológ pre žiakov medzinárodného programu PYP a MYP.</br>
Úradné hodiny: utorok, štvrtok a piatok medzi 8:00 a 15:30 </br>
Osobné stretnutia si dohovorte mailom alebo telefonicky. </br>
**E-mail:** nemcokova@gjh.sk </br>
**Tel:** 02/210 28 333 </br>

#### PhDr. Andrea Čapuchová
Psychológ pre primu až kvartu osemročného gymnázia a ZŠ. </br>
Úradné hodiny: denne od 8:00 do 15:30 </br>
Osobné stretnutia si dohovorte mailom alebo telefonicky. </br>
**E-mail:** capuchova@gjh.sk </br>
**Tel:** 02/210 28 318 </br>

