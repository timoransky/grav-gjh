---
title: Dokumenty
process:
    markdown: true
    twig: false
cache_enable: true
visible: true
---

## Dokumenty
<!--
* [Telefónny zoznam - klapky v školskom roku 2015/2016](klapky1516.pdf) (.pdf)
* [Školský poriadok](skolskyporiadok.pdf) (.pdf)
* [Vstupy do školy - informácia o otváracích časoch jednotlivých vchodov do budovy](vstupyzmena.pdf) (.pdf)
* [Platby za pobyt detí v ŠKD 2015/2016](skd1516.pdf) (.pdf)
* [Ponuka krúžkov pre žiakov do 15 rokov](kruzky1516zs.pdf) (.pdf)
* [Ponuka krúžkov pre žiakov nad 15 rokov](kruzky1516gjh.pdf) (.pdf)
* [Termínovník 2015/2016](terminovnik1516.pdf) (.pdf)-->

<hr>

* Informácie a dokumenty o programe MYP: [Stránky programu MYP](https://myp.gjh.sk/)
* Informácie a dokumenty o programe PYP: [Stránky programu PYP](#)
* Informácie a dokumenty o programe IB DIPLOMA: [Stránky programu IBD](https://ib.gjh.sk/en/)
* Dokumenty týkajúce sa Nadácie Novohradská: [Nadácia Novohradská](/o-skole/nadacia-novohradska)

___

### Dokumenty pre učiteľov

Potrebné dokumenty a informácie nájdete na [Intranete](/sluzby-na-webe/intranet-pre-ucitelov)