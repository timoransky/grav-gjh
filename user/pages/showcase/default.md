---
title: 'An ultimate guide to markdown and Grav pages editing'
---

# Toto je nadpis H1
## Toto je nadpis H2
### Toto je nadpis H3
#### Toto je nadpis H4
##### Toto je nadpis H5
###### Toto je nadpis H6

Vnútry textov stránky odporúčam použiť H3. Má nastavenú optimálnu veľkosť, no okrem H1 je povolené všetko.

Toto je obyčajný text v paragraphe. Text môže byť aj **hrubý** , _kurzívou_ či ~~preškrtnutý~~ 
Na oddelenie textu sa dá použiť oddeľovač ako napríklad tento tu dole tam...

---
### Zoznamy

V texte sa dá použiť číslovaný zoznam
1. ako
2. napríklad
3. toto

Alebo aj nečíslovaný zoznam
* ako
* napríklad
* toto

### Bloky

Dokonca vieme vytvoriť rôzne farebné bloky textu.
! Keď chceme na niečo upozorniť

!! Keď chceme na niečo upozorniť ale že naozaj

!!! Keď chceme len niečo zvýrazniť

!!!! Keď je niečo super fajn a každý o tom musí vedieť

### Tabuľky

Tabuľky sú tiež v pohode, ich zápis je síce trocha krkolomný ale dá sa naučiť

|  Column 1 Title  |  Column 2 Title  |  Column 3 Title  |
|  :-----          |  :-----          |  :-----          |
|  A |  potom |  vznikne |
| takáto |  pekná |  tabuľka |

### Obrázky a linkovanie

Do textu sa dá aj ľaho vložiť link na [Google](http://google.sk), alebo podobným spôsobom vložiť obrázok.

Obrázky sa daju alignovať podľa potreby. Buď ho necháme v pôvodnej veľkosti na stred ...
![Obrázok](https://s-media-cache-ak0.pinimg.com/236x/11/8e/6f/118e6f39fac9344d6589c84d5ee9e667.jpg){.center}

... alebo sa môže roztiahnuť po celej šírke ...
![Obrázok](https://s-media-cache-ak0.pinimg.com/236x/11/8e/6f/118e6f39fac9344d6589c84d5ee9e667.jpg){.full}

... alebo ho môžme dať napravo ...
![Obrázok](https://s-media-cache-ak0.pinimg.com/236x/11/8e/6f/118e6f39fac9344d6589c84d5ee9e667.jpg){.right}

... či dokonca naľavo ...
![Obrázok](https://s-media-cache-ak0.pinimg.com/236x/11/8e/6f/118e6f39fac9344d6589c84d5ee9e667.jpg){.left}

... a ak moc chceme, dá sa aj floatnúť napravo ...
![Obrázok](https://s-media-cache-ak0.pinimg.com/236x/11/8e/6f/118e6f39fac9344d6589c84d5ee9e667.jpg){.float-right}
> Cupcake ipsum dolor sit amet danish sweet wafer. Sesame snaps sweet roll tart I love. Macaroon pastry dessert lemon drops oat cake oat cake I love gummi bears. Jelly lemon drops I love cookie sugar plum caramels halvah oat cake. Sesame snaps apple pie pastry sweet gummies fruitcake jelly-o I love chocolate cake. Tart jelly-o I love pastry soufflé sugar plum. Chocolate dessert marshmallow pudding pudding chocolate cake toffee. Tiramisu toffee pudding. Macaroon halvah tootsie roll jujubes I love biscuit. Ice cream gummi bears lemon drops pie wafer.

... alebo aj naľavo ...
![Obrázok](https://s-media-cache-ak0.pinimg.com/236x/11/8e/6f/118e6f39fac9344d6589c84d5ee9e667.jpg){.float-left}
> Cupcake ipsum dolor sit amet danish sweet wafer. Sesame snaps sweet roll tart I love. Macaroon pastry dessert lemon drops oat cake oat cake I love gummi bears. Jelly lemon drops I love cookie sugar plum caramels halvah oat cake. Sesame snaps apple pie pastry sweet gummies fruitcake jelly-o I love chocolate cake. Tart jelly-o I love pastry soufflé sugar plum. Chocolate dessert marshmallow pudding pudding chocolate cake toffee. Tiramisu toffee pudding. Macaroon halvah tootsie roll jujubes I love biscuit. Ice cream gummi bears lemon drops pie wafer.



Viac sa dá dočítať na oficiálnej dokumentácii frameworku [Grav](https://learn.getgrav.org/), alebo na stránke venovanej [Markup](http://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)