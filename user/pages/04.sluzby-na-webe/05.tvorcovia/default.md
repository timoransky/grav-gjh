---
title: Tvorcovia
process:
    markdown: true
    twig: false
cache_enable: true
visible: true
---

### Tvorcovia školského webu
Za obsah, resp. aktualizáciu údajov zodpovedá Jaroslav Výbošťok. (učiteľ informatiky, GJH)<br>
Pripomienky a materiály s popisom na zverejnenie posielajte na adresu: webstranka@gjh.sk

Tvorcovia:

**Ján Timoranský a Tomáš Velich - absolventi GJH (4C, 2016)**
* autori súčasného webu (programovanie a design)
<br>

**Slavomír Hitka - absolvent GJH (Oktáva, 2010)**
* autor predchádzajúceho (2007-2016) webu a CMS
* programovanie a design
<br>


**Denis Donauer - absolvent GJH (4C, 2006)**
* autor predchádzajúceho webu (do roku 2007)
<br>


**Ďakujeme všetkým, ktorí akýmkoľvek spôsobom pomohli pri tvorbe súčasného i&nbsp;predchádzajúcich webov našej školy.**

