---
title: 'Jur Hronec '
process:
    markdown: true
    twig: false
cache_enable: true
visible: true
---

### Po kom sa naša škola volá

Jur Hronec (1881 Gočovo – 1959 Bratislava)
![Jur Hronec](jurhronec.jpg)

Osobnosť Jura Hronca spája úspešného matematika a organizátora, predstaviteľa snáh o vybudovanie vysokého školstva na Slovensku. Viedol prípravy na zriadenie Slovenskej vysokej školy technickej v Bratislave a Vysokej školy poľnohospodárskej a lesníckej v Košiciach, stál pri vzniku Prírodovedeckej fakulty Univerzity Komenského a Vysokej školy obchodnej. Uvedené úsilie sa opieralo o jeho úspešnú vedeckú prácu v oblasti matematiky, o jeho pedagogické skúsenosti vysokoškolského profesora, ako i precíznu znalosť slovenských pomerov. 

Záujem o matematiku a oblasť diferenciálnych rovníc u neho vzbudil počas jeho štúdia v Kluži profesor Ľ. Schlesinger, rodák z Trnavy. Sám študoval matematiku v Berlíne u L. Fuchsa, znalca problematiky diferenciálnych rovníc. Roku 1906 nastúpil Jur Hronec do gymnázia v Kežmarku, kde učil 16 rokov. Vzdelanie si rozšíríl počas ročného pobytu na univerzite v Göttingene, pričom si musel platiť svojho zástupcu na gymnáziu. V Göttingene chodil na prednášky jedného z najvýznamnejších matematikov 20. storočia Davida Hilberta. Roku 1912 v Giessene obhájil doktorát filozofie. V roku 1922 využil štátne štipendium a ročnú dovolenku na pokračovanie v štúdiách v Prahe, Göttingene a Giessene. Roku 1923 habilitoval na Prírodovedeckej fakulte UK v Prahe a začal tam prednášať po slovensky. Roku 1928 bol menovaný za profesora na Českej vysokej škole technickej v Brne. V 30. rokoch sa profesor Hronec snažil o kompletizáciu vysokého školstva na Slovensku. Okrem iných škôl sa významne zaslúžil o zriadenie Vysokej školy technickej M.R. Štefánika v Košiciach, kde roku 1937 vybral prvých profesorov a sám sa stal rektorom školy. Keď Košice pripadli Maďarsku, SVŠT sa premiestnila najprv do Prešova, potom do Martina a nakoniec do Bratislavy. Profesor Hronec mal aj vysoký pedagogický úväzok – bol profesorom matematiky na SVŠT, vedúcim Katedry matematiky na Prírodovedeckej fakulte UK v Bratislave. 

Prevažná časť 11 knižných prác a 24 štúdií profesora Hronca sa týkala problematiky diferenciálnych rovníc (dvojzväzkové dielo Diferenciálne rovnice). Jur Hronec bol autorom prvých slovenských učebníc vyššej matematiky. Tieto učebnice zohrali významnú úlohu pri výchove novej generácie slovenských matematikov. Ďalšou oblasťou záujmu profesora Hronca bola pedagogika. V tejto oblasti uverejnil dve knihy a 15 článkov. Zameriaval sa na osobnosť pedagóga, pričom využíval svoje bohaté pedagogické skúsenosti. Na vysvetľovanie vzťahov učiteľ – žiak vo výučbovom procese sa snažil aplikovať matematické vzťahy. Napríklad výkon vo vyučovaní (V) vyjadril vzťahom V=c.S.I, kde c sú vlastnosti učiteľa, S jeho schopnosti a prax a I je žiakova povaha. Jednotlivé veličiny Jur Hronec aj presne definoval. 

Profesor Hronec sa ešte ako profesor na kežmarskom gymnáziu zasadzoval o jednotnú školu a vysokoškolskú prípravu všetkých učiteľov. Zdôrazňoval nevyhnutnosť vedeckej prípravy vysokoškolských učiteľov, jej prislúchajúceho ocenenia i vytvorenia vhodných podmienok pre vedeckú prácu. Na tému vedeckej práce povedal: „Produktívna vedecká práca potrebuje dobrú pôdu. Kde jej niet, tam je vedecká práca neúrodná, neplodná, a keď nejaká jestvuje, nie je cenná. Cennú vedeckú prácu neslobodno posudzovať tak, ako sa posudzuje práca úradníka“. Roku 1921 sa zúčastnil ako jediný zo Slovenska na práci 12-členného kuratória Československého ústavu pedagogického v Prahe. Snažil sa o reformu stredných škôl, ich zmenu na jednotný typ škôl s možnosťou štúdia pre všetkých. 

Profesor Hronec bol autoritou pre slovenské vedecké kruhy i pre celú spoločnosť. Roku 1945 bol spolu s L.Novomeským predsedom Matice slovenskej. Stal sa predsedom Slovenského múzea v Bratislave, akademikom Slovenskej akadémie vied. Získal Rad práce a posmrtne medailu Jana Amosa Komenského. Na jeho počesť je umiestnená pamätná tabuľa na Matematickom pavilóne Fakulty matematiky, fyziky a informatiky Univerzity Komenského v Mlynskej doline v Bratislave.