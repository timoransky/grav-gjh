---
title: 1979/1980
---

Školský rok 1979 - 1980

4.A</br>
Triedny profesor: Ľubomír Krištof</br>
Helena Balášková, Margita Baroková, Elena Bártová, Eva Deáková, Karol Drgoň, Viera Glatznerová, Rudolf Fatranský, Igor Fingerland, Libor Fingerland, Ľudovít Francľ, Marián Gálik, Ľubomír Garaj, Magda Hamadová, Eva Hasáková, Zuzana Hejná, Peter Hlásnik, František Kobza, Jozef Kovačovský, Štefan Málik, Jana Matulová, Branislav Mičík, Ladislav Molnár, Silvia Novotná, Zuzana Ondriašová, Beata Pavlíková, Zuzana Polónyová, Daň Pospíšil, Edita Potočná, Miloš Reháček, Dagmar Roháčová, Ružena Schrancová, Miroslav Styk, Natália Šteučeková, Tatiana Tkáčiková, Ľudmila Žáková

4.B</br>
Triedny profesor: Ján Koval</br>
Teodor Baláž, Pavol Bartoš, Peter Bernát, Jaroslav Böhm, Marián Brichta, Andrej Černík, Miloš Daranský, Marián Drobný, Miroslav Ďuratný, Anton Julény, Peter Jurášek, Oľga Kátlovská, Katarína Koncová, Miroslav Linek, Eva Luptáková, Dalibor Málek, Katarína Miháliková, Vladimír Mišík, Karol Morár, Peter Ondreas, Jakub Osvald, Boris Pardubský, František Petrík, Stanislav Polakovič, Dana Slovíková, Miroslav Stejskal, Jana Šamudovská, Peter Šulek, Igor Turek, Iveta Tureková, Ján Vicen, Marián Weber, Karol Zverka

4.C</br>
Triedny profesor: Vladimír Jodas</br>
Andrea Brichtová, Vladimír Broček, Pavol Čepčiansky, Jozef Daňko, Dušan Dobiáš, Jozef Gregor, Karol Hoffman, Michal Horváth, Ľubomír Hotový, Katarína Hrotková, Branislav Hučko, Oliver Hudec, Juraj Jonek, Andrej Kapusta, Dana Kolibiarová, Antonín Kostka, Jaroslav Krč, Ľubomír Kulla, Miluše Matejíčková, Karola Novotná, Martin Oravec, Jana Osvaldová, Dušan Pakši, Eva Paulíková, Alena Polenová, Radomír Porubský, Rastislav Prelec, Mária Slováčková, Dáša Šulejová, Vladimír Tesař, Mikuláš Tučnák, Zuzana Zemanová

4.D</br>
Triedna profesorka: Elena Miškovičová</br>
Marta Bendisová, Erika Dekanová, Katarína Fülöpová, Roman Havlíček, Eva Horváthová, Elena Hudecová, Ingríd Hujová, Katarína Huličková, Peter Iványi, Zdena Končeková-Filová, Ivica Lednická, Mária Miklerová, Peter Pálka, Juraj Pavlinec, Oľga Poláčeková, Iveta Radosová, Alena Sedláková, Vladimír Ťažký, Vladimír Zelenák

4.E</br>
Triedny profesor: Peter Schotter</br>
Dagmar Ballová, Želmíra Bellová, Roman Bober, Martin Bobok, Peter Čaniga, Peter Čulík, Jana Gardiánová, Pavol Griač, Damask Gruska, Martin Holec, Igor Holländer, Zuzana Jaloviarová, Mária Koleničová, Henrieta Krištofova, Milada Langerová, Andrej Leško, Soňa Mamrillová, Peter Mandík, Ľubica Miklovičová, Elena Mikulajová, Katarína Novomeská, Wanda Ondreičková, Miroslav Palát, Katarína Pavúkova, Peter Pecho, Tatiana Petrášová, Alica Rosová, Darina Slezáková, Alena Šárová, Aida Šebestová, Zuzana Šepáková, Lubor Šešera, Jozef Štefanovič, Ondrej Štrauch, Peter Tomcsányi, Zuzana Zámečníková

AK - nadstavbové štúdium so zameraním na programovanie a obsluhu počítacích strojov</br>
Triedna profesorka: Oľga Grmanová</br>
Dagmar Balhárová, Melánia Bálintová, Vladimír Blaha, Daniela Cyprichová, Peter Fülöp, Tatiana Havettová, Daniel Haberland, Peter Hofman, Erich Hojčka, Alena Ješková, Anna Kašubová, Peter Kavecký, Erika Kellnerová, Tibor Kiss, Klára Kittlerová, Iveta Kosíková, Katarína Kozáková, Branislav Kundrák, Darina Kusá, Tibor Liszy, Jozef Mach, Juraj Miklos, Ľudovít Molnár, Alena Nagyová, Miloš Nitran, Dagmar Nováková, Ján Papoušek, Juraj Rydlo, Katarína Rumanovská, Hilda Švecová, Magdaléna Tóthová, Eva Vachalíková, Mária Zaťková, Peter Zajac