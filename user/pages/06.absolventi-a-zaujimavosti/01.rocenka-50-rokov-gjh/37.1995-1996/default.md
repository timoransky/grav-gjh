---
title: 1995/1996
---

Školský rok 1995 - 1996

4.A</br>
Triedna profesorka: Klára Grečnerová</br>
Michal Barlok, Richard Bořuta, Richard Čerhit, Katarína Dudáková, Ivan Červeň, Peter Hronček, Eva Chanasová, Veronika Ižová, Martin Kanich, Peter Kotolan, Pavol Kováč, Zuzana Kršková, Branislav Květoň, Martin Marko, Lenka Miklošová, Barbora Murgašová, Andrej Netri, Peter Neubauer, Eva Obrancová, Peter Olah, Martin Plesch, Pavol Polgár, Tomáš Potúček, Vladimíra Schlöglová, Snežana Schotterová, Karolína Sobeková, Gabriel Sobota, Pavol Stašík, Matúš Straka, Michal Šárnik, Zuzana Šefčovičová, Rastislav Šutta, Ondrej Turza, Lucia Zaťková

4.B</br>
Triedna profesorka: Ľubica Hafnerová</br>
Michaela Áčová, Branislav Bložon, Ľubor Čunderlík, Tomáš Dermišek, Michal Fischer, Branislav Greger, Igor Hantuch, Ján Hefty, Ľuboš Horváth, Mikuláš Chovanec, Martin Jandačka, Miroslav Kamenský, Andrea Kinderová, Radoslav Kováč, Jaroslav Kovář, Matúš Kráľ, Dušan Lacko, Ľuboš Lednár, Lucia Lexmannová, Martin Lohnert, Jozef Matlovič, Martin Mucha, Ľubomír Nerád, Martin Partyk, Martin Pilka, Katarína Ravasová, Martin Ret, Matej Reviliak, Ľubomír Ruttkay, Lucia Sestrienková, Peter Schmidt, Katarína Škripeňová, Ondrej Smutný, Richard Marko, Diana Sučanská

4.C</br>
Triedna profesorka: Mária Križanová</br>
Zuzana Blažejová, Ján Borovský, Zoltán Boršoš, František Cedula, Juraj Číž, Silvia Dvorská, Milan Dzilský, Michal Ďurdina, Čestmír Hýbl, Šimona Chrenková, Mária Koledová, Monika Kuhajdová, Peter Lauček, Juraj Matula, Juraj Onruš, Zuzana Pirchanová, Ľubomír Polonec, Tomáš Pour, Milan Regec, Juraj Richter, Róbert Rozboril, Jana Ružická, Andrej Sládeček, Miroslava Smitková, Róbert Solčan, Stanislav Suchovský, Juraj Šarkan, Mário Škrovánek, Svorad Štolc, Radoslav Štuhl, Lenka Vágnerová, Mária Vajteršicová, Juraj Valent, Róbert Vehner, Michal Zeman, Pavol Zemaník

4.D</br>
Triedna profesorka: Viera Smolková</br>
Peter Adamčík, Veronika Babušíková, Matej Bartoš, Viktor Bálint, Tomáš Beňa, Martin Bielik, Rastislav Blišák, Ján Caletka, Miroslav Endrych, Peter Fabian, Róbert Fric, Jasin Hamid, Monika Javoreková, Peter Jáni, Juraj Kobza, Silvia Kováčiková, Eva Kováčová, Miloš Križan, Michal Lindner, Andrea Marochničová, Gregor Mistrík, Miroslav Morávek, Vladimír Novák, Oto Papp, Ondrej Parízek, Michal Prónay, Ivan Prutkay, Vladimír Scholtz, Stanislav Šimuna, Marek Šmelík, Filip Vadovický, Ivan Valašík, Martina Virágová, Martin Viglaský

4.IB</br>
Triedna profesorka: Renáta Balková</br>
Ester Bébarová, Petra Brežná, Rastislav Cesnek, Kamila Černeková, Katarína Holčíková, Martina Hunová, Vladimír Ježík, Silvia Kováčiková, Tomáš Kudláč, Katarína Lalíková, Martina Lapšanská, Michal Lesay, Katarína Litterová, Ivan Majerčák, Jana Majerníková, Peter Richtárík, Michal Rosík, Michaela Schöberová, Róbert Stach, Adriana Švarcová, Miroslav Toma, Vladimír Vaňo, Zuzana Zorkovská, Marek Žitňanský