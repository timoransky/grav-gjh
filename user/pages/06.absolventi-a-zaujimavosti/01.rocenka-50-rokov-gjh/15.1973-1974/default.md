---
title: 1973/1974
---

Školský rok 1973 - 1974

4.A</br>
Triedny profesor: Jozef Balala</br>
Zdenko Alexy, Juraj Babiak, Igor Bakoš, Denisa Belišová, Peter Cichra, Miloš Dekánek, Marta Görfölová, Milan Hladký, Ferdinand Hriadel, Alena Jungmayerová, Juraj Kabina, Anna Kalníková, Dušan Klusák, Miloš Krajčovič, Juraj Kadlec, Jozef Kučeravý, Oľga Kučerová, Radomil Květoň, Dana Matušková, Tibor Melíšek, Rudolf Nagy, Bernadetta Otrubová, Mária Paulenová, Mária Pjechová, Mária Polková, Daniel Prekop, Ľuboš Púčik, Bohuslav Spišiak, Viera Šándorová, Anton Šmid, Juraj Špilka, Ján Tirjak, Eleonóra Valašeková, Jozef Virec

4.B</br>
Triedny profesor: Július Šoltés</br>
Mikuláš Bakay, Peter Bauer, Miroslav Belica, Ján Bilík, Martin Bišťan, Miloslav Brunclík, Margita Debnárová, Michal Dibarbora, Martin Dvořák, Tibor Foltin, Štefan Gális, Peter Gavaľa, Juraj Hargaš, Ján Hianik, Marta Hincová, Ľubomír Chovan, Dana Kmecová, Miroslav Knapec, Ľubor Kollár, Pavel Kořínek, Gabriela Kramáriková, Ján Krč, Mária Máleková, Pavol Meravý, Štefan Olejník, Bohuslav Partyk, Peter Paulinský, Rudolf Pokorný, Pavol Poliak, František Pšenko, Jozef Širáň, Pavol Tekula, Štefan Varga, Milan Wendl, Pavol Zlatoš

4.C</br>
Triedna profesorka: Viera Hánová</br>
Tatjana Andrusovová, Fedor Brandobur, Jozef Bročko, Igor Broska, Ján Brúska, Ivan Cambel, Ivan Doležal, Jozef Dóša, Peter Duda, Peter Fabiánek, Stanislav Fekete, Miroslav Frecer, Eva Grenčíková, Ján Halász, Tibor Jakubis, Andrej Kadlic, Róbert Káčer, Dana Karellová, Milan Kekeňák, Karol Košuk, Ružena Kováčová, Milan Kučera, Jana Lišková, Pavol Maňásek, Ján Mihálik, Jaroslav Mikloš, Marica Mistríková, Tomáš Parízek, Jozef Pener, Peter Pokorný, Zuzana Pukančíková, Helena Sarnová, Jana Skalská, Helena Šipková, Ivo Špányi, Marta Ulrychová, Elena Vajciková, Darina Valeková, Ľubica Veličová, Elena Voderková

4.D</br>
Triedna profesorka: Taťjana Lörincová</br>
Babor Bystrík, Beata Begányiová, Katarína Brestovanská, Ján Breštenský, Juraj Buriánek, Jana Čelková, Peter Drdúl, Ľuboš Dudík, Ivan Fraas, Kristína Hornová, Milan Hutta, Tibor Chebeň, Marieta Jablonská, Daniela Kubíková, Peter Macko, Alexander Marčák, Kristína Neuová, Vladimír Pavlík, Milan Poliak, Kata Repášová, Zuza Schwartzová, Viera Skákalová, Marián Sládek, Miroslav Slávik, Vladimír Tikl, Vladimír Vereš, Vladimír Vorobjov, Dušan Vrabec, Zuzana Zajacová, Jozef Zemánek

4.E</br>
Triedna profesorka: Emília Hrnčiríková</br>
Jarmila Belková, Darina Bieleschová, Eva Candráková, Magdaléna Bridová, Juraj Dobiš, Karol Dubecký, Andrej Ferko, Zuzana Gidová, Miroslava Hégerová, Jindra Hermanová, Katarína Hlaváčová, Eva Jakubovie, Marta Koleková, Viera Kopčoková, Tamara Kostovská, Michaela Kubovičová, Ingrid Lodererová, Zuzana Machánková, Bibiána Minďašová, Eleonóra Mlčuchová, Emília Mosná, Žofia Murgašová, Elena Ondrušková, Dagmar Pagáčová, Viera Pažitnayová, Zuzana Pindúrová, Mária Práčková, Izabela Rajnová, Edita Róvóová, Vladimír Roy, Ladislava Sedlíková, Magdaléna Sekáčová, Eva Slovjaková, Ján Šmarda, Adela Tučková, Eliška Vaculková, Viola Vanáková, Oľga Volárová, Alena Zervanová

AK - nadstavbové štúdium so zameraním na programovanie a obsluhu počítacích strojov</br>
Triedny profesor: Ondrej Demáček</br>
Zuzana Baloghová, Barnabáš Baráti, Mária Bošanská, Ľubomír Danko, Štefan Dolinský, Ladislav Drapka, Mária Fonódová, Petko Fodorov Goridkov, Zuzana Herineková, Mária Hodálová, Juraj Hoppan, Magdaléna Horváthová, Katarína Hrehorová, Gejza Ilčík, Tibor Jakubík, Anna Jasenáková, Ľubica Klčová, Branislav Kovašič, Oľga Královičová, Stanislav Krchnák, Dagmar Krížová, Jana Kulichová, Marián Lavo, Mária Lednická, Daniela Markova, Ladislav Mikuš, Peter Moravčík, Gizela Nevériová, Michal Oravec, Ján Oslanec, Marta Pokorná, Vlasta Polakovičová, Ladislav Rabatin, Miroslava Ričková, Viktor Roman, Viera Svítková, Tibor Szarvas, Miloš Švantner, Eduard Timko, Július Zapletal