---
title: 1985/1986
---

Školský rok 1985 - 1986

4.A</br>
Triedna profesorka: Oľga Grmanová</br>
Jela Adamčová, Oľga Baginová, Tatiana Balážová, Jana Baráthová, Samuel Brečka, Monika Buzalková, Viera Deáková, Jana Dorotková, Igor Faktor, Zuzana Horváthová, Mária Ivicová, Rastislav Janota, Jana Januschkeová, Rastislav Korienek, Oxana Kosiačková, Helena Kozáková, Zuzana Langerová, Radoslav Lellák, Vladimír Magyar, Katarína Miklová, Monika Nemčeková, Milada Neméthová, Alexandra Orbanová, Renáta Petríková, Daniela Šefránková, Martin Sepp, Gabriela Siváková, Ján Sláva, Eva Slezáková, Jaroslav Sobotka, Richard Somoš, Stanislav Stowasser, Soňa Suchá, Jozef Šarina, Peter Šrank

4.B</br>
Triedny profesor: Július Šoltés</br>
Michal Balan, Alan Bílý, Daniel Bobok, Vladimír Čerňanský, Pavol Červeň, Tatiana Čipkárová, Tomáš Fischer, Juraj Griač, Branislav Grman, Juraj Haluška, Imrich Hybký, Hana Ješková, Maroš Kamenský, Iveta Kandráčová, Jozef Konečný, Ivana Králiková, Richard Leščenko, Peter Lešták, Viridiana Majdlenová, Stanislav Meduna, Mária Orgonášová, Marián Orlický, Katarína Petrovičová, Ivan Polách, Vladimír Potisk, Peter Rajňák, Miloš Randák, Rastislav Sopúch, Dušan Spurný, Ivan Synek, Stanislav Šáner, Roman Šolc, Zuzana Šemmerová, Róbert Trávnik, Martin Uher, Anna Vojtková

4.C</br>
Triedna profesorka: Ľubica Bednáriková</br>
Peter Bako, Ján Bánsky, Boris Bergendi, Róbert Gál, Pavol Hrádek, Peter Jurči, Barbora Kamrlová, Márius Kopcsay, Andrej Korbely, Tomáš Lencz, Jana Lešková, Peter Marek, Juraj Nosko, Dagmar Palkovičová, Eva Pechová, Katarína Pořízková, Miloš Prelec, Bojan Radovanovič, Róbert Schenk, Peter Soldán, Barbora Šajnohová, Peter Štefánek, Helena Takácsová, Peter Urban, Dušan Vígľaský, Milan Zachar, Juraj Žídek

4.D</br>
Triedna profesorka: Viera Eližerová</br>
Marek Bajza, Peter Bakoš, Beáta Belicová, Peter Cecko, Pavol Dobrodský, Ľudmila Ďurčanská, Marián Hatiar, Eva Izáková, Michal Lisý, Vladislav Matej, Zuzana Maurery, Danica Mičudová, Margita Mikulová, Róbert Nogell, Andrea Novomeská, Rastislav Pálka, Juraj Pavlovský, Zuzana Sniščáková, Katarína Solčanská, Róbert Steyrer, Slavomíra Šeligová, Martin Šimúth, Aleš Široký, Peter Ťažký, Igor Tešovič, Aleš Tomeček, Branko Trančík, Martin Vass, Dagmar Vrábliková, Juraj Zachar