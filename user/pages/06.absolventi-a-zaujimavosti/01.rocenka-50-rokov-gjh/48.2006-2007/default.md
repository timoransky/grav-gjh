---
title: 2006/2007
---

Školský rok 2006 – 2007

4. A</br>
Triedna profesorka: Zuzana Čontošová</br>
Ayan Arbe, Barbora Bajanová, Miroslav Bodiš, Daniel Brnčík, Peter Griač, Jakub
Horáček, Lukáš Chovan, Tomáš Karovič, Ján Kleštinec, Michal Klímek, Lucia
Krajčovičová, Tomáš Mazal, Peter Morvay, Zuzana Mosná, Vladimír Mosný, Thao Ngo
Huong, Ján Polakovič, Peter Sedláček, Robert Schmidt, Ľuboš Sliva, Ivan Stračiak, Filip
Struhár, Alexandra Šimková, Jaroslava Šnegoňová, Radoslav Šrámek, Zuzana
Štefkeová, Bohumil Trávniček, Elena Urbanová, Jana Vargová, Rudolf Vido, Miroslava
Vizváryová, Martin Vojtko, Zuzana Záhonová, Peter Zváč,

4. B</br>
Triedna profesorka: Alena Bučková</br>
Martin Baumann, Juraj Bebjak, Tomáš Borecký, Maroš Ďuríček, Peter Herman,
Angelika Herucová, Lukáš Hrbáň, Renáta Chromíková, Juraj Jakabovič, Stanislav
Kováč, Tomáš Kovačovský, Andrej Krištúfek, Andrea Kubalová, Jana Kuzmová, Tomáš
Labuda, Magdaléna Mackovičová, Tomáš Mazák, Filip Noge, Nikola Nusová, Júlia
Omastová, Martin Rusňák, Miroslava Slušná, Marián Slušný, Iveta Steigaufová, Peter
Sucha, Jana Šišmišová, Lukáš Šterba, Marcel Švec, Zdenko Tarasovič, Peter Tomek,
Natália Tunegová, Ján Vlčko, Ivana Volochová, Zdenko Vozár

Oktáva A</br>
Triedny profesor: Marián Slušný</br>
Roman Bobák, Michal Ferko, Matej Ftáčnik, František Hajnovič, Lucia Hasonová,
Vladimír Horváth, Veronika Kisková, Miroslav Kováč, Ján Kovačič, Vojtech Kubek,
Pavol Kuklica, Peter Kuklica, Denisa Lakatošová, Vladimíra Laššáková, Jana
Maďarová, Veronika Morháčová, Peter Mráz, Pavol Námer, Paula Nürnbergerová,
Michaela Obuchová, Adam Okruhlica, Zuzana Pakanová, Michal Pecho, Denisa
Poláková, Martin Procházka, Ladislav Rampášek, Ondrej Rohoň, Katarína Sénašiová,
Peter Siváček, Alexander Václavík, Martin Švec, Tomáš Varga, Matúš Vizváry, Michal
Vychytil

Oktáva B</br>
Triedna profesorka: Ivana Zajacová</br>
Adela Antalová, František Balko, Lucia Bartóková, Eva Bognárová, Ivan Buštor, Peter
Buzák, Juraj Ďurajka, Marek Havrila, Henrieta Hodáková, Martin Hrnčíř, Miriam
Chladová, Martin Jurášek, Miroslav Kaiser, Peter Kajan, Tea Kerla, Branislav Košík,
Ivana Koťková, Martin Krafčík, Tomáš Lačný, Lukáš Láni, Ivan Lapin, Maroš Mader,
Matej Malík, Michaela Miklášová, Veronika Mikušová, Veronika Mladá, Michal Nagy,
Tamara Stohlová, Rastislav Táborský, Radka Vicenová, Jozef Vojtko, Danica Zajacová,
Matúš Zaťko

4. IB</br>
Triedny profesor: Ľubomír Lanátor</br>
Júlia Adamčáková, Jakub Albert, Martin Boroš, Juraj Cvik, Peter Diţo, Andrea
Doleţálková, Peter Fabšič, Róbert Frecer, Lenka Halačová, Martina Hojčková, Simona
Hrnčíriková, Jana Janková, Peter Kraus, Diana Lörincová, Róbert Lukáč, Tímea
Lukáčová, Juraj Mach, Jana Müllerová, Martin Pavlovský, Michal Poláček, Jakub
Repiar, Šimon Soták, Petra Strapáčová, Romana Szabóová, Fedor Šimkovic, Barbora
Šútorová, Katarína Tóthová