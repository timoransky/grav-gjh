---
title: 1960/1961
---

Školský rok 1960 - 1961

11.A</br>
Triedny profesor: Dr. Anton Šíma</br>
Danica Bernáthová, Vladimír Blaho, Viola Cibulová, Júlia Gábelová, Marián Hanzalík, Viktor Hlocký, Ingeborg Huberová, Igor Hutník, Marta Izáková, Alica Jedličková, Bohumil Kráľ, Elena Kuzmová, Ivan Lacina, Peter Lieskovský, Brigita Lipecká, Edita Liščíková, Eva Matzenauerová, Margita Miklánková, Pavel Mikšík, Elena Minarovičová, Lýdia Nazarejová, František Novák, Helga Polónyiová, Edita Rajterová, Eduard Rychlovský, Mária Sekerová, Magdaléna Sluková, Milan Trebula, Jarmila Vanická, Albert Zámočník, Vlasta Zvonárová

11.B</br>
Triedny profesor: Katarína Hermannová</br>
Marta Bacová, Vladimír Baláž, Eva Baranyiová, Anna Blanárková, Ján Danko, Viola Galková, Antónia Grambličková, Danuša Horvátová, Kamil Hrebíček, Igor Janec, Viera Kollárová, Viola Krajmerová, Alžbeta Krnáčová, Taťjana Majerská, Gregor Makarian, Peter Marejka, Milan Marschall, Viola Mazurová, Edita Medzihradská, Eleonóra Miklušová, Eva Ozábalová, Rodjena Palacková, Peter Panigay, Anna Procházková, Tatiana Rysuľová, Irena Schauerová, Bohumil Sabo, Mária Šubertová, Jana Švajdlerová, Ľubica Rosová, Marta Vargová, Júlia Veselská, Lýdia Vincúrová, Róbert Wolf, Ján Zváč

11.C</br>
Triedny profesor: Jozef Balala</br>
Nadežda Draškalová, Július Alexander Ebers, Milada Fischerová, Milan Ftáčik, Ľubomír Gubič, Dagmar Gregorová, Milada Hájková, Peter Hexner, František Hrdlovič, Angela Hrozová, Mária Hudecová, Eva Cholujová, Mária Illenčíková, Marta Ivašková, Alena Jarmila Karkušová, Elena Kocianová, Margita Kozlíková, Burghardt Otto Krämer, Ľubor Lamoš, Ján Ličko, Ján Lukáč, Karol Macák, Viera Majlingová, Darina Maštenová, Eva Matelová, Margita Melišková, Dušan Mikloš, Ľubomír Mlynárik, Viera Mrázová, Ivo Petráš, Katarína Puobišová, Jozef Suchý, Peter Šamko, Darina Štefanovičová

11.D</br>
Triedny profesor: Ján Kadlečík</br>
Božena Blahová, Marta Bugárová, Zlata Caňová, Igor Červeňan, Emília Čiffaryová, Edita Diesnerová, Mária Fraštacká, Vlasta Gallová, Viera Hanečková, Ján Havelka, Ivan Huorka, Jozef Janíčkovič, Ľudmila Jergušová, Milena Kačincová, Ditta Kittová, Imrich Kollár, Tibor Kollárik, Alexandra Kremlerová, Vladimír Marko, Igor Minárik, Aladár Other, Margita Őlbstová, Júlia Paracková, Vladimír Pavlík, Igor Rosenberger, Eleonóra Rudašová, Anna Rudolfová, Agnesa Ružovičová, Peter Urbánik, Zuzanna Vidová, Eva Voleková

11.E</br>
Triedny profesor: František Mikulka</br>
Viera Baganecová, Soňa Bartošova, Elena Benková, Daniela Blechová, Mária Cettlová, Ján Čarnogurský, Peter Daučík, Zora Dvončová, Valéria Eštóková, Milan Horčičák, Marián Horský, Igor Jakubík, Otilia Kastlová, Tatiana Katyková, Štefánia Krihová, Marián Letko, Vladimír Mezera

Karol Mihálek, Jana Michalková, Dušan Mikula, Elena Novoveská, Nadežda Polakovičová, Mária Roháčová, Emil Selecký, Emília Skalská, Anna Sokolová, Ján Spevák, Peter Šikuta, Mária Štulajterová, Jana Voržáková
