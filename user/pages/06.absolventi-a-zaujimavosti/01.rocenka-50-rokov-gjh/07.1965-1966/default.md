---
title: 1965/1966
---

Školský rok 1965 - 1966

3.A</br>
Triedna profesorka: Erna Haraksimová</br>
František Alexander, Jarmila Balalova, Vladimír Bartolčič, Dana Buchelová, Eva Dudáková, Ladislav Ehn, Igor Farbák, Ladislav Györffy, Igor Hantuch, Ján Hanzel, Alexandra Havettová, Jozef Holomáň, Štefan Chudík, Stanislav Inovecký, Jozef Jamriška, Zuzana Kaliská, Peter Kollár, Stanislav Kovár, Ľubor Malina, Peter Matejčík, Peter Mikulecký, Mária Nagyová, Alojz Némethy, Zdeno Nerád, Ján Nový, Bohuslav Pavlovič, Viera Pastuchová, Brigita Petrášová, Juraj Poledna, Zuzana Polomská, Pavol Šermer, Zuzana Šimovičová, Mária Šipkovská, Vladimír Špilka, Renáta Virsíková

3.B</br>
Triedny profesor: Karol Rovan</br>
Vladimír Bartoň, Dušan Dobrovodský, Svetozár Ferenčík, Katarína Fitošová, Juraj Gross, Peter Grossinger, Ladislav Hajný, Mária Harsányiová, Martin Holézcy, Ľubomír Jalč, Ľubor Jursa, Marian Karšai, Veronika Kollárová, Ivan Kusý, Pavol Liko, Peter Marcel, Magdaléna Matejovová, Beáta Meszárosová, Peter Nemec, Ján Novanský, Július Paštrnák, Zora Paštrnáková, Ľudmila Pavlíková, Róbert Penz, Ivan Rohan, Daniela Smiešková, Daniela Straková, Norbert Szemzö, Marián Šimlovič, Pavol Valent, Kamil Vaník

3.C</br>
Triedna profesorka: Oľga Volárová</br>
Helena Adamcová, Peter Adamec, Ivan Babušík, Antónia Bačíková, Milan Baláž, Edgar Beňovský, Juraj Bilčík, Vladimír Bilčík, Peter Bluska, Peter Čekovský, Alexander Dudaško, Ľudmila Horná, Vladimír Hupka, Jana Hvojníková, Vladimír Jonáš, Edita Jurčíková, Mária Jurovatá, Jozef Kováč, Marta Kutejová, Nella Križanová, Milan Mazák, Peter Mičieta, Lea Mrázová, Milan Nemec, Gabriela Nemečkayová, Igor Niepel, Ľubica Oríšková, Milan Orsáry, Pavol Sýkora, Gabriela Sklenáriková, Ladislav Stréber, Magda Šinkovičová, Eva Vojtechová

3.D</br>
Triedna profesorka: Zora Pašková</br>
Marianna Arpášová, Vladimíra Bartoňková, Jelena Bosioková, Peter Bruckner, Karok Bílsky, Ivan Bystranin, Helena Černáková, Eva Gáliková, Eva Halásová, Vladimír Horský, Igor Hrušovský, Peter Kalina, Tomáš Kamenský, Milan Kánya, Peter Knechtsberger, Alexander Knorr, Katarína Kobzová, Zdena Kozáčková, Miroslav Krumpál, Ľudmila Kubovčáková, Oľga Kučerová, Viera Letanová, František Lipčík, Juraj Lipscher, Milada Macejovičová, Daniela Mandíková, Pavol Mihálek, Viera Mišeková, Šarlota Námerová, Mária Nesseumannová, Jaroslava Osvaldová, Daniela Paráková, Katarína Rudová, Alžbeta Pašková, Stanislav Stríž, Zuzana Svatošová, Marian Šaling, Mária Turoňová, Elena Vancová, Angelika Zámocká, Jaroslav Ždiňák

3.E</br>
Triedna profesorka: Elena Weberová</br>
Mária Árpová, Felícia Báchorová, Alojz Blahuta, Taťjana Čádyová, Ivan Čeman, Daniela Glausová, Alena Grohmannová, Jana Hovančíková, Eleonóra Haimová, Brigita Hybelová, Svetozár Chabada, Oľga Janečková, Karol Juhari, Vlasta Kabátová, Katarína Kášová, Ladislav Klementis, Viera Korcová, Eva Korecká, Helena Kunšteková, Štefan Labuš, Veronika Lesáková, Magda Mackovýchová, Daniela Miklášová, Anna Orčíková, Alžbeta Pamulová, Viera Pechancová, Danuša Petříčková, Zlatislav Podhorský, Pavol Sadák, Oľga Srníková, Anna Uhlíková, Ivan Vážny, Dagmar Vejříková, Lucius Vika, Ján Vizner, Boris Zábrodský, Ružena Závadová, Marián Zelenský, Anna Zervanová, Vladimíra Zimmerová

3.F</br>
Triedna profesorka: Zlatica Reháčková</br>
Mária Balážová, Hana Danáková, Alžbeta Ďurďáková, Jana Ferancová, Jaroslava Ferancová, Viera Ficelová, Viera Flöglová, Soňa Gálová, Stela Gálová, Anna Hanesová, Anna Hírešová, Emília Chrenková, Oľga Janáčková, Eliška Jantnerová, Anna Kaffková, Ľudmila Kavická, Daniela Koscivová, Alžbeta Kusá, Oľga Matrková, Eva Menšíková, Jana Mistríková, Marta Molnárová, Oľga Nižňanská, Viera Ondriašová, Marta Pelikánová, Marta Preinerová, Ivana Royková, Zuzana Rudová, Alžbeta Sedláčková, Dagmar Smolínska, Katarína Szantová, Eva Svrčková, Eva Uhrová, Kveta Valušiaková, Mária Valušiaková, Zdena Vilková, Mária Vulganová, Oľga Výrostková, Daniala Žáčková

3.G</br>
Triedny profesor: dr. Anton Šima</br>
Elena Bednárová, Alexander Bugár, Božena Čavaňáková, Michal Čierny, Sylvia Dillnbergerová, Danica Fridrichová, Elena Globanová, Pavol Guttmann, Štefánia Haklová, Mária Holzingerová, Oľga Chomová, Zuzana Chorváthová, Marta Jordanová, Marta Kolláriková, Elena Kubánová, Katarína Margótsyová, Dušan Meliš, Zuzana Mešťanová, Igor Mydlík, Ilona Neprašová, Anna Paškayová, Eva Patková, Gabriela Pulpitlová, Eva Roháľová, Magdaléna Salvová, Maianna Sedláčková, Jana Sluková, Mária Števurková, Viola Tomašovičová, Zlatica Vadkertyová, Ľubica Velková, Judita Vráblová, Andrea Záborská, Igor Zajac, Jozef Zummer

3.Š</br>
Triedny profesor: Július Šoltés</br>
Elena Bachratá, Eduard Benda, Pavol Butvin, Miroslav Fencl, Pavol Fischer, Ivan Georgiev, Soňa Glézlová, Štefan Gramblička, Eduard Hozlár, Dorota Kováčova, Roman Lazar, Jaroslava Margitánová, Helena Mikľová, Pavol Peťovský, Ján Pobežal, Juraj Steiner, Jozef Szigeti, Ľudmila Števovičová, Viera Vydarená, Ján Zajac, Mária Žibritová

AK - nadstavbové štúdium so zameraním na programovanie a obsluhu počítacích strojov</br>
Triedny profesor: dr. Zdeno Vlachynský</br>
Drahomíra Behúlová, Katarína Borková, Margita Bražinová, Zlatica Čierna, Anna Ďurčeková, Anna Fiedlerová, Mária Gabková, Belo Handl, Ľubomíra Hájková, Dezider Hiros, Ján Horan, Jolana Hrabová, Jiří Chochol, Jana Jakabová, Karol Kavec, Alžbeta Knapová, Mária Kocúnová, Daniela Kovárová, Ľubica Kubíková, Terézia Miškovičová, Vlasta Pažitná, Oľga Rybanaská, Emília Slottová, Ján Sopóci, Gustáv Špak, Oľga Štegerová, Oľga Šullová, Mária Tóthová, Ivan Velčický, Marta Zápražná