---
title: 1968/1969
---

Školský rok 1968 - 1969

3.A</br>
Triedny profesor: Ľubomír Krištof</br>
Viliam Aufricht, Mária Bilíková, Mária Blažková, Roman Cisár, Jozef Čačko, Juraj Dubay, Juraj Fest, Milan Filadelfi, Ivan Hrmo, Ladislav Hudec, Vladimír Hudek, Viera Chocholáčková, Juraj Institoris, Anna Ivanová, František Kocmunda, Jozef Križanský, Karol Krupa, Eduard Lifka, Milan Mastihuba, Radko Mesiar, Miroslav Milán, Jozef Mokrý, Peter Némethy, Ľudovít Niepel, Darina Pračková, Anna Sirová, Martin Terray, Daniela Vaňová, Jozef Víšek, Ivan Zvada

3.B</br>
Triedna profesorka: Zlatica Reháčková</br>
Eva Baníková, Imrich Bertók, Jiří Bouška, Jozef Brestenský, Ladislav Brimich, Elena Candráková, Tibor Čellár, Juraj Čulman, Ľubomíra Fitošová, Juraj Földes, Tamara Haverlíková, Hviezdoslav Herman, Oľga Holičová, Čestmír Hýbl, Iveta Jaceniaková, Peter Kompiš, Július Kotzig, Miroslav Kovanič, Peter Kovár, Mirjam Lešková, Mária Májeková, Anna Mašejová, Alena Osičková, Peter Parízek, Blažena Rovanová, Bronislava Sarnová, Eva Šipková, Tatiana Šutková, Pavol Tekeľ, Dana Tkáčová, Pavol Tomasta, Zuzana Tóthová, Veronika Valachová, Jozef Valentovič, Mária Zelenková, Peter Zíka

3.C</br>
Triedna profesorka: Helena Kotzigová</br>
Zdena Babčanová, Michal Beliš, Peter Bella, Peter Čunderlík, Ľudovít Drus, Zuzana Ficelová, Pavol Forro, Mária Hajachová, Peter Hanzák, Peter Hensch, Tomáš Horňák, Oľga Housková, Zuzana Kandráčová, Elena Klačanská, Samuel Krúpa, Ján Kubis, Oľga Lackovičová, Emil Mayer, Pavol Mikulaj, Vladimír Pánik, Gustáv Pastucha, Viliam Perknovský, Eva Píšťková, Dušan Rosa, Martin Rusnák, Vladimír Rychlík, Pavol Sečkár, Ľubica Stašíková, Marián Supek, Vladimír Sýkora, Jana Šovčíková, Beáta Šteinová, Gabriela Tatarková, Jana Zábojníková, Zuzana Zábudlá, Ivan Žáry

3.D</br>
Triedna profesorka: Zora Pašková</br>
Viera Belicová, Zuzana Brestovanská, Dana Ededyová, Ján Fajdel, Katarína Gajdošíková, Bohumil Gašpierik, Eleonóra Haulišová, Terézia Havrillová, František Huňa, Alžbeta Hupková, Danica Hrivnáková, Viera Ivaničová, Jana Jančeková, Ján Janík, Eva Klačanská, Ivan Klein, Zuzana Kobzová, Dušan Kolenič, Igor Krumpál, Tatjana Lošáková, Dušan Malík, Juraj Michálek, Silvia Michálková, Ladislav Michlík, Emil Pažický, Alexander Pěkov, Ľudmila Porubská, Michal Potfaj, Michal Ročák, Uľjana Sekajová, Mikuláš Straca, Květa Sumbalová, Živa Sumbalová, Ján Šimaljak, Ivan Šimkovic, Marián Šulík, Ivan Takáč, Ladislav Valent, Michal Vanca, Mojmír Vodička, Jozef Weber, Rudolf Zajac, Jana Zvarová

3.E</br>
Triedna profesorka: Zuzana Orovanová</br>
Ján Bahna, Jana Bečková, Katarína Bendová, Katarína Bognárová, Róbert Brežný, Eva Dodeková, Ľubica Dračková, Ľubica Faiglová, Róbert Francisci, Galina Gajdošová, Pavol Gettler, Štefan Holovič, Juraj Jankela, Ján Ježek, Vladisláv Jurický, Katarína Karpišová, Ján Kekeňák, Gabriela Kellová, Peter Klačanský, Anna Kobelová, Soňa Kudličková, Vincent Lisy, Dagmar Masárová, Štepánka Mečířová, Vladimír Mikláš, Oľga Mináriková, Ondrej Mračka, Gabriela Námerová, Ľubomír Oláh, Štefan Paško, Mária Pišoftová, Anton Roman, Ľubica Ružeková, Jozef Schramm, Ľuboš Sivek, Jaroslav Strážay, Daniela Šinkovičová, Pavol Šoltés, Peter Špaňo, Marta Varadinová, Milan Vojtech, Emil Vržďák

3.F</br>
Triedny profesor: Karol Rovan</br>
Štefan Bohucký, Viera Braunová, Marcela Dullová, Martin Ebergényi, Ľubomír Fajták, Bruno Chmel, Ľubica Chorváthová, Terézia Jakubcová, Milan Kasanický, Eleonóra Kastelová, Darina Kašičková, Milena Kočišová, Milan Kopecký, Ľubica Korbačková, Katarína Krišťáková, Juraj Lajda, Soňa Lányiová, Vladimír Lipscher, Katarína Lukáčová, Ľubica Luknišová, Marta Májeková, Virginia Mislovičová, Magdaléna Murčová, Pavol Parízek, Ľubica Peterajová, Katarína Pivoňková, Juraj Podolan, Tatiana Riečanová, Elena Spišáková, Jana Studená, Viera Šebestová, Michal Šebo, Vladimír Toma, Ján Ursíny

3.G</br>
Triedna profesorka: Elena Weberová</br>
Mária Buchelová, Rudolf Burdy, Jozef Csöllei, Jaroslava Čelková, Zora Denická, Alžbeta Fialová, Soňa Háblyová, Zdena Havlíčková, Viera Hvizdáková, Anna Hubíková, Michal Chovan, Eva Churová, Daniela Jesenská, Eva Jurkovičová, Oľga Kleinová, Darina Krištofíková, Alžbeta Konrádová, Martin Koštiaľ, Mária Kováčiková, Viera Macurová, Jana Máčalová, Marianna Marková, Dagmar Medveďová, Bohuslav Mikelka, Gabriela Patoprstá, Daniel Rašlík, Ladiskav Rampašek, Lýdia Rybárová, Alena Slováková, Magda Suchovská, Daniela Székelyová, Daniela Švehlová, Mária Valentínyová, Pavol Valuška, Viera Velgosová, Oľga Vikartovská

AK - nadstavbové štúdium so zameraním na programovanie a obsluhu počítacích strojov</br>
Triedny profesor: Zdeno Vlachynský</br>
Mária Bajusová, Júlia Bauerová, Daniel Béder, Oľga Benčatová, Žolt Bene, Peter Bičan, Gabriela Csutorová, Mária Dovičovičová, Ján Engler, Peter Fedák, Ľubomír Galáš, Alexander Gmiterko, Magdaléna Gregorová, Anna Hricíková, Berta Janíková, Dušan Kráľ, Ľudovít Mózeš, Eva Mrázová, Mária Papaneczová, Jiří Pataki, Miroslava Pomajbová, Anna Ryzeková, Monika Sálusová, Miroslav Smékal, Markéta Stašíková, Juraj Sulík, Ján Šúr, Richard Vančo, Oľga Vitálošová, Darina Zámečníková