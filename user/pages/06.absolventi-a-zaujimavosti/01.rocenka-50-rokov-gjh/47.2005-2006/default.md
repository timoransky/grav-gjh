---
title: 2005/2006
---

Školský rok 2005 – 2006

4. A</br>
Triedny profesor: Tomáš Slezák</br>
Michal Baláž, Robert Bielik, Ivana Čiháková, Eva Demuthová, Michal Dobiaš, Eva
Dukátová, Monika Foltýnová, Petra Hochmannová, Juraj Holec, Jakub Imriška, Michal
Jarábek, Lukáš Kalivoda, Ján Klíma, Vladimír Klimo, Jakub Kollár, Matej Krchniak,
Marika Kuboňová, Slavomíra Lenhardtová, Peter Líška, Ivan Lizák, Michal Lohnický,
Michal Mitterhauszer, Huy Nguyen Nhat, Vladimír Oremus, Eva Palkovičová, Dušan
Palo, Pavol Panák, Martin Sedlák, Lucia Stráňaiová, Tereza Števíková, Martin Štiglic,
Daniel Švoňava, Boris Valentín, Soňa Vavríková, Zdenko Weidler

4. B</br>
Triedna profesorka: Danica Olexová</br>
Tomáš Ábel, Igor Andris, Ivona Bíziková, Tomáš Bunček, Tomáš Cielontko, Michal
Csáder, Adam Dobšovič, Tomáš Drahoš, Martin Galla, Martin Gottweis, Andrea
Halačová, Radovan Halamiček, Alexandra Hanusková, Eugen Harton, Pavol Hrčka,
Kristína Jasencová, Ľubica Marušiaková, Andrej Morávek, Iveta Nagyová, Ľubomír
Novák, Jakub Parák, Barbora Pinčíková, Matej Polievka, Marek Rakús, Matej Roško,
Matej Sabo, Kristína Saxunová, Lucia Schmidtová, Lea Skýpalová, Martin Stopka,
Štefan Šafár, Martina Vyskočová, Richard Weber, Jana Zlatošová

Oktáva A</br>
Triedni profesori: Alena Hechtová, Radko Ambrózy</br>
Michal Béder, Dalibor Fondrk, Pavol Helebrandt, Dáša Hrapková, Tomáš Jurík, Filip
Koluš, Michal Krajčí, Martina Kubincová, Barbora Kubošková, Hoai Le Anh, Karolína
Lehotská, Martin Marčák, Samuel Ondrušek, Barbora Palátová, Martin Pastorek, Renáta
Rehorovská, Juraj Spusta, Peter Šimko, Radoslav Škultéty, Martin Thurzo, Martina
Trochtová, Martina Vargová, Denisa Villarisová

Oktáva B</br>
Triedna profesorka: Andrea Šrámková</br>
Beno Miroslav, Daniel Bundala, Ivana Danielová, Viliam Dillinger, Juraj Falťan, Kamila
Hollá, Tomáš Kešjar, Marek Mego, Katarína Náglová, Miloš Nováček, Lukáš Orihel,
Tomáš Paulik, Martin Rehák, Juraj Schiffer, Anna Sukniak, Miroslav Sýkora, Peter
Šurányi, Karol Vosyka

Oktáva C</br>
Triedni profesori: Ivana Pichaničová, Martin Fajkus</br>
Daniel Ban, Stanislav Beko, Peter Bella, Dominika Compelová, Zuzana Černáková,
Hana Darmová, Juraj Drábik, Lenka Galbavá, Lenka Hajnovičová, Katarína Hučková,
Michal Jesenský, Michal Kiča, Andrej Koleda, Pavel Koţík, Eva Kubovčíková, Barbora
Lebedová, Barbora Matečná, Martina Obloţinská, Andrej Ochaba, Peter Podhradský,
Izabela Riečanová, Ján Satinský, Stanislav Smolár, Nicoletta Socratousová, Peter
Tureček, Eva Vallová, Silvia Zmeková

4. IB</br>
Triedni profesori: Alexander Sopkovič, Elena Vojtelová</br>
Lucia Ambrošová, Gabriela Augustínyová, Matej Benka, Martin Baláţ, Lucia Banáková,
Dagmar Belešová, Andrea Bolgáčová, Veronika Grausová, Marcela Hrdá, Katarína
Chalupková, Filip Jelínek, Ľuboslav Karkalík, Zdenka Kissová, Alexis Denigh
Knappenberger, Martina Knopová, Lucia Kopecká, Peter Kóša, Adela Kuzmiaková,
Michal Labík, Martina Lišková, Michaela Mackovičová, Zuzana Masárová, Grigorij
Mesežnikov, Katarína Miklovičová, Andrej Mikolášik, Jana Musilová, Jan Páleš, Jakub
Pánik, Anna Proksová, Martin Reguli, David Rintel, Michaela Stovičková, Milan
Székely, Roman Táborský, Matej Varga, Ján Voderadský, Martin Vojtela