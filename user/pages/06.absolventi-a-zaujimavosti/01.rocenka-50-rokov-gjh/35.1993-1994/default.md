---
title: 1993/1994
---

Školský rok 1993 - 1994

4.A</br>
Triedna profesorka: Eugénia Sýkorová, Daniela Ďurajková</br>
Andrej Bachár, Andrej Baliga, Martina Bernáthová, Matej Beňa, Pavol Bluska, Patrik Brachňák, Veronika Grešová, Dáša Grünfeldová, Tomáš Horák, Zuzana Hornáčková, Silvia Chudá, Ivan Ivan, Zuzana Kanisová, Juraj Kotian, Martin Koubek, Dagmar Králiková, Richard Lacko, Dana Magulová, Patrik Malec, Branka Martinovičová, Lubomir Okrúhlica, Zuzana Ondrejková, Vladimír Pacák, Márius Paliga, Pavol Pauliny, Jiří Poláček, Martina Práznovská, Daniela Rauová, Branislav Rebroš, Róbert Rajchl, Peter Szabo, Andrej Šporer, Marián Varga, Anna Zajacová, Ľubomír Zvolenský, Daniel Živica

4.B</br>
Triedny profesor: Ondrej Demáček</br>
Ján Baráth, Roman Bartíšek, Štefan Bellus, Roman Body, Tomáš Branický, Bronislava Brejová,. Štefan Buchta, Lucia Hlavačková, Peter Chovanec, Juraj Kamenský, Jana Kordošová, Michal Kostovský, Emília Kouřílová, Eleonóra Krčméryová, Zuzana Kušnieriková, Branislav Kuvik, Daria Leváková, Tibor Liptaj, Dávid Machajdik, Michal Michalko, Zenon Mikle, Andrej Mikuš, Matej Mráz, Ondrej Muravský, Milan Mušec, Boris Petráš, Branko Petro, Zuzana Pokojná, Vladimír Procházka, Adriana Prváková, Patrik Sučanský, Marek Štefánek, Kamila Štullerová, Erika Takácsová, Lucia Tarábková, Martina Totkovičová, Martina Vančíková

4.C</br>
Triedna profesorka: Jozef Varga, Zuzana Mináriková</br>
Tomáš Bašnák, Bronislava Benkovičová, Zuzana Bezeková, Michal Blažej, Katarína Čermáková, Martin Dolník, Mária Fuknová, Svetozár Gálik, Silvia Harmathová, Igor Hudcovský, Marek Jankovič, Daniel Jursík, Slávka Karnasová, Marek Kočan, Martin Kočan, Annamária Koubeková, Marián Kováčik, Monika Kováčová, Gabriel Králik, Martin Latečka, Martin Mach, Igor Macháč, Tibor Mencl, Martina Mičková, Andrej Mráz, Tomáš Ogrodník, Rastislav Pechr, Marián Ružič, Adam Sádovský, Marián Šimo, Katarína Šimovičová, Michal Turzo, Gabriela Václavíková, Andrea Vojtechová, Rastisalv Žabka

4.D</br>
Triedna profesorka: Alena Bučková</br>
Andrej Banáš, Oľga Bosáková, Michal Čisárik, Martin Driensky, Martin Duffek, Silvia Fábryová, Martina Freudenschussová, Daniel Horák, Tomáš Horniš, Andrea Juhászová, Andrej Jursa, Michal Klima, Pavel Kočí, Martin Kollár, Róbert Kováč, Jana Krivošová, Robert Longauer, Jozef Marko, Darina Martinčeková, Viera Mikesková, Juraj Murín, Marián Parajko, Vanda Poliaková, Rastislav Procházka, Lenka Procházková, Marián Ralbovský, Stanislav Rendek, Martin Sladký, Jana Slivková, Rastislav Valentovič, Alexandra Vrebová, Katarína Zaťková

4.E</br>
Triedny profesor: Peter Demkanin</br>
Martin Alexy, David Antošík, Lucia Balanová, Pavol Benovics, Martina Blažeková, Pavel Fecko, Svetlana Gavorová, Katarína Haladová, Zuzana Hoffmannová, František Ikrényi, Katarína Kabáthová, Tomáš Kalavský, Peter Kobza, Ján Kolc, Andrea Kramaričová, Radovan Kubiš, Alena Maarová, Dušan Marko, Michal Matula, Andrej Mikloš, Eva Mistríková, Juraj Mlčoch, Matej Mlčúch, Zuzana Nagyová, Peter Neštepný, Elena Novotová, Martina Plačková, Ivan Prcúch, Dana Rafčíková, Tomáš Schlögl, Martina Slobodová, Marián Spišiak, Michaela Tornaiová, Juraj Zelman