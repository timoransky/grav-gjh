---
title: 1988/1989
---

Školský rok 1988 - 1989

4.A</br>
Triedna profesorka; Tatiana Lörincová</br>
Peter Cesnek, Katarína Číčelová, Juraj Drusc, Martin Grančič, Katarína Harbulová, Juraj Chovanec, Roman Chrást, Barbara Jakubíková, Jana Jamrišková, Gejza Jenča, Vojtech Kabáth, Martin Kalužný, Patrik Karaba, Roman Klučka, Tatiana Kollárova, Boris Krehel, Karin Kubíková, Monika Machová, Miroslav Meliško, Marta Mihaliková, Eva Mokrá, Lucia Mušková, Natália Nitranová, Viera Osuská, Drahomíra Prieberová, Patrik Rejko, Adriano Richnavský, Katarína Scheberová, Marek Stupka, Patrik Šanko, Martin Trník, Peter Turcer, Libor Udvardy, Patrik Vasil, Vladimír Záhorčák

4.B</br>
Triedny profesor: Vladimír Jodas</br>
Denis Balent, Igor Baník, Koloman Bernáth, Miroslav Cisárik, Eva Čunderlíková, Gabriela Dubecová, Alexander Fiala, Juraj Frolkovič, Irena Gersová, Ľubica Grmanová, Ľubomír Hritz, Tomáš Jakubík, Pavol Janšta, Stanislav Januschke, Jozef Juríčka, Ján Klíma, Jaroslava Kováčiková, Ľudovít Kuruc, Beatrice Kvaszová, Marián Markovič, Ilja Martišovitš, Milan Mosný, Jana Nogová, René Pázman, Ján Počiatek, Daniel Polakovič, Hana Prónayová, Vladimír Repáš, Dagmar Selecká, Lesana Schotterová, Zora Stöszelová, Gabriela Strmisková, Karin Tänzerová, Radoslav Tomek, Stanislav Trnovský

4.C</br>
Triedna profesorka: Elena Krížová</br>
Ján Čarnogurský, Marcel Dallemule, Zuzana Drobná, Michal Filip, Alexander Frický, Milan Hedera, Katarína Hojová, Radovan Jablonovský, Tomáš Kamrla, Jana Kobydová, Jana Kyšová, Ľudmila Kyšová, Katarína Liptáková, Pavol Lukšic, Stanislava Luptáková, Katarína Matheová, Katarína Mišíková, Richard Paule, Martin Plávka, Andrej Slezák, Štefan Szeiff, Radovan Šajben, Stanislava Šipkovská, František Špaček, Rastislav Tamaškovič, Branislav Vass, Andrej Vavrík, Ľuba Valachovičová, Soňa Vrábľová, Jana Výrašteková, Ján Zeleňák