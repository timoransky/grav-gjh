---
title: 2001/2002
---

Školský rok 2001 - 2002

4. A</br>
Triedny profesor: Igor Horváth</br>
Andrej Akáč, Katarína Andelová, Martin Augustin, Laura Bartová, Michal Bebjak, Andrej
Belan, Katarína Bilá, Alexander Bunčák, Štefan Dlugolinský, Mária Dzureková, Martin
Havrila, Martina Holičová, Daniela Chalániová, Pavol Kaiser, Hana Kautmanová, Filip
Kovář, Eva Králová, Pavol Macho, Martin Mrázek, Magdaléna Nagyová, Juraj
Ondruška, Tomáš Orda-Oravec, Barbora Orlíková, Pavol Ostertag, Lucia Pániková, Erik
Radnóti, Miroslava Sirotová, Michal Slodička, Natália Volárová, Katarína Vorobjovová,
Sláva Zelinová

4. B</br>
Triedne profesorky: Natália Zorvanová, Zuzana Mináriková</br>
Vladimír Benko, Linda Birnsteinová, Katarína Boteková, Ondrej Danko, Zuzana
Dratvová, František Dugovič, Petra Falťanová, Veronika Gömöryová, Michal Hajko,
Juraj Herain, Jozef Jamriška, Ján Karaba, Ivan Kohút, Katarína Kosorínová, Martina
Kostrošová, Lukáš Krajčovič, Dagmara Kruľová, Michal Lietava, Dušan Marko,
Barbora Némethová, Andrej Osuský, Lucia Plaváková, Tomáš Plch, Tomáš Poláček,
Filip Ravinger, Štefan Šurina, Peter Tuchyňa, Michal Ungvarský, Daniel Urbančík, Martin
Valdner, Michal Vereš, Mária Volentičová, Lukáš Zajac

Oktáva</br>
Triedni profesori: Ondrej Demáček, Miriam Čuntalová</br>
Ján Adámek, Peter Bella, Michal Blaško, Martin Boško, Jakub Cambel, Silvia
Droppová, Štefan Eisele, Richard Forschner, Peter Furucz, Milan Gajdoš, Marek
Hôrčik, Mikuláš Hrubiško, Karolína Husárová, Andrea Kalivodová, Matúš Klčo,
Michal Knitl, Juraj Koutný, Táňa Kováčová, Korina Krchniaková, Zuzana Kušíková,
Michal Mikuš, Michal Petro, Tomáš Pížl, Tomáš Sabo, Roman Šarmír, Martin Török,
Jozef Tvarožek, Tomáš Záhorec, Jaroslav Žigo

4. IB</br>
Triedny profesor: Alena Polakovičová</br>
Katarína Bendíková, Tamara Bobáková, Eva Bolceková, Ľuboš Bosák, Dagmara
Brichtová, Matúš Harvan, Mária Horáková, Lucia Hulková, Naďa Jašíková, Ľudovít
Kontšek, Ondrej Mihályi, Michal Miklovič, Pavol Minárik, Martin Mrázik, Júlia
Mušáková, Sana Nourani, Michal Palík, Andrea Pechová, Zuzana Petráková, Juraj
Schwarz, Natália Suchá, Radovan Šesták, Adam Široký, Elena Špániková, Lucia
Triererová, Kristína Vávrová, Lukáš Vrba, Barbora Zajacová, Róbert Žittňan