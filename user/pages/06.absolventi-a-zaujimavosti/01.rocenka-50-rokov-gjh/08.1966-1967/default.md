---
title: 1966/1967
---

Školský rok 1966 - 1967

3.A</br>
Triedny profesor: Jozef Balala</br>
Ján Blažek, Zuzana Dicková, Norbert Feitscher, Karol Fischer, Helena Garguláková, František Hajnovič, Anna Hamošová, František Havlík, Miloň Hlubocký, Lucia Holanová, Oľga Holécyová, Pavol Holič, Marián Hornáček, Mária Janečková, (Vladimír Karvaš), Fedor Mika, Eva Molnárová, Eva Osvaldová, Daniela Patoprstá, Mária Pecúchová, Helena Posluchová, Želmíra Pribilová, Ľudmila Revúcka, Peter Stašík, Jozef Šinka, Bronislava Štekláčová, Michal Tvrdoň, Anna Ursínyová, Viera Vazanová, Ján Vaškor, Ľudmila Vlchová, Jana Vyrobíková, Gejza Wimmer, Libor Žilinek, Milan Žirko

3.B</br>
Triedny profesor: František Mikulka</br>
Filip Alexy, Marcela Balalová, Vladimír Bella, Eva Benčíková, Anton Bobák, František Csiky, Jana Derková, Pavol Gutik, Viola Horňáková, Ján Hruška, Anton Chrenko, Karol Janík, Milan Jusko, Katarína Kadlečíková, Dušan Kostovský, Peter Kotleba, Jozef Kováč, Jozef Malík, Táňa Muríňová, Zdenka Pavlíková, Bohuslav Piatko, Magdaléna Piscová, Dagmar Profantová, Marián Štofko, Rastislav Uhlár, Kamila Urlandová, Roman Vrba, Peter Zálupský, Jana Zubajová

3.C</br>
Triedna profesorka: Alica Beňová</br>
Pavol Bernáth, Jana Grácová, Dana Hrobská, Marta Jaššová, Štefan Karpiš, Stanislav Konrád, Eugen Kotzig, Dušan Kubovčák, Júlia Matínková, Milada Mihálová, Jaroslav Novák, Ľudovít Orlík, Zdena Pavlíková, Emília Podhradská, Štefan Pohorelec, Jozef Polák, Jozef Rezník, Peter Rosa, Jozef Sarka, Štefan Sówka, Dušan Špitalský, Ľubica Šustrová, Milan Švec, Ladislav Tajcnár, Konrád Vrba

3.D</br>
Triedna profesorka: Viera Vavrová</br>
Zuzana Bianchi, Zlatica Bínovská, Olívia Braunová, Mária Bystraninová, Alžbeta Cverglová, Jaroslava Ededyová, Marta Grohmanová, Mária Hanicová, Mária Hoffmanová, Yveta Hudáková, Peter Hudek, Štefan Hupka, Branko Illek, Zlatica Knollmayerová, Ľudovít Kollár, Tamara Korinová, Kristína Koupanová, Ján Krutý, Mária Májovská, Anna Miklovičová, Alica Ondriášová, Júlia Platthyová, Elena Práčková, Zora Smetanová, Magda Storková, Milan Sulimanec, Jarmila Števonková, Mária Tarabová, Jarmila Vajcíková, Lýdia Vojčeková, Jana Žakovicová, Nadežda Žužičová

3.E</br>
Triedna profesorka: Tatjana Lörincová</br>
Jana Barančíková, Silvia Dvorská, Eva Fandáková, Boris Filan, Pavol Hammel, Dušan Hlavenka, Hana Chrenková, Peter Jankovič, Alena Jariabková, Edita Jaslovská, Karol Juhári, Peter Kahay, Martin Katriak, Veronika Klepáčová, Andrej Kolár, Blažena Kováčiková, Jana Kraslová, Katarína Langerová, Irena Majbová, Miroslav Malik, Valéria Marcinová, Marián Mastihuba, Zuzana Michaličková, Michal Mládek, Milan Moško, Daniela Pazderová, Mária Podberská, Mária Pohánková, Olívia Pokorná, Magda Polóniová, Elena Siebenstichová, Eva Sopušková, Ľudmila Sýkorová, Mária Šípošová, Valéria Šnajderová, Daniela Tomašovičová, Margita Véghová, Jozef Veselý, Alica Wursterová

3.F</br>
Triedna profesorka: Edita Bořutová</br>
Viera Balážová, Daniela Bednáriková, Eva Benovičová, Peter Bianchi, Angelika Biesková, Mária Boďová, Danica Cibuľková, Taťána Cignová, Katarína Čulenová, Peter Dobrovodský, Eva Frýdecká, Katarína Gavorová, Alžbeta Hillerová, Ladislav Hohoš, Oldřich Holba, Mária Horečná, Jozef Chovanec, Eva Irová, Marta Joríková, Daniela Kellová, Jaroslav Klimeš, Daniela Kočvarová, Marta Krämerová, Božena Kromerová, Daniela Kubániová, Peter Lečko, Marian Malach, Ján Minárik, Dagmar Múdra, Stanislav Novák, Ján Nvota, Vladimír Plášek, Helena Psotová, Viliam Světlovský, Jana Svobodová, Karol Škoda, Viera Tisoňová, Darina Tóthová, Ivanka Tursunová, Magdaléna Vajdová, Zuzana Vážna, Štefan Zora

3.Š</br>
Triedny profesor: Július Šoltés</br>
Pavol Babiak, Igor Gubala, Mária Hasprunová, Peter Holič, Stanislav Jakúbek, Ján Jesenský, Zdena Joríková, Elena Karáčová, Libor Koronthály, Ľubomír Kucek, Alexandra Lokvencová, Martin Macháček, Ivan Mikulecký, Gregor Ostrožlík, Ladislava Polačeková, Boris Sládeček, Marián Slovák, Dušan Svrček, Marianna Šefčíková, Attila Tóth, Peter Záturecký

AK - Nadstavbové štúdium pre absolventov SVŠ so zameraním na programovanie a obsluhu počítacích strojov</br> 
Triedny profeor: Zdeno Vlachynský</br>
Anna Danovičová, Dušan Dubecký, Mária Gallová, Jaroslav Gazda, Henrich Gregor, Magdaléna Guregová, Eduard Gurka, Terézia Györiová, Mária Halajová, Zita Havrillová, Katarína Hollósyová, Ján Husárik, Oľga Janáčková, Anna Janušková, Anna Kaňová, Magdaléna Kebísková, Ondrej Kecer, Jarmila Krajčírová, Ivan Lobík, Peter Mach, Jozef Minárik, Eva Pagáčová, Helena Pajonková, Jozef Pastucha, Róbert Petróci, Július Pipíška, Peter Poláček, Mária Repová, Miroslav Seko, Eva Sýkorová, Kornélia Vachanová, Alžbeta Vargová, Margita Vargová, Vlasta Vicianová, Renáta Záhumenská