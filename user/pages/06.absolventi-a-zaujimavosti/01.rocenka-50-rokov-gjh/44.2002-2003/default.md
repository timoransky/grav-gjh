---
title: 2002/2003
---

Školský rok 2002 - 2003

4. A</br>
Triedni profesori: Radoslav Popelka, Alena Bučková</br>
Denisa Altdorfferová, Jana Andelová, Roman Antal, Barbora Boboková, Alexandra
Bundalová, Matúš Danko, Linda Dobrovičová, Kristian Dúbravský, Juraj Fašanga,
Katarína Fischerová, Plamen Furnadţiev, Lucia Gregorová, Pavel Habáň, Michal
Horniš, Michala Chvostíková, Ján Jamriška, Ivana Jarošová, Roman Kilár, Vladimír
Kiss, Renáta Konrádová, Zuzana Kubíková, Ján Kvapil, Monika Madarászová, Zuzana
Martinovičová, Jozef Mikuš, Zuzana Neupauerová, Miroslav Rybárik, Milan Sanitra,
Tomáš Selnekovič, Peter Skovajsa, Michal Sulík

4. B</br>
Triedny profesor: Marián Slušný</br>
Peter Baran, Andrea Berková, Martin Frlička, Ondrej Gábriš, Simona Glozneková,
Dalibor Haleš, Adam Hanták, Martin Jankaj, Barbora Józsová, Peter Jurácsik, Diana
Kayyaliová, Jana Kiselová, Dalibor Kminiak, Michal Kotrbčík, Evelína Kováčiková,
Peter Krištofič, Marián Kurek, Milan Mandák, Juraj Mišúr, Jaroslav Miťko, Adam
Pustay, Samuel Semjan, Rudolf Šeliga, Veronika Šišková, Erika Škorvagová, Dušan
Tomiš, Natália Zavarská, Matej Zorkóci

Oktáva A</br>
Triedne profesorky: Eva Kratochvílová, Elena Zelenayová</br>
Veronika Boďová, Tatiana Cárová, Marek Dvorský, Ivana Fabianová, Magdaléna
Fejérová, Andrej Gömöry, Soňa Haluzová, Jana Hasenöhrlová, Peter Hrabě, Roman
Jančiga, Halina Kalašová, Jana Krčmáriková, Milan Kriţan, Juraj Kuzma, Juraj
Marikovič, Peter Matoušek, Martin Minárik, Filip Minich, Martin Molnár, Lukáš
Novotný, Katarína Paľová, Martin Polakovič, Peter Pukančík, Ján Reguli, Jaroslav
Šumný, Zuzana Vallová, Martin Zíka

Oktáva B</br>
Triedna profesorka: Mária Križanová</br>
Dávid Běhal, Monika Bočanová, Martin Dávid, Ján Dinga, Michal Drobný, Ivan Grund,
Andrea Chyliková, Peter Janiga, Ivor Kollár, Štefan Konečný, Jozef Košík, Juliana
Lipková, Ján Machajdík, Nina Marinová, Michal Mäsiar, Miroslav Merschitz, Tomáš
Mikuš, Viera Mocková, Zuzana Nehajová, Magdaléna Oravcová, Michal Patočka, Marcel
Potočný, Emanuel Procházka, Andrea Rábeková, Katarína Rošková, Lenka
Strmeňová, Michal Širaň, Vladimír Tomeček, Michal Války, Martin Vaško, Lenka
Želinská

4. IB</br>
Triedna profesorka: Daniela Kusá</br>
Michal Adamec, Zuzana Blažejová, Pavol Cvik, Kristián Danev, Daniela Ďurajková,
Katarína Chebeňová, Lucia Kayserová, Jaroslav Klíma, Edita Koščová, Pavol Krajči,
Katarína Kretová, Ľuba Krivá, Matej Kušnír, Lucia Laurincová, Ján Markoš, Anna
Marzecová, Michaela Mravcová, Ela Nahálková, Barbora Ondrušková, Miroslav
Pastorek, Peter Rajnoha, Tomáš Rybár, Michal Sabadoš, Jana Straková, Lucia
Šikulíncová, Branislav Štepita, Mária Tarasová, Jozef Veselý, Branislav Zagrapan