---
title: 1972/1973
---

Školský rok 1972 - 1973

4.A</br>
Triedna profesorka: Zora Pašková</br>
Mária Balážová, Jozef Barta, Peter Čechovič, Gabriela Černá, Peter Dobrota, Gabriela Dudová, Magdaléna Dudová, Ivan Dinga, Andrej Goč, Alena Hašanová, Michal Chudík, Daniela Juhásová, Alexander Jakubec, Ľubomír Klaučo, Ľubica Klčová, Miloš Kmety, Ivan Koza, Miroslav Krčma, Martin Križan, Maroš Lazár, Ján Májek, Peter Málik, Peter Marčák, Peter Marendiak, Miroslav Mosný, Štefan Meliš, Juraj Námer, Juraj Nvota, Ján Potúček, Andrej Rády, Vladimír Rajčák, Viktor Roman, Miroslav Rusko, Peter Slavkovský, Zuzana Šmatláková, Ivan Šmogrovič, Igor Špoták, Miloš Švantner, Marián Uherko, Edgar Zachar

4.B</br>
Triedna profesorka: Zuzana Orovanová</br>
Eva Augustová, Ľubica Bargeľová, Tibor Blažík, Zuzana Čelková, Pavel Frieder, Zdenka Gaislerová, Milon Gajdoš, Pavol Gavaľa, Peter Gemeran, Anna Gerthoferová, Danica Greisingerová, Darina Halašová, Milan Hán, Otília Havlíková, Viera Hôrčiková, Anna Chmeliarová, Ľubomír Jankovič, Marián Kasanický, Daniela Kolářová, Milan Kolibiar, Liana Kovácsová, Jana Kurucová, Katarína Kutáková, Gabriela Lišáková, Miloslav Moravec, Tatiana Mrocková, Silvia Paulíková, Marián Poláček, Ivana Sabová, Renáta Šebíková, Ľubomír Šedivý, Róbert Šuffák, Zuzana Tichá, Eva Vágnerová, Viera Valentová, Norbert Vrbjar, Viera Vršková

4.C</br>
Triedna profesorka: Elena Weberová</br>
Dagmar Ambrusová, Zora Bellová, Dušan Cintula, Eva Čunderlíková, Jozef Danek, Beata Dohnányová, Katarína Duhajová, Zdena Ededyová, Katarína Ferenceiová , Mária Gajdová, Tibor Gajdošík, Eva Hanzelová, Nadežda Hradská, Tatiana Hrnčiríková, Zuzana Holíková, Viera Hudecová, Darina Husíková, Zoja Ivaničková, Jaroslava Kopčová, Helena Kovačovičová, Marta Kováčiková, Peter Kresák, Barbara Kozmová, Rudolf Mešťan, Karol Mrva, Gabriela Nagyová, Jozef Pukalovič, Eva Pišútová, Eleonóra Profousová, Roman Rutkovský, Viera Repášová, Daniela Schaleková, Katarína Šišková, Zuzana Šebestová, Dana Školníková, Alena Škodová, Jana Ťašká, Danica Tenerová, Jozef Valkár, Ján Vandlík, Mária Virághová, Martin Znášik

3.F</br>
Triedna profesorka: Helena Kotzigová</br>
Igor André, Pavol Babal, Rastislav Bednarič, Ivan Belák, Ján Beláň, Milan Beláň, Peter Bézay, Pavol Bílik, Branislav Bundala, František Cako, Peter Čepčiansky, Peter Dunaj, Štefan Filipko, Emil Francisci, Marta Gálová, Štefan Geleta, Branislav Hodál, Marián Holečka, Martin Hrivnák, Eva Jungová, Igor Janetka, Jozef Kajan, Marián Kečkéš, Mária Klajbanová, Jozef Klement, Peter Kostelničák, Dagmar Krížová, Dušan Mak, Dagmar Masná, Emília Matúšková, Milan Michalík, Tomáš Nehera, Juraj Orel, Peter Ostrožlík, Juraj Renčko, Pavol Ret, Vladimír Sádovský, Blažena Stračárová, Eva Školíková, Scarlett Thurzová, Ivan Topoľský, Helena Valková, Ján Zelinka

3.G</br>
Triedna profesorka: Mária Stracová</br>
Ľubor Bajzík, Soňa Barbuščáková, Mária Bernadičová, Gabriel Bianchi, Marián Borovský, Silvia Čillíková, Juraj Danko, Ľuboš Danko, Ivan Demmer, Eva Fabiánová, Dagmar Chlapíková, Judita Chovanová, Tatiana Jakešová, Mária Jirásková, Ingrid Joklová, Alfonz Kilian, Vlastimil Kratochvíl, Kornélia Krébesová, Mária Krištofičová, Katarína Kuffová, Jana Kúkeľová, Erich Kužma, Stanko Máchata, Hana Mihalovičová, Jarmila Minaříková, Viera Murínová, Jozef Opat, Anna Miženková,Taja Ošmerová, Stanislav Petrášek, Miroslava Petrášová, Milan Pikna, Anna Purkertová, Igor Rzavský, Alexander Soldán, Klára Šimončičová, Ružena Šimová, Katarína Turecká, Daniela Vŕbová

AK - nadstavbové štúdium so zameraním na programovanie a obsluhu počítacích strojov</br>
Triedny profesor: Ján Koval</br>
Eva Ábelova, Katarína Ábelovská, Eva Adamkovičová, Miroslav Almer, Mária Bagitová, Ľubomír Barčák, Judita Béresová, Anna Bileková, Marta Budiašová, Tibor Csadi, Zoltán Doboš, František Egry, Ondrej Hegyi, Mária Golovová, Magda Hostinská, Terézia Hrušková, Marián Hudec, Magdaléna Kompánová, Ľudmila Kubaščáková, František Kubovič, Nedežda Lančiová, Gabriel Melega, František Mesároš, Veronika Mušáková, Milan Paliatka, František Pavlačka, Viera Račková, Elena Salnická, Magdaléna Soosova, Jozef Szoocs, Anna Škrovinová, Vlastimil Šubr, Gabriel Teplický, Mária Tomoriová, Ivan Turský, Bohuslav Zajíček