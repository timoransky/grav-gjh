---
title: 2008/2009
---

Školský rok 2008 – 2009

5. A</br>
Triedna profesorka: Zuzana Mináriková</br>
Mária Bánhegyiová, Martin Barát, Lenka Bosá, Marcela Bučeková, Juraj Čík, Tomáš
Dado, Svetlana Durmeková, Zdena Faragulová, Nina Feriancová, Monika Gurská,
Martina Holá, Nina Holešová, Miroslava Kubincová, Katarína Kubovicová, Adam
Nekola, Petronela Pivarčiová, Michala Prokešová, Andrea Rakovská, Mária Rumanová,
Kristína Svarinská, Martina Svobodová, Alena Šarmírová, Júlia Šimečková, Lucia
Tomovičová, Zuzana Ürgeová, Lucia Vaculová, Jozef Vlčko

4. B</br>
Triedny profesor: František Kosper</br>
Katarína Baláţová, Jozef Belko, Ondrej Benc, Andrea Čiháková, Kristína Danková,
Metod Eliáš, Michal Grňo, Michal Hajdin, Michal Hozza, Milan Husár, Lukáš Igaz,
Zuzana Jančovičová, Katarína Jasencová, Andrej Komada, Michal Kopp, Monika
Krafčíková, Kristína Krištúfková, Miroslav Kríţ, Igor Liška, Peter Macko, Pavol Majer,
Michal Markusek, Mário Mertin, Barhum Nakhlé, Jozef Négli, Michal Novoveský, Filip
Pakan, Ondrej Poláček, Jakub Sigmund, Barbora Šimonová, Zuzana Šišmišová, Ľubomír
Šteffek, Juraj Talčík, Matej Truben, Milan Vraník, Ľubica Zemanová

4. C</br>
Triedna profesorka: Renáta Horváthová</br>
Michal Amon, Katarína Babincová, Tomáš Bohumel, Lucia Brezová, Michal Dorner,
Samuel Grečner, Ivan Grman, Barbara Herucová, Pavol Hrabovský, Matej Hudec,
Dušan Hupka, Ján Janočko, Nikola Kňazovická, Tomáš Kohút, Roman Korenek, Ivan
Košdy, Peter Kottáš, Ivan Kozmon, Adam Letenay, Monika Miklášová, Júlia
Murányiová, Martina Obţerová, Jakub Ondruš, Ján Pavlásek, Martin Paţický, Martin
Rusňák, Tomáš Sabo, Juraj Senecký, Martin Schön, Simona Socratousová, Róbert
Stríbrnský, Tomáš Sucha, Adam Šabla, Andrea Šajbidorová, Viktor Tomkovič, Tomáš
Varga

Oktáva A</br>
Triedna profesorka: Miriam Čuntalová</br>
Jaroslav Bielik, Michal Bočan, Maroš Bratko, Martin Čechvala, Dann Daood, Diana
Demkaninová, Dávid Faktor, Matej Karkalík, Richard Kittler, Lea Kostolná, Jakub
Košnár, Jozef Kováč, Marek Láni, Lenka Madudová, Dorota Martinková, Michaela
Mlynarčíková, Adam Močkoř, Matúš Muroň, Roman Noge, Mojmír Paulička, Róbert
Peschl, Helena Proksová, Jakub Rehák, Matej Rehák, Miroslava Rusová, Adriana
Ruţičková, Judita Sehnalová, Daniel Slezárik, Ivan Šiška, Štefan Štefanov, Andrej
Thurzo, Jerguš Trlica, Roman Váhovský, Ţaneta Vargová, Andrej Ţiak

Oktáva B</br>
Triedny profesor: Ján Mayer</br>
Jana Bizoňová, Radovan Brliť, Ján Dubovský, Stela Fabišíková, Lucia Foltýnová,
Zuzana Fordinálová, Juraj Gallus, Adam Garin, Adam Grand, Alena Hechtová, Michal
Jurík, Adam Klanica, Adrián Kollár, Tatiana Kosztolányiová, Patrik Kováč, Andrej
Kuffa, Matúš Lačný, Mária Molnárová, Matúš Nagy, Andrej Osvald, Milan Pacher,
Patrik Patáč, Vladimír Rajecký, Dávid Rau, Michaela Rošková, Martina Stančíková,
Tomáš Timek, Július Vaňura, Michal Venglár

4. IB A</br>
Triedny profesor: Matej Gonda</br>
Adam Ambrus, Ján Bača, Martin Bačik, Mária Beňačková, Lukáš Boško, Alexej
Filippov, Maroš Forgáč, Andrej Gnip, Pavol Hronský, Tomáš Chrien, Veronika
Jankovičová, Pavol Kiraľvarga, Ján Komadel, Dominika Kramerová, Michal Kubiš,
Juraj Ľahký, Lenka Matejovičová, Nikoleta Moravcová, Hana Sipková, Tomáš Srnka,
Petra Steigaufová, Juliana Surovcová, Martin Szakál, Martin Šebesta, Natália Tóthová,
Peter Vanya, Katarína Veselá

4.IB B</br>
Triedny profesor: Alec Carter</br>
Matej Belín, Lenka Bendová, Kristína Darázs, Jana Daučíková, Matúš Dišanec, Linda
Görfölová, Ján Grnáč, Alena Halgašová, Milan Helebrandt, Lenka Chorvatovičová,
Peter Ivan, Filip Jajcaj, Lucia Kóšová, Magdaléna Kováčová, Zuzana Koţuchová, Anna
Krasztev-Kováč, Marcel Krutil, Christian Lennaerts, Rudolf Nemec, Patrícia
Nguyenová, Mária Perignáthová, Anya Safira, Lucia Schoberová, Daniel Spišiak, Trang
Vu Huyen, Filip Ujlaky, Barbora Vojtková