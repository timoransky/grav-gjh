---
title: 1999/2000
---

Školský rok 1999 – 2000

4. A</br>
Triedny profesor: Peter Demkanin</br>
Miroslav Bajtoš, Natália Bakajová, Martin Belan, Zuzana Beňová, Pavol Bujňák, Martin
Ciran, Lenka Dávidová, Peter Fargaš, Peter Farkaš, Marek Goldschmidt, Dáša
Gregorová, Peter Haňo, Michal Hapčo, Igor Havlíček, Miroslava Jančová, Marek Kinka,
Vladimír Kočí, Dušan Kraxner, Matúš Maar, Peter Májek, Magdaléna Mančušková,
Martin Martinkovič, Peter Morong, Peter Náther, Martin Nemček, Tatiana Puhová,
Matej Slezák, Daniela Svetlíková, Štefan Šimek, Juraj Švec, Pavol Tomaschek, Kristína
Tomková, Laura Vršková, Miroslav Vrškový, Zuzana Zbellová

4. B</br>
Triedna profesorka: Klára Grečnerová</br>
Tomáš Ágošton, Karol Achberger, Csaba Czuprai, Katarína Dohnányová, Martin
Fabian, Barbora Gavenčiaková, Štefan Horvát, Samuel Imriška, Peter Ješko, Viliam Koiš,
Peter Košinár, Martin Koštrna, Eva Kuchyňárová, Ľudovít Lučenič, Peter Mach, Daniel
Mentel, Juraj Monošík, Zuzana Netiková, Juraj Onderík, Veronika Pavlíková, Marek
Petrík, Barbora Pitrunová, Andrea Plávková, Marián Potočný, Soňa Princová, Veronika
Pukančíková, Zoltán Radnóti, Andrej Řikovský, Monika Satúryová, Marek Strmeň, Daniel
Tóth, Marián Uherčík, Martin Valentovič, Štefan Varga, Peter Vavrák, Zuzana Vrťová

4. C</br>
Triedna profesorka: Anna Jankovičová</br>
Jaroslav Bálik, Zuzana Baloghová, Monika Bančanská, Bela Belohorská, Peter Bock,
Kristína Boďová, Matej Breja, Ján Breza, Ján Broniš, Jaroslav Bugarovič, Petra
Čárska, Marek Fučila, Marek Grác, Tomáš Gurský, Desana Hlavčáková, Martin Jurček,
Jakub Kedrovič, Katarína Kešjarová, Martin Klubal, Ondrej Krško, Albert Marenčin,
Lucia Miklíková, Zuzana Miklóšová, Matej Minárik, Veronika Molnárová, Petra
Nováková, Tomáš Pleceník, Marek Podmaka, Dušan Poruban, Peter Romančík, Boris
Slávik, Michal Šuster, Peter Ţalman

4. D</br>
Triedny profesor: Ján Mayer</br>
Milan Báthory, Jerguš Benkovič, Martin Bujňák, Marek Fabian, Marek Horváth, Jozef
Chajdiak, Dušan Juřena, Martin Kadlec, Katarína Kincelová, Juraj Kocka, Tomáš
Kratochvíl, Marek Kusenda, Juraj Lalík, Samuel Lamačka, Svetlana Mečiarová, Lenka
Mikulová, Andrea Mišianiková, Andrej Osif, Peter Pinter, Július Pomšár, Martin
Praško, Martin Rymarenko, Matej Štalmach, Tatiana Tekeľová, Barbora Thomasová,
Juraj Tomík, Tomáš Trella, Zuzana Vajdová, Ľubomíra Vojteková, Igor Ţáček

4. IB</br>
Triedny profesor: Alena Polakovičová</br>
Jana Bilíková, Jana Čákanyová, Vladimír Černý, Marián Daubner, Jakub Demáček,
Elena Dikáczová, Rastislav Duriš, Juraj Dzifčák, Zoja Hollá, Ondrej Hrdlička, Katarína
Husárová, Alexandra Katreničová, Marek Kováč, Juraj Kramara, Ivana Lehocká, Peter
Luciak, Miriam Marušiaková, Juraj Mesároš, Hana Michálková, Ľubica Mošková, Matej
Sapák, Miroslav Skovajsa, Marek Soska, Zuzana Ščurková, Jana Teremová, Marta
Tomišová, Filip Vítek, Veronika Vlčková, Michal Zemko