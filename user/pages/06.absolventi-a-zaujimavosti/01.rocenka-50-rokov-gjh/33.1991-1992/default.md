---
title: 1991/1992
---

Školský rok 1991 - 1992

4.A</br>
Triedna profesorka: Alena Polakovičová</br>
Karol Blizňák, Martin Busnyák, Katarína Dlugolinská, Andrej Dziak, Elena Gašparovičová, Peter Gurnik, Marek Határ, Jozef Horváth, Monika Hybenová, Daniel Chovanec, Danica Jankovičová, Ivona Jankovská, Vladimír Jaraba, Martin Koník, Stanislava Kollárová, Zuzana Krutková, Jana Lexmannová, Roman Matulík, Angelika Melichová, Juraj Méry, Matúš Minárik, Roman Noga, Richard Ostertág, Martin Rudy, Peter Rybár, Danica Sedláková, Vladimír Sekerka, Andrea Svitoková, Ľubica Šimová, Pavol Tarábek, Jana Veselovská, Jana Vítková

4.B</br>
Triedna profesorka: Ľubica Bednáriková, Mária Križanová</br>
Marián Bartkovjak, Miriam Belianska, Martin Benča, Jana Boboková, Martina Dobrovodská, Marek Farkaš, Tibor Hlavatý, Zoltán Horváth, Katarína Horváthova, Eva Chudá, Alena Javorníková, Renáta Kiselicová, Matej Kordoš, Viktor Kouřil, Zuzana Kováčová, Uršula Kráľová, Táňa Kubičárová, Jana Lacková, Andrej Lupták, Zuzana Luptáková, Karol Macák, Hana Machová, Marianna Makarová, Zuzana Mathesová, Andrea Mikušová, Tibor Németh, Ľubomír Požgay, Ľuboš Suchjak, Michal Suchoba, Marek Šalkovič, Dušan Vallo, Martin Vršanský, Zuzana Záhradná

4.C</br>
Triedna profesorka: Alena Dvorská</br>
Alexandra Adamcová, Ján Barnáš, Martin Bečka, Michal Božík, Veronika Brimichová, Martin Bučko, Katarína Cecková, Martin Černý, Martina Derková, Martina Esterleová, Lenka Fibíková, Tomáš Flassik, Zuzana Gallová, Simona Hodermarská, Oľga Illášová, Kamila Koronthályová, Lucia Kršková, Robert Kuchárik, Juraj Laifr, Peter Lauko, Ján Lindtner, Silvia Matlovičová, Henrieta Meszárošová, Radovan Mráz, Pavol Németh, Jarmila Paligová, Nikoleta Polačeková, Peter Polák, Ján Präsens, Július Sloboda, Zuzana Sochorová, Monika Šimončíková, Miroslava Tomagová, Richard Vašečka

4.D</br>
Triedna profesorka: Darina Vrbová</br>
Beáta Angyalová, Jana Barboríková, Peter Bartal, Peter Bernát, Kvetoslava Blažeková, Martin Boďa, Monika Brezováková, Marcela Burdová, Zuzana Dorotková, Soňa Feňvešiová, Andrea Hincová, Martin Hlavačka, Pavol Hraško, Gabriel Király, Barbora Koubeková, Denisa Krížová, Andrea Lapuníková, Michal Marek, Ladislav Medveczky, Michal Minárik, Andrej Olejník, Norbert Paksi, Radoslav Pohlod, Branislav Selčan, Renáta Štefunková, Martin Valentovič, Daniela Vodičková, Veronika Zábrodská, Silvia Žišková

4.E</br>
Triedna profesorka: Viera Eližerová, Jana Huttová</br>
Gabriela Andrašiková, Barbora Bartošová, Andrea Baňacká, Miroslav Blanárik, Alica Budveselová, Jana Cidoríková, Viera Dorníková, Martina Ferancová, Barbora Gabániová, Ľudovít Hajnovič, Marián Henc, Oto Hlinčík, Ivan Horvát, Juraj Hurný, Hugo Hýbal, Ján Janšta, Robert Jung, Andrej Jurík, Marek Kalavský, Luboslava Kamenická, Zuzana Karvašová, Ľubomíra Kolenčíková, Eva Kováčova, Andrej Michalík, Peter Nemček, Pamela Pálová, Denisa Párovská, Roman Pivka, Narcisa Plačková, Jana Potecká, Vladimír Prutkay, Laura Siváková, Marián Tokár, Dana Tomanová