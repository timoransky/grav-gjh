---
title: 1983/1984
---

Školský rok 1983 - 1984

4.A</br>
Triedny profesor: Ľubomír Krištof</br>
Ivan Bečka, Štefan Beke, Igor Bořuta, Martin Cádra, Martin Číčel, Miloš Demovič, Jana Dobišová, Zuzana Dunajčíková, Lenka Györiová, Martin Floch, Vanda Hambálková, Branko Hančok, Jana Hennelová, Libor Chrást, Elena Janegová, Vladimír Januschke, Roman Ježík, Vladimír Jurášek, Ladislav Kamenický, Dana Kostolanská, Zdenka Kováčová, Ľubomír Menczer, Martin Mišík, Jana Olléová, Zuzana Paulíková, Zuzana Ploczeková, Mariana Soukupová, Dana Štofková, Nataša Tomková, Monika Zeleňáková

4.B</br>
Triedny profesor: Vladimír Jodas</br>
Ľuboš Bajzík, Martina Barnášová, Peter Belohorec, Martin Bukera, Miloslav Bystrický, Tomáš Dérer, Ján Draškovič, Peter Fellegi, Tomáš Gedeon, Martin Hanula, Michal Hejný, Tomáš Horváth, Ivan Illich, Martin Jurkovič, Jana Kátlovská, Monika Khandlová, Pavel Kneppo, Roman Koller, Dagmar Komorová, Eva Koncová, Anna Lövová, Ladislav Lukáč, Peter Macháč, Martin Môťovský, Emil Páleš, Tomáš Svoboda, Katarína Szivósová, Eva Šándriková, Daniel Škoda, Ján Škvarka, Ivo Švec, Juraj Švitel, Marián Vittek

4.C</br>
Triedna profesorka: Tatiana Lörincová</br>
Boris Ballo, Peter Bielik, Miroslav Bukvai, Monika Buranová, Viera Cocherová, Iveta Dingová, Martin Drobný, Alexej Fulmek, Viera Hájková, Zuzana Hašková, Lucia Holzerová, Igor Hrinko, Štefan Hudec, Dana Husáková, Helena Írová, Dáša Kokuľová, Ingríd Krejnusová, Igor Kucej, Eva Kurtiová, Radim Lýsek, Igor Malinovský, Andrea Molnárová, Michal Noge, Jana Pelikánová, Eva Raučinová, Katarína Rothová, František Stano, Rastislav Staroň, Miroslav Švehla, Pavol Viskupič, Jana Vojtková, Marta Vršková

4.D</br>
Triedny profesor: Pavol Hronček</br>
Jozef Bachratý, Peter Brádler, Renáta Butková, Miroslav Červený, Oľga Drgoňová, Eva Dubovská, Mariana Ďurinová, Miroslav Fikar, Dušan Franců, Juraj Holka, Ivona Chudá, Miroslav Kolečáni, Dagmar Konečná, Juraj Lichvár, Roman Martinec, Bibiána Melišková, Kristian Nandraský, Dalibor Pajchl, Andrea Peterichová, Peter Podhorný, Peter Polakovič, Katarína Sádecká, Fedor Šrobár, Renáta Šterbová, Adriana Zacharová

4.E</br>
Triedny profesor: Marián Slušný</br>
Adriana Bajáková, Boris Bilčík, Adrián Bobula, Mária Böhmová, Eva Debnárová, Jana Ferková, Vladimír Gánoczy, Brigita Géczová, Miroslav Grajcar, Juraj Granec, Adriana Grosschmidtová, Ingrid Grúberová, Gejza Holzer, Rudolf Holzer, Róbert Jacko, Alžbeta Klemanová, Alena Knapíková, Mária Kollerová, Miloš Koprla, Zuzana Kotlebová, Kerstin Kováčova, Alexander Kozár, Juraj Machač, Róbert Martiško, Miroslav Měkota, Peter Nagy, Drahuša Oktávcová, Táňa Pastorková, Ivana Salajová, Ivan Sečanský, Pavel Struhárik, Pavol Strunga, Vladimír Šešera, Juraj Škvarka, Pavol Tamáši, Tibor Trnovský, Viera Vdovičenková