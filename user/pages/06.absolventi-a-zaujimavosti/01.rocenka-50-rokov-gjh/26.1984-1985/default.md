---
title: 1984/1985
---

Školský rok 1984 - 1985

4.A</br>
Triedny profesor: Ondrej Demáček</br>
Roman Bartoschek, Ivan Benda, Pavol Benkovič, Martin Brunovský, Jana Čapková, Peter Draškovič, Eva Ďurošiová, Marcela Glasová, Peter Grajcar, Juraj Greš, Jozef Hanus, Viktor Hidvéghy, Peter Hromec, Ivan Huljak, Martin Husár, Daniel Jóna, Juraj Krištúfek, Ján Kudláč, Peter Lászlo, Monika Miklošková, Valér Ostrovský, Tomáš Pavelka, Martina Pavelková, Juraj Pavle, Mira Reháková, Jozef Rieger, Peter Smižanský, Martin Šebesta, Peter Šimko, Matej Vagač, Zuzana Vajcziková, Alexander Vencel, Martin Vician, Eva Vlasáková, Dagmar Zacharová

4.B</br>
Triedna profesorka: Viera Vavrová</br>
Igor Barnovský, Beáta Bednárová, Ivan Čársky, Peter Častulík, Marián Drozd, Róbert Dyda, Andrej Džadoň, Peter Chochula, Silvia Ivanová, Rastislav Kanovský, Juraj Klein, Zoltán Kocsis, Karin Komorová, Eva Kopecká, Peter Kossey, Oľga Kosseyová, Miroslav Líštiak, Ján Löv, Roman Marko, Václav Maťoška, Ivan Móro, Andrea Móžiová, Michaela Murínová, Róbert Nürnberger, Michal Pala, Ivan Pecho, Ján Petrovič, Imrich Polák, Zuzana Potroková, Viera Richtáriková, Erich Siebenstich, Anton Slatinský, Peter Smorádek, Pavol Šimkovic, Tibor Vavro

4.C</br>
Triedny profesor: Ján Kysel</br>
Katarína Blahová, Dana Brysová, Michal Dobre, Ľubica Dubíková, Katarína Fabriciová, Katarína Harnošová, Miroslav Havelka, Oľga Hegerová, Stanislav Hrušovský, Peter Janega, Andrea Jozsová, Gabriela Jursová, Ernest Kabát, Ivana Kollárová, Andrea Krakovská, Dagmar Kubíková, Štefan Lahita, Beata Melicharová, Dana Melníková, Ján Molnár, Zdena Morávková, Juraj Nemček, Richard Pintér, Mario Porubec, Andrea Ptáčková, Ľubica Purdová, Alexander Sobek, Branislav Ševčík, Viera Tesařová

4.D</br>
Triedna profesorka: Mária Ondriašová</br>
Erika Árendášová, Olymp Borovský, Rastislav Broska, Silvia Ceizelová, Beatrix Debnárová, Marián Dočkal, Viktor Drahoš, Jozef Farkas, Ingrid Horváthová, Jozef Iványi, Veronika Jánošíková, Mário Jurča, Marián Kadlíček, Branislav Klikáč, Jacqueline Kukučková, Martin Langfelder, Martina Majerčíková, Katarína Marková, Andrea Markovičová, Zuzana Matulová, Peter Mitka, Vladimír Németh, Andrej Obuch, Zuzana Paulíková, Miroslav Sedlár, Hermína Vallová, Helga Vargová, Viera Vdovičenková, Ivan Zakov, Matej Zaťko