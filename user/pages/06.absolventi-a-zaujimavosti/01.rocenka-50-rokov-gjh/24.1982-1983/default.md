---
title: 1982/1983
---

Školský rok 1982 - 1983

4.A</br>
Triedna profesorka: Zlatica Reháčková</br>
Mária Augustínova, Anna Blahútová, Zuzana Cinová, Lenka Čipkárová, Ján Dižo, Dušan Faktor, Blanka Gašparíková, Iveta Györiová, Zuzana Hurdálková, Zuzana Chodúrová, Katarína Jakubcová, Ľubica Jodasová, Dagmar Kobetičová, Alena Kodajová, Renáta Kováčiková, Pavel Křižka, Tibor Kyseľ, Ľubomír Michálik, Jarmila Michálková, Beata Mráčková, Imrich Nögell, Ida Ondráčková, Erika Pečenadská, Ingrid Poláková, Anna Pospíšilová, Juraj Richtárik, Igor Ripka, Miroslava Řeháková, Jana Smejkalová, Andrej Stančík, Eva Stejskalová, Anna Šrámková, Lýdia Štefánková, Tamara Ursínyová, Hana Vaníková, Andrea Vodrážková, Zuzana Zigová

4.B</br>
Triedna profesorka: Anna Akácsová</br>
Mária Antalicová, Anton Bendis, František Blažíček, Peter Borovanský, Pavol Bôrik, Zora Brežná, Mariana Červeňová, Zoltán Domonkos, Miroslav Florek, Karol Guniš, Tomáš Havetta, Tomáš Hulka, Dušan Chudý, Helga Jakešová, Iľja Jurkovič, Peter Kordoš, Anna Kosseyová, Jana Košecká, Libor Lányi, Hana Lichardová, Roman Lichner, Viktor Martišovitš, Bronislava Maťošková, Adriana Minarovičová, Róbert Osvald, Stanislav Panák, Richard Pulmann, Ondrej Repka, Hana Riečanová, Miloš Sís, Milan Smolík, Peter Steltepohl, Martin Syč-Milý, Ladislav Szántó, Miloš Tichý, Jana Vietorisová, Michal Winczer

4.C</br>
Triedna profesorka: Blažena Stračárová</br>
Daniela Bagoňová, Andrej Bederka, Michal Benák, Peter Bilik, Ronald Bitterer, Martina Búciová, Ľuboš Dobšovič, Martin Florián, Ján Harman, Milan Hnáta, Juraj Holec, Sylvia Hroudná, Tatiana Jedináková, Marcel Juck, Miroslav Kachlík, Igor Kintly, Tomáš Kollár, Jaroslav Kozák, Mário Lelovský, Peter Makýš, Marián Marek, Jana Maxiánová, Teodor Palenčár, Milan Prochác, Ľubica Ružeková, Dušan Slezák, Igor Strmiska, Jana Štubňová, Anton Takáč, Ľuboš Tibenský, Martin Zverka

4.D</br>
Triedny profesor: Edita Bořutová, Jaroslav Švach</br>
Renáta Bajusová, Peter Barok, Viliam Bendel, Milan Boháč, Jana Brichtová, Kornélia Čádyová-Bellová, Aurel Ducko, Ján Filip, Dagmar Gápelová, Erna Gréserová, Jaroslav Halvoník, Dagmar Haraštová, Rastislav Horváth, Nataša Hricíková, Pavol Ištok, Soňa Kátlovská, Kristína Kocúrová, Iveta Končeková, Stanislav Krátky, Vladimír Linek, Anna Liptáková, Igor Maťko, Andrea Mičudová, Tomáš Minárik, Jana Omastová, Rudolf Púchy, Marcel Schwarz, Rastislav Sláma, Ivo Spiller, Marta Stančíková, Pavol Stríženec, Viliam Sukuba, Dagmar Šiklová, Peter Tiňo, Boris Vavrík, Mária Vlasáková, Vladimír Volentier, Pavol Zálešák

4.E</br>
Triedna profesorka: Hana Komorová</br>
Alžbeta Altrichterová, Magdaléna Boboková, Ján Bulla, Soňa Čechovičová, Ingrid Drgoňová, Eleonóra Foltínová, Andrea Ginterová, Zita Gömöryová, Daniel Hatiar, Tibor Heger, Katarína Hlásniková, Alena Hrabovská, Jaroslav Janda, Brian Janečka, Monika Janicsová, Daniel Kadera, Jana Karasová, Ján Klikáč, Branislav Kollárik, Katarína Korábová, Eva Kostolányiová, Marián Kováč, Pavol Kuták, Dagmar Malatincová, Kozmos Málek, Ján Malík, Jaroslav Náter, Peter Pecha, Anna Pleváková, Renáta Podmaníková, Elena Rukavicová, Igor Slezák, Ita Srnová, Zuzana Starovecká, Katarína Šándorová, Juraj Šepák, Viola Vajdová, Roman Zelenka, Dana Žilavá