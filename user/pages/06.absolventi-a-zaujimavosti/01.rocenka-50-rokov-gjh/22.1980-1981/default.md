---
title: 1980/1981
---

Školský rok 1980 - 1981

4.A</br>
Triedny profesor: Ján Kysel</br>
Ján Blatnický, Martin Bosák, Jacqueline Burianová, Milan Celler, Bohumil Čider, Gabriela Čuková, Katarína Feketeová, Gabriela Franczová, Ján Gyermek, Ivan Hanzlík, Juraj Heger, Ingríd Hodinárová, Dana Kaňková, Dana Kapusníková, Katarína Lengová, Igor Liba, Katarína Majerská, Mária Malejčíková, Róbert Matzenauer, Ivan Morávek, Ingrid Morvayová, Soňa Podhorná, Eva Poláčková, Iveta Poláková, Erika Pomichalová, Peter Popík, Ľuboslava Putišová, Renáta Sedláková, Erich Šašinka, Martin Václav, Eva Veselá, Eva Zellnerová

4.B</br>
Triedna profesorka: Elena Ivančíková</br>
Juraj Broček, Dagmar Brodňanská, Gabriel Droba, Dominik Filipp, Irina Galanová, Jozef Gécz, Jana Gonová, Mária Gúcka, Milan Hanajík, Zuzana Hanulíková, Michal Jarábek, Róbert Koráb, Roman Kováč, Milan Krempaský, Peter Lichard, Elena Neubauerová, Peter Oravec, Marian Oltman, Jana Pavelková, Peter Pecháň, Roman Rigáň, Peter Slivka, Peter Spudil, Jana Surovková, Hugo Schilder, Vladimír Sýč-Milý, Peter Šimčisko, Katarína Štiavnická, Martin Turčan, Peter, Zverka, Ivan Žajdlík

4.C</br>
Triedna profesorka: Viera Vavrová</br>
Margita Balážová, Jozef Bielik, Eva Čuková, Želmíra Dzvoníková, Tibor Ďurďovič, Tomáš Frištacký, Ján Garaj, Zuzana Halušťoková, Janette Hornická, Ján Horváth, Peter Hrušovský, Miloš Chodas, Peter Jamrich, Zdeno Jedlička, Jozef Jentner, Juraj Meszároš, Hana Němcová, Zuzana Podivinská, František Podivinský, Jozef Řezníček, Miroslav Stračár, Ondrej Sütö, Peter Šebej, Vladimír Takáč, Dušan Trochta, Andrej Václav, Dušan Volentier, Róbert Zavadil

4.D</br>
Triedny profesor: Jaroslav Švach</br>
Eva Babbová, Juraj Baček, Zuzana Balková, Martin Bartoš, Peter Bernát, Ľubica Blechová, Ivan Brychta, Miloš Daranský, Alena Erösová, Dalida Haťapková, Soňa Heribanová, Iveta Kačmáriková, Katarína Katušová, Viera Knapíková, Klaudia Kuševová, Jela Liščáková, Marko Miglierini, Zuzana Michálková, Eva Naňová, Alena Petrášová, Alena Pisárová-Levárska, Drahoslava Poliaková, Jaroslav Rukavica, Iveta Slávikova, Jana Slobodová, Iveta Sojáková, Katarína Šinková, Jana Štemberová, Viera Tuhá, Katarína Vargová, Zdena Zemanová

4.E</br>
Triedna profesorka: Viera Eližerová</br>
Katarína Bartalská, Igor Bartl, Andrej Bartók, Jana Benžová, Renáta Čapkovičová, Ivan Fraňo, Juraj Hajdúk, Andrea Hajduková, Jana Hanzlovičová, Alica Hässlerová, Dorota Hegerová, Silvia Hontiová, Katarína Hrubanová, Tomáš Konc, Ivo Kovačič, Katarína Kszelová, Soňa Kučerková, Zuzana Langfelderová, Zuzana Macejková, Michal Maheľ, Zdena Mayerová, Zdena Mitasová, Peter Ondrčka, Jana Orgonášová, Jana Prokopová, Michal Savčenko, Zlata Savčenková, Viktor Sobek, Eva Smutná, Igor Strinka, Martin Šujan, Milan Tichý, Alena Typalová, Pavel Veis

AK - nadstavbové štúdium so zameraním na programovanie a obsluhu počítacích strojov</br>
Triedna profesorka: Oľga Grmanová</br>
Eva Aschenbrennerová, Mária Balážová-Gallusová, Igor Balla, Ružena Bányaiová, Terézia Csizmadiová, Ivan Erben, Ladislav Fazekas, Ildikó Fazekasová, Katarína Györgyová, Gabriel Habán, Martin Hurtík, Marta Janečková, Eva Kömüvesová-Drugdová, Timár Lancz, Mária Lúčová, Anna Michalisková, Ivan Nagy, Anna Rajniaková-Schindlerová, Jozef Ravasz, Darina Remenárová, Ildiko Sütöová, Mária Szazová, Jozef Vachan, Richard Vanko, Gabriel Vilagi, Miroslav Vitek