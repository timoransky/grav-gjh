---
title: 1981/1982
---

Školský rok 1981 - 1982

4.A</br>
Triedna profesorka: Zuzana Fraasová</br>
Peter Babák, Július Brichta, Mária Čížová, Dušan Fašung, Zora Fikarová, Zuzana Foltýnová, Dana Fričová, Zuzana Hlavicová, Jana Horniačková, Katarína Jandová, Pavol Janovič, Marián Knap, Jaroslava Komárová, Viola Kondrótová, Jana Križankovičová, Vladimír Kudláč, Peter Laššák, Vladimír Markusek, Daniel Matej, Anna Miklášová, Adriana Mytýzková, Miroslav Pasek, Gabriel Paulíny, Ľubica Pavlincová, Alena Poláková, Alexej Prokop, Ivana Smažilová, Peter Szvitek, Alena Urbánková, Ivan Važan, Tatiana Vančová, Michaela Weissová

4.B</br>
Triedny profesor: Július Šoltés</br>
Viliam Bernát, Miriam Dvořáková, Juraj Fejda, Igor Haľama, Peter Havaš, Andrea Huppmannová, Eliška Hurňáková, Jaroslav Janáč, Igor Jex, Ján Julény, Dušan Jurčo, Peter Kajan, Želmíra Kirková, Ivan Klobušický, Michal Kotek, Juraj Krempaský, Karol Kyselica, Marián Lilov, Danica Morárová, Ivan Paulík, Monika Petríková, Roman Piffl, Peter Rajčáni, Ronald Ružička, Peter Sališ, Peter Sklár, Ivan Spudil, Ľubomír Ševec, Oliver Šípka, Boris Šramko, Igor Uderian, Rastislav Váňa, Juraj Vaník, Martin Zelman

4.C</br>
Triedna profesorka: Dana Laučeková</br>
Janka Balážová, Zuzana Barbušová, Alžbeta Bieliková, Alena Bodnárová, Roman Čech, Želmíra Dobáková, Ingríd Fabianová, Marián Fiala, Miroslav Hodál, Roman Holoubek, Ivan Hoťka, Peter Hulička, Dana Jamáriková, Gabriela Kissová, Zuzana Kopčová, Zuzana Koraušová, Dušan Kováč, Katarína Kubovičová, Zuzana Lojschová, Igor Matloň, Peter Naňo, Oto Nevický, Ivan Novák, Branislav Ohrablo, Dáša Paganíková, Slávka Petrášová, Vladimír Pištek, Mirka Popoffová, Denisa Prümmerová, Katarína Schreiberová, Jozef Szalay, Tomáš Šimkovic, Zuzana Šimkovičová, Zuzana Šinková, Jaroslav Švehla, Igor Švorc, Zlata Táborská, Pavol Vician, Renáta Zacharová, Dušan Zorkócy

4.D</br>
Triedna profesorka: Elena Šimkovičová</br>
Tatiana Baginová, Iveta Búriová, Aurel Danysz, Peter Dittrich, Pavol Heriban, Zora Hlásniková, Dita Hrivňáková, Rudolf Hrušovský, Ján Hulko, Jana Janíková, Tatiana Jodasová, Renáta Kontrošová, Ján Koráb, Peter Kovačič, Peter Kováčik, Ivan Marták, Michal Masaryk, Zuzana Mizerová, Andrea Mráziková, Peter Obdržálek, Roman Oleš, Milan Pafčo, Igor Pasek, Danica Petrová, Zuzana Pišútová, Zuzana Popovičová, Zuzana Pospíšilová, Zuzana Roháčová, Igor Rumpel, Ivan Schlosser, Jana Smatušíková, Ingrid Sucharova, Jana Šimkovicová, Karin Šoltésová, Peter Valter, Mária Vlachovičová, Tibor Waltera, Yvetta Zárecká

4.E</br>
Triedna profesorka: Zora Pašková</br>
Zoltán Agócs, Lucia Bacigalová, Michal Bagoňa, Jozef Bachníček, Peter Bajcsy, Radovan Baron, Gabriela Belicová, Zuzana Bezáková, Michal Dúžek, Dagmar Dvorakovičová, Mária Fabriciová, Peter Ferák, Peter Gahér, Ivan Gardian, Peter Gmiterko, Igor Habán, Ondrej Halušťok, Juraj Husár, Vojtech Jankovič, Andrea Krausová, Michal Križan, Henrieta Kutejová, Pavol Mišiga, Oleg Močkoř, Ingrid Mydliarová, Zuzana Ráczová, Ján Spurný, Juraj Šujan, Juraj Tóth, Iveta Štefancová, Ivo Ušiak, Pavol Vojtyla, Darina Vrbovská

AK - nadstavbové štúdium so zameraním na programovanie a obsluhu počítacích strojov</br>
Triedna profesorka: Oľga Grmanová</br>
Marián Darmo, Eva Dekanová, Soňa Drexlerová, Alena Erösová, Jozef Gál, Dalma Gubačová, Jozef Jentner, Jiří Kaigl, Eva Kériová, Danka Konečná, Bohuš Konečný, Karin Križanová, Iveta Krywultová, Klaudia Kuševova, Alena Levárska, Barbora Lovášová, Jana Mancová, Vladimír Mezei, Róbert Moravčík, Gabriela Morvaiová, Barnabáš Magy, Alexander Noság, Iveta Praženková, Juraj Sedlák, Edita Szalayová, Jozef Šimek, Róbert Zavadil, Jaroslav Zaťko, Anna Žitná