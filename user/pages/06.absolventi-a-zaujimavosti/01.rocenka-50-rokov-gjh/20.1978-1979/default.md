---
title: 1978/1979
---

Školský rok 1978 - 1979

4.A</br>
Triedny profesor: Juraj Pribula</br>
Roman Akáč, Vladimír Bajan, Anna Bílá, Blahoslav Číčel, Boris Dado, Peter Dekánek, Stano Drastich, Ivan Drobný, Peter Gonda, Mária Gyenesová, Emília Hamadová, Katarína Havaríková, Ladislav Herbanský, Danica Chovanová, Elena Jentnerová, Jozef Kacvinský, Jana Karpišová, Jozef Kožár, Eva Lacovičová, Eva Libová, Vladimír Macho, Igor Marek, Milena Měkotová, Vlastibor Minarovjech, Helena Mráziková, Helena Mungyerová, Zora Olléová, Ivan Ondruš, Dušan Palkovič, Jana Pisárová, Margita Pokojná, Miroslav Randuška, Dana Schrammová, Helena Somogyváriová, Alexander Suchal, Zuzana Šimková, Ľuboš Tereň, Jana Vavrová, Emil Vician, Karol Vosyka

4.B</br>
Triedna profesorka: Zuzana Šimkovicová</br>
Matej Berenčík, Vojtech Breza, Katarína Fellegiová, Alexander Fügedi, Ladislav Gažo, Boris Glos, Ondrej Hurňák, Lucius Chudý, Igor Izák, Martin Kalina, Boris Kalinec, Mária Koncová, Pavol Kubovič, Eugen Leitman, Vladimír Loula, Peter Nedobytý, Martin Petrík, Boris Rudolf, Ivan Rychetský, Peter Schreiber, Peter Skala, Peter Szivós, Katarína Široká, Jozef Škultéty, Dušan Šoltés, Martin Trnovec, Alexander Urbančok, Vojtech Valašík, Pavol Vatrt, Miloš Vozárik, Ivona Žabková

4.C</br>
Triedna profesorka: Tatiana Lörincová</br>
Norbert Bartalský, Ján Baťka, Tatiana Bieliková, Jaroslav Brabec, Peter Bugár, Ivan Binder, Juraj Čupka, Vladimír Fabián, Ingrid Grundová, Vladimír Hruban, Martin Kráľ, Ján Kuffa, Eugen Kullman, Ján Kuzmík, Peter Laurenčík, Ladislav Mandík, Peter Melichar, Ivan Mészároš, Stanislava Mišigová, Peter Müller, Vladislav Nešpor, Branislav Oktavec, Milan Pakši, Eva Parízková, Ľuboš Rzavský, Tatiana Sitárová, Juraj Tureček, Vladimír Valenta, Ján Vírostko, Ivan Zitkovský

4.D</br>
Triedna profesorka: Oľga Chovanová</br>
Lýdia Benčúriková, Vladimír Béreš, Juraj Fogarassy, Ján Frlička, Anna Fusková, Ingrid Gajarská, Daniel Haberland, Jana Hanajíková, Vladimír Haraksím, Eva Hégerová, Milan Hrivnák, Peter Ivica, Magdaléna Jursová, Dagmar Kissová, Dana Kovaríková, Zdeno Kováč, Katarína Kozáková, Viliam Kožík, Oľga Križanová, Peter Kučera, Ladislav Kuracina, Peter Litomerický, Marta Lörinczová, Rasťo Malatinec, Ladislav Manek, Jana Močkořová, Dagmar Paroulková, Zdeno Pavelka, Silvia Pechová, Marián Pekár, Vladimír Pokojný, Ivan Simonyi, Richard Sporina, Patrik Španko, Jana Šimkovičová, Peter Tamáši, Ivan Vlasko, Miroslav Weiser

4.E</br>
Triedna profesorka: Helena Kotzigová</br>
Tamara Abelovičová, Nadežda Bittnerová, Juraj Bosák, Jozef Brhel, Ingrid Čenčáriková, Vladimír Ečery, Jana Formelová, Eva Hoffmanová, Tibor Hubert, Peter Jakubec, Maroš Kondrót, Pavol Kučera, Peter Lenko, Ivan Lexa, Desana Lýsková, Mária Meszárosová, Jaroslav Mikula, Anna Mrázová, Eva Ničová, Štefan Polanský, Patrícia Porubská, Ladislav Rau, Juraj Riečan, Silvia Sestrienková, Vladimír Simonides, Alexander Statelov, Mária Šumerová, Ingrid Tanczerová, Jozef Tuhovčák, Ľubica Valášková, Marián Vicen

AK - nadstavbové štúdium so zameraním na programovanie a obsluhu počítacích strojov</br> 
Triedna profesorka: Oľga Grmanová</br>
Roman Barlok, Jana Bordáková, Magdaléna Csiková, Jana Čeřovská, Anna Darnadyová, Ladislav Dudok, Darina Fortuníková, Mária Gablíková, Viera Gregušková, Viola Hanyusová, Anna Kalisová, Jaroslav Komačka, Ľudovít Košík, Ľudovít Krippel, Radek Krlín, Anna Langová, Ľudovít Ladáni, Vendelín Lužbeták, Alojz Macho, Kvetoslava Miklošová, Štefan Mikulášik, Zuzana Molnárová, Ferdinand Nagy, Igor Pavelka, Mária Pregiová, Milan Pružinec, Miroslav Schramko, Igor Šenkarčín, Jarmila Vaňková, Ondrej Varga