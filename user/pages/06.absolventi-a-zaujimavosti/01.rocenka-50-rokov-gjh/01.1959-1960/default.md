---
title: 1959/1960
---

Školský rok 1959 - 1960

11.A</br>
Triedna profesorka: Mária Selecká</br>
Elena Bauerová, Marta Bončová, Peter Bošnák, Mária Čunderlíková, Dušan Danko, Nadežda Dohnálková, Ján Dudáš, Mária Ďurčová, Anna Fialová, Jozef Gál, Hugo Herrmann, Helena Henmannová, Pavlína Hojná, Božena Hučková, Hana Husičková, Justína Chmelárová, Pavlína Chrenová, Helena Jancová, Katarína Kásová, Pavol Koleda, Ivan Kukla, Ivan Kulašík, Viera Lenková, Sidónia Lesáková, Jozefína Malíšková, Gabriela Minárská, Eduard Molnár, Ivan Ondruš, Katarína Orčíková, Roman Robota, Viera Sokolová, Oľga Staneková, Katarína Svitoková, Ivan Škripeň, Erika Thernová, Marta Vanelíková, Jaroslav Vadovič, Anna Vargová, Agnesa Wlachovská, Marta Žabková

11.B</br>
Triedna profesorka: Mária Balalová</br>
Pavel Danysz, Jozef Drinka, Peter Duchaj, Pavel Formánko, Marta Grambličková, Juraj Gyüre, Valéria Habajová, Margita Huláková, Miloš Holman, Viera Holotíková, Gertrúda Hrušková, Pavol Jablonský, Pavol Klein, Helena Kolláriková, Mária Krkošková, Viera Kúdelová, Jolena Kunštárová, Rudolf Kusák, Stanislav Macko, Elena Malatinová, Helena Marčeková, Monika Miezgová, Mária Mihálková, Emília Michelová, Katarína Nagyova, Marta Némethová, Melinda Pálfiová, Katarína Petruláková, Pavol Porubský, Júlia Račkayová, Mária Rehušová, Eva Repová, Renáta Röderová, Ján Schlögl, Saskia Schmidlová, Helga Schmiedová, Oľga Šurinová, Helena Vančíková, Eva Videcká, Rudolf Žiak

11.C</br>
Triedny profesor: Jozef Balala</br>
Ilza Blaschková, Ivan Bolcek, Bohuslava Brázdová, Peter Čunderlík, Ján Danko, Lýdia Franková, Mária Fratričová, Eva Galusová, Mirjam Grozdaničová, Magda Huttová, Vladimír Kozmál, Ján Krížik, Miroslav Kučerka, Karol Leitgeb, Elena Letňanová, Anna Maštenová, Jana Matúšková, Juraj Madzenauer, Fedor Medek, Milan Menyhard, Ján Meszáros, Eduard Moravčík, Ivan Novák, Viliam Ochaba, Ladislav Pánik, Edita Pavličková, Oľga Petrová, Ingeborg Salesová, Juraj Špička, Estera Štecková, Rudolf Štencl, Margita Tobolková, Ľudovít Tomkó, Mária Vaculčiaková, Jana Vašíčková, Milan Veselský, Andrea Virdzeková, Ingeborg Zabadalová, Nataša Zelenková

11.D</br>
Triedny profesor: František Mikulka</br>
Gerhard Alexy, Nadežda Čabelková, Viera Čatlošová, Eva Černochová, Marcela Csöváriová, Lucia Čunderlíková, Koloman Čurgali, Edita Dunajková, Visla Dubecká, Ľubica Ďuriačová, Zita Földešová, Peter Frešo, Viera Hajtšová, Valeria Hauerová, Erika Haubtová, Táňa Cholujová, Eva Jamrichová, Mária Klasová, Judila Korpášová, Oľga Kováčová, Jana Lukáčová, Zdravka Martánovičová, Peter Matlon, Katarína Mrázová, Ľudmila Neničková, Helena Pecárová, Ladislav Reiman, Beata Rippová, Dana Roháčová, Mária Schweizpacherová, Dana Slobodová, Terézia Šubínová, Karol Teuk, Anna Tollová, Mária Vávrová, Eva Víllková, Zdena Záziková

11.E</br>
Triedny profesor: Júlia Jacková</br>
Peter Áč, Juraj Barath, Gabriela Barineková, Adriena Breštenská, Pavel Čerešník, Viera Deáková, Dušan Drahoš, Eva Dzurošková, Pavel Fargaš, Marta Flessnerová, Igor Gazdík, Eva Gondová, Milan Gríger, Anna Homolková, Magdalena Jakubičková, Mária Kašičková, Marianna Khandlová, Alžbeta Košalková, Darina Košťálová, Karol Kučera, Michaela Kuzmová, Otto Láska, Valeria Leskovská, František Ludvig, Blanka Majerská, Elena Martišová, Zbyněk Müller, Darina Peterková, Zdena Peterková, Viera Petrášová, Elégia Poláková, Marta Radičová, Karol Rusznyák, Viera Rybová, Imrich Sedlár, Oľga Sevruková, Emília Tyralová, Zlata Vodová, Mikuláš Wagner, Václav Weinzettl, Ivan Žembery

11.F</br>
Triedny profesor: Anton Šíma</br>
Daniela Bacharová, Ľudmila Čermáková, Štefan Danko, Šarlota Dobišová, Dušan Fickuliak, Viera Holúbková, Stanislav Holý, Ivan Huliak, Jaroslav Janků, Ján Jursa, František Kadleček, Juraj Kolek, Eva Kollerová, Ján Korček, Marta Majerová, Ján Matúš, Soňa Menzelová, Ľudmila Mičinská, Martin Mikuš, Eva Mislovičová, Jana Oláhová, Elena Petrušová, Edita Sakanová, Gizela Schmidtová, Friderika Seemannová, Ľudmila Sládková, Eva Škapcová, Peter Takáč, Monika Tatarková, Eva Temerová, Valeria Vargová, Sylvia Vorlíčková, Anna Vrbická, Viera Žilincová