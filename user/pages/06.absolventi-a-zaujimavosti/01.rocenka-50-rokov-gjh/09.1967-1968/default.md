---
title: 1967/1968
---

Školský rok 1967 - 1968

3.A</br>
Triedna profesorka: Zuzana Šimkovicová</br>
Katarína Alexyová, Peter Babinec, Rudolf Balaj, Blanka Ballová, Michaela Bilčíková, Martin Bukovčan, Peter Cveček, Vladimír Čech, Peter Človieček, Ján Gašparík, Michal Grell, Otokar Grošek, Anton Huťa, Ivan Kalavský, Viera Masaryková, Rudolf Németh, Ján Petrík, Ivan Petrovič, Ľubomír Pieružek, Rudolf Požgay, Marián Puškár, Vladimír Repáš, Ján Salava, Ľubomír Sestrienka, Kvetoslava Slivková, Michal Smrečanský, Juraj Stanislav, Markéta Stašíková, Juraj Sztuka, Jolana Šimorová, Jozef Vrábel

3.B</br>
Triedna profesorka: Elena Šimkovičová</br>
Vladimír Baláž, Eva Balíková, Igor Bobro, Jiří Čapek, Vendelín Čunderlík, Zuzana Dorogiová, Andrej Fandák, Michal Fendek, Kamil Frimmel, Eva Hanusová, Vladimír Hanzel, Andrej Hegedüš, Štefan Chmel, Anna Chudíková, Marián Jakubec, Igor Jirkovský, Jozef Kanás, Martin Končko, Peter Krajčirovič, Imrich Kuruc, Jozef Kytka, Peter Lančarič, Barbara Laššuová, Eva Lukáčová, Imrich Molnár, Juraj Masler, Peter Ňukovič, Karol Pastor, František Paulíny, Ivan Pavlovič, Ivan Penz, Bohuslav Prachár, Ľudmila Prelovská, Vladimír Pukančík, Anna Schwarzová, Ladislav Sihocký, Ivan Šutka

3.C</br>
Triedna profesorka: Anna Gavorová</br>
Brigita Bohunská, Peter Bořuta, Jozef Černák, Oldrich Černý, Ctibor Dobrovodský, Oleg Fabian, Rudolf Hasprún, Helena Haverlová, Oľga Hazuchová, Juraj Hromec, Miroslav Chmel, Tamara Jilemská, Daniela Jurická, Jarmila Kadnárová, František Kmetony, Stanislav Kobza, Ján Kocián, Július Kohút, Alica Kollárova, Milan Kováč, Mária Kováčová, Marcela Kubalová, Mária Máčalová, Milan Majer, Vladimír Móži, Ladislav Pánd, Mária Patková, Štefan Rác, Dušan Sabolčík, Daniela Sabová, Zuzana Studená, Živa Sumbalová, Ferdinand Szamák, Eva Šimová, Ján Šúr, Ladislav Vaškovič, Štefan Velgos, Andrej Virsík, Oľga Vitálošová, Milan Vojtech, Alojz Wimmer, Miroslav Žabka

3.D</br>
Triedna profesorka: Oľga Heribanová</br>
Dušan Božík, Martin Bielik, Margita Čečetková, Elena Dibarborová, Alena Gálisová, Jaroslav Havlíček, Jozef Hornáček, Ľubomír Hrmo, Elena Husíková, Štefan Jankela, Juraj Klačanský, Jana Kovalovská, Silvia Krajčovičová, Vojtech Lajoš, Jaromír Malovec, Mária Markusová, Ladislav Meňuš, Silvia Mikulášková, Helena Ondriašová, Ladislav Orlík, Viera Ostrovská, Viera Praženková, Mária Puhová, Ján Rak, Róbert Rehák, Andrej Révay, Jitka Smetanova, Beáta Stražanová, Jozef Supek, Peter Šimlovič, Juraj Škrabák, Peter Šomek, Pavol Špaček, Pavol Tkáč, Marta Tokárová, Peter Záhoranský, Danica Zubacká

3.E</br>
Triedna profesorka: Libuše Kostková</br>
Iľja Cigna, Martin Čulen, Zuzana Demeterová, Marián Glinda, Dagmar Grzwinová, Helena Havettová, Soňa Händlerová, Anna Immová, Elena Jankovičová, Pavel Janota, Eva Kadurová, Katarína Kirchmayerová, Ľudovít Kocián, Anna Kováčová, Marta Krajčová, Dušan Lečko, Zuzana Lipková, Mária Liptáková, Ľubica Nemcová, Veronika Petrová, Milan Pilárik, Róbert Plačko, Zuzana Reiselová, Miroslav Rihák, Anna Stromčeková, Hana Stryhalová, Mária Súsedková, Daniela Sušeková, Michal Šermer, Eva Škultétyová, Mária Tarábková, Eva Tyllová, František Veselý, Viliam Vittek

3.F</br>
Triedna profesorka: Viera Hánová</br>
Mária Berská, Rudolf Buzinkay, Irena Céreová, Eva Drieňová, Eva Feitscherová, Danica Filípková, Karol Francisci, Tatiana Herrmannová, Katarína Horvayová, Želmíra Ilavská, Zuzana Jakešová, Jozef Karpat, Soňa Kmeťová, Jana Kocholová, Elena Kollárová, Katarína Lacková, Hana Lehotayová, Daniel Luther, Peter Majerský, Jaroslava Mešťanová, Daniela Migrová, Irena Molnárová, Mária Nemešová, Štefan Ondek, Daniela Pallová, Rozália Papová, Michaela Pavlásková, Jana Plichtová, Eva Profantová, Mária Rosenbaumová, Agnesa Schrammová, Eva Svitková, Vladimír Velkov, Hedviga Žuchová

3.G</br>
Triedna profesorka: Ľudmila Mičíková</br>
Peter Bezák, Juraj Bezdek, Kamil Braxátor, Peter Buliščák, Pavol Drotár, Gabriela Haluzová, Pavol Hanúsek, Veronika Heinzelmannová, Miloš Hrvoľ, Eva Hudeková, Zuzana Jeleneková, Beata Kleinedlerová, Emília Lidajová, Lýdia Lukáčová, Ľubica Marišová, Lýdia Mašiková, Ladislav Matúšek, Marián Mikláš, Jaroslav Mrva, Pavol Múdry, Elena Otherová, Karol Paluš, Daniel Ráno, Alena Richterová, Helena Smatanová, Dagmar Srnenská, Helena Stríbrnská, Štefan Szeiff, Daniela Šamová, Oľga Štrbová, Viera Švecová, Alexander Uhlík, Daniela Vargošová, Dana Zaťková, Jolana Zirinová

AK - nadstavbové štúdium so zameraním na programovanie a obsluhu počítacích strojov</br>
Triedny profesor: Zdeno Vlachynský</br>
Terézia Algayerová, Mária Barančíková, Marián Biely, Ján Blažek, Tibor Čutora, Jana Derková, Miroslav Donoval, Cecília Dorocáková, Anna Dubovská, Emília Dudová, Mária Grešová, Dušan Hlista, Veronika Horečná, Margita Hreusová, Dana Hulková, Anna Chanová, Alica Chrappová, Priska Ivanicsová, Katarína Kametlerová, Mária Králová, Ľudmila Lopošová, Katarína Molnárová, Ján Navrátil, Peter Petríček, Dušan Reich, Elena Šoltysová, Petronela Šulecová, Miroslav Truc, Katarína Uglerová, Tibor Závodský