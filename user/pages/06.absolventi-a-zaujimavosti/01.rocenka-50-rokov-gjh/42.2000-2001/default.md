---
title: 2000/2001
---

Školský rok 2000 - 2001

4. A</br>
Triedny profesor: Radko Ambrózy</br>
Martin Bahurinský, Kristína Ballányová, Matúš Benčík, Mária Binderová, Petra
Dermišková, Veronika Deščíková, Roman Dibarbora, Michal Glaus, Ţofia Gulášová,
Daniela Hantuchová, Branislav Hluchý, Martin Hromek, Martin Huska, Zuzana
Chovanová, Jakub Institoris, Ján Kadlec, Michaela Kaličiaková, Veronika Kalincová,
Rastislav Kazík, Lukáš Kowalski, Branislav Kriška, Adam Lamačka, Tomáš Lukačka,
Matej Macháč, Martin Machala, Matúš Mikula, Peter Pivarči, Pavol Prikryl, Peter Prikryl,
Jakub Šašinka, Ľubomír Šorf, Peter Tomašovič, Oliver Urbánek, Ondrej Ţáry, Miroslav
Žitňanský

4. B</br>
Triedna profesorka: Alena Hechtová</br>
Martin Ababei, Tomáš Barantal, Andrej Belas, Vladimír Beleš, Adam Berka, Vladimír
Dráb, Veronika Farková, Juraj Fendek, Katarína Gombárová, Miroslava Grambličková,
Jana Gregorová, Silvia Grünzweigová, Marek Hladík, Zuzana Hoghová, Mikuláš Ivaško,
Radim Kníţat, Tomáš Koreň, Michal Kovačič, Bronislava Kušnieriková, Michal Kyselica,
Martin Lopatník, Pavol Marko, Tomáš Ölvecký, Miroslava Pagáčová, Dáša Peťková,
Alena Perďochová, Miroslav Pomšár, Silvia Rábeková, Peter Rendoš, Martin Roško,
Miroslav Szabó, Ján Terkanič, Soňa Trhanová, Mária Trusiková

Oktáva</br>
Triedny profesor: František Kosper</br>
Radoslav Buranský, Boris Burdiliak, Branislav Burdiliak, Tomáš Cár, Barbora Cerovská,
Katarína Csányiová, Daniel Duranka, Richard Ďuriš, Miloslav Falťan, Jana Feriancová,
Marián Fridrich, Martin Fúrik, Tomáš Hajas, Martin Hán, Eva Haramiová, Matej Hrica,
Tomáš Hromek, Tomáš Konečný, Martin Kriţan, Andrea Kruľová, Peter Lisý, Ivan
Mitošinka, Branislav Nikš, Petra Novotná, Tomáš Profant, Filip Schragge, Barbora
Skalníková, Mária Slodičková, Vojtech Slovik, Eva Szarková, Matúš Tekeľ, Michal
Tvaroţek, Matej Valdner, Barbora Zahradníková, Andrea Ţitná

4. IB</br>
Triedne profesorky: Oľga Zelmanová, Zuzana Burietová</br>
Petra Bajdichová, Nina Bakošová, Peter Brezina, Viktor Franek, Anna Franeková, Marek
Frecer, Ariane Charlotte Händler, Petra Havelská, Igor Holas, Martin Hornáček,
Ľudmila Hostová, Ľubomír Hromádka, Ján Humaj, Michaela Jacová, Katarína
Kubešová, Ivana Kopecká, Martin Kochan, Petra Marinová, Martin Michalica, Lauri
Moyle, Daniela Nágelová, Andrea Najvirtová, Dávid Nguyen, Ľuboš Okoličány, Roman
Ondruš, Gabriela Pavlendová, Tomáš Polák, Andrea Proková, Anna Ringlerová, Kristína
Sojáková, Mária Svrčková, Ivan Záhorec