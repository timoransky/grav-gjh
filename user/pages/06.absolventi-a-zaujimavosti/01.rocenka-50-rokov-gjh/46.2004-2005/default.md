---
title: 2004/2005
---

Školský rok 2004 – 2005

4. A</br>
Triedne profesorky: Dana Tomanóczyová, Miriam Čuntalová</br>
Samuel Arbe, Peter Balko, Martin Baţík, Norbert Benkovský, Roman Bobiš, Zuzana
Boušková, Martin Dekan, Katarína Gergelyová, Darina Graczová, Martin Gramblička,
Michal Holub, Barbora Hrdá, Daniel Igaz, Richard Kafka, Juraj Kollár, Peter Kotvan,
Peter Kucharovič, Róbert Najvirt, Vladimír Oravec, Emília Oťapková, Tomáš Palkovič,
Veronika Petráková, Zuzana Plšková, Peter Ruţička, Ondrej Stanek, Matej Stračiak,
Kristína Šimeková, Matúš Šimkovic, Barbora Tomíková, Martin Valentíny, Vojtech
Villaris, Andrej Wertlen, Peter Zeman

4. B</br>
Triedna profesorka: Alena Vicenová</br>
Pavol Bizoň, Barbora Bodnárová, Radoslav Ciglanský, Tomáš Dittinger, Martin
Drozdík, Michal Dvorský, Igor Fekeč, Martin Hodas, Juraj Hodúl, Martina Hrabalová,
Vladislav Kaššovic, Adam Kiss, Richard Kister, Martin Kopča, Michal Kottman, Michal
Lipták, Matúš Maras, Daniela Mikušová, Lucia Mojteková, Martin Nováček, Michal
Puffler, Ivana Rauová, Monika Satková, Michal Stríţenec, Jakub Šimko, Dorota
Šoltésová, Eva Šteffeková, Stanislava Tichá, Barbora Tomančeková, Ondrej Trnavský,
Tomáš Vančo, Ľubomír Vranka, Jana Vranová, Dušan Zeleník, Martin Zibala

4. C</br>
Triedny profesor: František Kosper</br>
Maroš Bajtoš, Katarína Cambelová, Denis Donauer, Miroslava Englmanová, Alexandra
Filová, Martin Halač, Katarína Hánová, Pavol Hromádka, Martin Kovanič, Veronika
Lacková, Martin Marikovič, Bruno Michálek, Katarína Morháčová, Nečas Tomáš,
Daniela Olejníková, Petra Plešková, Michal Podlucký, Ján Sehnal, Zuzana Schmidtová,
Ivana Sovová, Katarína Šimončičová, Marek Štalmašek, Andrea Štefánková, Jozef
Tomek, Andrea Trandţíková, Martin Valacsai, Milan Valla, Ondrej Vavro, Matej Vitko,
Pavel Zajac, Tomáš Ţoldoš

4. D</br>
Triedna profesorka: Hana Mlynarčíková</br>
Vladimír Aleksandrov, Peter Bobošík, Stanislav Buštor, Jana Dadová, Beatrix Fatranská,
Miriam Ferenčíková, Gabriel Filadelfi, Andrej Hrivnák, Martin Hudec, Martin Iring,
Peter Ivanov, Michal Katuša, Hana Kováčová, Zuzana Letašiová, Martin Madaras, Jana
Majeríková, Lucia Matúšková, Lenka Minďášová, Michal Mráz, Ondrej Mrlian, Michal
Nánási, Lýdia Ostertágová, Ivica Paulovičová, Marian Ridilla, Adéla Sedláková, Marek
Slávik, Peter Valentovič, Anna Vaneková, Martin Vícen, Marek Willmann, Juraj Zaťko,
Marek Zeman, Monika Zíková

4. IB</br>
Triedna profesorka: Katarína Kublová
Lenka Beláňová, Daniel Boţík, Milan Bratko, Peter Crkoň, Daniel Daučík, Ivana
Ertlová, Tomáš Fabšič, Martin Ftáčnik, Štefan Gurský, Barbora Habáňová, Ivana
Hlavatá, Vladimír Hruda, Mária Hulková, Jakub Jajcay, Eva Janošková, Martina Jašová,
Michal Kesely, Anna Kovačičová, Ivana Lajčiaková, Jana Lukačišinová, Alejandro
Garcia Misas, Martin Palovič, Peter Petrík, Ľubica Polláková, Matej Rimár, Miroslava
Saxunová, Stanislava Sojáková, Eva Steinová, Ľudmila Straková, Pavel Struhár, Patrícia
Svrčková, Nataša Šramková, Igor Trebatický, Zuzana Trnovcová