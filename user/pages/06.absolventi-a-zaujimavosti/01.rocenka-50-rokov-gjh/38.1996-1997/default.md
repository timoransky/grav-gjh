---
title: 1996/1997
---

Školský rok 1996 - 1997

4.A</br>
Triedna profesorka: Elena Vojtelová</br>
Michaela Andelová, Dušan Barok, Peter Benco, Soňa Bojnová, Juraj Ďuriš, Daniel Ferák, Anton Gajdošík, Michael Gallo, Martin Géč, Jozef Házel, Martin Hronkovič, Roman Hudec, Marián Janovič, Diana Kandalaftová, Miloslav Kátlovský, Ivan Kolesár, Denisa Kovářová, Vladimír Kozub, Lorant Krajcsovics, Adam Lukačka, Paulína Majorová, Peter Merschitz, Michal Mikláš, Roman Mojžiš, Peter Mráz, Michaela Neuwirthová, Tomáš Ostrožlík, Miroslava Psotová, Marián Roško, Daniel Sedláček, Zuzana Sestrienková, Peter Sklenár, Zuzana Tkáčiková, Roman Válik, Richard Willmann, Igor Zajac

4.B</br>
Triedny profesor: František Kosper</br>
Barbora Bachová, Martin Balog, Soňa Brichtová, Soňa Bučková, Vladimír Držík, Tomáš Gálik, Peter Hadviger, Ján Hudec, Marián Chrvala, Daniel Janiak, Ľubor Kleinert, Ivan Klimo, Matej Kontriš, Tomáš Marczell, Vladimír Marko, Ľuboš Mego, Alexander Moravčík, Kristína Nagyová, Norbert Ördög, Peter Požgay, Viktor Požgay, Eva Prikrylová, Ladislav Prvák, Natália Rolková, Martin Sádovský, Tomáš Trúsik, Martin Vašíček, Juraj Vávra, Róbert Vereš, Katarína Vozárová, Martin Vrbovský, Peter Zika, Irina Vladimirovna Malkina

4.C</br>
Triedna profesorka: Ľubica Kováčová</br>
Martin Albrecht, Andrej Balog, Alena Bócová, Vladimíra Božíková, Peter Dráb, Marián Horváth, Jana Hrdličková, Andrej Hrnčiar, Renáta Ivanová, Daniel Janotka, Viliam Jaross, Peter Kaliňák, Jana Klimentová, Vladimír Kocian, Katarína Kostovská, Bronislava Kovalčíková, Marián Kučerka, Roman Květon, Peter Lintner, Jozef Novák, Miroslava Oleksová, Martina Porubská, Kristián Rummer, Marek Stanczyk, Maroš Straka, Rastislav Synek, Juraj Široký, Martina Škultétyová, Barbora Uličná, Alena Valentová

4.D</br>
Triedna profesorka: Mária Ondriašová, Erika Fajnorová</br>
Jozef Baláž, Boris Belas, Martin Bella, Tomáš Benčík, Tomáš Blaho, Pavel Bobák, Michal Cagarda, Miloš Djuračka, Blažej Dolista, Martin Eifler, Róbert Filc, Soňa Formánková, Martin Halada, Michaela Horniaková, Martin Horský, Peter Jankech, Rastislav Kaššák, Martin Kincel, Dana Kleinertová, Martin Kováč, Martina Kováčová, Peter Kúdola, Andrea Lispuchová, Tatiana Majerská, Michal Perďoch, Rastislav Pridala, Alexandra Putzová, Richard Satúry, Martin Sporinský, Martin Staníček, Jaroslav Škrovánek, Peter Šurnovský, Peter Varga, Juraj Vaško, Viktor Zigo, Andrej Zvolenský

4.IB</br>
Triedna profesorka: Renáta Balková</br>
Veronika Bubnášová, Juraj Cvečka, Branislav Čík, Štefan Daňo, Ivana Foťková, Alexandra Gronská, Mária Hanulová, Michal Komada, Kristián Krajčovič, Katarína Kurkinová, Martin Mucha, Peter Mužík, Petra Nahálková, Marek Navrátil, Martin Oravec, Katarína Orogvániová, Viktor Piršel, Linda Sodomová, Michal Staroň, Jana Švecová, Martin Švitel, Alexandra Turzová, Ján Zambor, Juraj Ziegler