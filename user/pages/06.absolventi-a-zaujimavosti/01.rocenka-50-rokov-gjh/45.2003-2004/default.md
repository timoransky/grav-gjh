---
title: 2003/2004
---

Školský rok 2003 – 2004

4. A</br>
Triedna profesorka: Renáta Horváthová</br>
Tatiana Baďová, Miroslav Baláž, Ivana Bezáková, Filip Blaťák, Matúš Čelko, Anna
Danková, Peter Glaus, Jozef Gocal, Petra Gombárová, Pavol Grman, Filip
Gschwandtner, Rastislav Halamiček, Paulína Kalivodová, Tomáš Kováč, Zuzana
Krajčovičová, Dárius Kráľ, Kamil Kuboň, Miroslava Lisá, Michal Mandík, Michal
Mitrík, Zuzana Nemčeková, Martin Ochránek, Štefan Olejník, Robert Ördög, Stanislav
Panák, Dagmara Purgatová, Slavomír Skovajsa, Andrej Strausz, Dagmar Šišková,
Richard Štefanec, Ivan Trejbal, Barbora Trubenová, Peter Varga, Monika Viskupová,
Barbora Zsilková

4. B</br>
Triedna profesorka: Renáta Monošíková</br>
Peter Ambroţ, Dominika Bartová, Miloš Bulík, Peter Čunderlík, Adela Dibarborová,
Peter Fodran, David Gavaľa, Tomáš Grečko, Ľubica Chriašteľová, Ján Janík, Jaroslav
Kaššovic, Eva Kázmerová, Alexander Kiš, Andrej Klinka, Michal Kobora, Stanislava
Kobzová, Ondrej Kolárik, Martin Kontsek, Emília Košťálová, Jakub Koţíšek, Peter
Krajčír, Martin Krivička, Hana Kucharovičová, Mariana Kuchyňárová, Marko Kvito,
Jana Mácsadiová, Marek Mardiak, Natália Poliaková, Barbara Präsensová, Dana
Smaţáková, Viera Sporinská, Radoslav Štefkovič, Jana Vašíčková, Dušan Zajac, Matej
Žáry

Oktáva A</br>
Triedny profesor: Ján Mayer</br>
Peter Ababei, Eva Balaţovičová, Zdenka Bartošová, Lenka Beková, Dávid Bezák,
Martin Bies, Michaela Biharyová, Adam Birnstein, Matej Bittner, Pavol Čudrnák, Peter
Čudrnák, Marek Ďuriš, Tomáš Földeš, Marcel Hausknecht, Zuzana Hrachovinová,
Tomáš Hrnčíř, Katarína Jurdáková, Michal Kramarič, Andrej Krč, Boris Kruľ, Diana
Lakatošová, Rastislav Lenhardt, Rastislav Monošík, Eva Palacková, Juraj Petrík, Evarist
Polakovič, Miroslav Schiffer, Martin Slávik, Katarína Strapková, Samuel Sulík,
Stanislav Števík, Jakub Tekeľ

Oktáva B</br>
Triedny profesor: Peter Demkanin</br>
Peter Akáč, Michal Bajan, Tomáš Balogh, Martina Báňasová, Lucia Bellová, Peter Biľo,
Lucia Bohušová, Ondrej Buchel, Veronika Compelová, Jozef Daňko, Juraj Gbelský,
Michal Hrobár, Michaela Krajčovičová, Lucia Kuklicová, Michal Mardiak, Igor
Miškovič, Hedviga Mrázová, Peter Nevláčil, Roman Nízky, Matúš Pavlovič, Roman
Petrinec, Igor Sekáč, Martin Sekerka, Daniel Skákala, Andrej Slota, Daniela Soldánová,
Juraj Straka, Michael Tyrala, Ján Valaška, Miroslav Vích, Tomáš Zvozil

4. IB</br>
Triedny profesor: René Sidorov</br>
Lenka Blahutová, Petra Borská, Milan Burda, Michal Čermák, Matúš Dekánek, Pavol
Dravecký, Dávid Džamba, Zita Ferenčíková, Anna Filippová, Anna Hanulová, Peter
Harvan, Kristína Hečková, Mária Kačeňuk, Hana Kolibiarová, Katarína Kopajová,
Dominik Kosorín, Veronika Kovácsová, Jakub Kováč, Lucia Kováčová, Miroslava
Mifková, Juraj Michálek, Kristína Mikulová, Lenka Mišániková, Zuzana Molnárová,
Vladimír Sadloň, Helena Šarkanová, Tibor Šiška, Anton Štefánek, Barbora Vagaská,
Darina Valeková, Ján Veselý, Tamara Vraţdová, Lenka Žemberová