---
title: 1990/1991
---

Školský rok: 1990 - 1991

4.A</br>
Triedna profesorka: Helena Klobušická</br>
Milan Cisár, Mikuláš Dudáš, Valentín Farba, Žofia Franková, Zbyněk Hadrbolec, Pavol Hanzalík, Alena Hunáková, Petra Janišová, Dušan Józsa, Lucia Krempaská, Marián Kvačka, Ľudovít Lauko, Martin Polák, Sylvia Pompurová, Ľuboslava Rafajdusová, Katarína Rauová, Miroslav Rusňák, Jana Seginková, Daniel Smorádek, Ondrej Sprušanský, Zuzana Straková, Regina Sunková, Róbert Szöke, Katarína Šimová, Vladislav Turan, Michaela Veselská, Jana Zemánková, Andrej Zužo, Elena Žáková, Bohdana Wlachovská

4.B</br>
Triedni profesori: Július Šoltés, Elena Kalašová</br>
Lenka Balážová, Michal Barabás, Ivan Baričič, Barbora Belyusová, Martin Danihel, Beáta Dreninová, Peter Dudák, Monika Faberová, Jana Fabianová, Martin Fajkus, Zuzana Fuknová, Zuzana Harangozó, Štefan Holakovský, Marek Hudec, Martin Chovanec, Henrieta Kaholeková, Gabriela Kačurová, Štefan Khandl, Miroslav Kočan, Helena Koladová, Kristína Kostková, Tomáš Kováč, Tomáš Lahola, Peter Langfelder, Igor Malý, Iveta Morávková, Martin Stanek, Marek Szabó, Mária Široká, Andrea Tomečková, Martin Vojtko, Dana Vranová, Ivan Weiss, Svetlana Zemanová, Martina Zsírosová

4.C</br>
Triedny profesor: Marián Slušný</br>
Erik Iglesias Abella, Drahomíra Babušíková, Róbert Bestro, Branislav Brocko, Eva Búranová, František Deglovič, Roman Frimm, Sylvia Gočová, Miroslav Jurík, Ján Hergott, Richard Hideghety, Peter Kaličiak, Helena Kollárová, Barbora Krištofovičová, Juraj Leško, Mária Miklošková, Martin Mosný, Stanislav Nevláčil, Lucia Nogová, Matej Ondruš, Jozef Ošlejšek, Marta Peňkovská, Sylvia Práznovská, Andrea Semanová, Jana Rajňáková, Martin Sedlák, Patrik Slovák, Peter Šandrik, Martin Šnajdr, Daniela Štrkulová, Mária Tarábková, Rastislav Ujváry, Radoslava Vaculová, Anna Vachulová, Pavol Veselovský, Andrej Vojtičko, Alžbeta Zajacová

4.D</br>
Triedna profesorka: Eva Žitná</br>
Katarína Baranová, Michaela Brokešová, Miroslava Černá, Katarína Dobrovodská, Soňa Eližerová, Jana Florková, Juraj Gregáň, Peter Guštafík, David Hlubocký, Marián Jalovičiar, Matúš Janota, Róbert Jankech, Monika Kamenická, Richard Kastel, Jozef Kausich, Eduard Kostolanský, Martin Laifr, Libor Láznička, Miriam Lexmanová, Dáša Matušíková, Branislav Oliva, Martin Olšavský, Jana Púčiková, Petra Puškárová, Renáta Sládeková, Erik Stupka, Martin Sůra, Sylvia Szviteková, Radovan Ševčík, Ľubica Šimkovičová, Ján Štrbák, Jela Tomaškovičová, Juraj Vančo, Marianna Vannayová, Andrea Žáčiková