---
title: 1975/1976
---

Školský rok 1975 - 1976

4.A</br>
Triedny profesor: Ľudomír Krištof</br>
Štefan Árendáš, Július Čabala, Ivan Čársky, Gabriela Časnochová, Mária Durilová, Viktória Fordinálová, Eva Fülöpová, Vladimír Gergeľ, Branislav Horný, Ivan Kalaš, Soňa Kanošová, Pavol Karaba, Vladimír Koťka, Juraj Križo, Ján Lehoťan , Helena Machová, Igor Makúch, Eduard Masár, Zuzana Onderíková, Dagmar Partyková, Igor Patocs, Anton Pisár, Karol Polák, Juraj Polakovič, Adriana Ruttkay-Nedecká, Hana Sedláčková, Jana Šafáfová, Viera Václavová, Noemi Viesnerová

4.B</br>
Triedna profesorka: Anna Snohová</br>
Alžbeta Bišťanová, Andrej Blaho, Miloš Blanárik, Igor Brilla, Milan Čistý, Miroslav Drkoš, Zdenek Faltus, Vladimír Frecer, Alžbeta Fusková, František Gahér, Radomír Háčik, Juraj Hájek, Bohuslav Huraj, Edita Izakovičová, Dušan Janičkovič, Pavol Kossey, Pavol Krchnák, Peter Martíšek, Bibiana Molčanová, Ondrej Náther, Marián Slodička, Martin Šamaj, Jana Slovíková, Robert Šimončič, Andrej Šoltész, Vendelín Šnapko, Rastislav Vadovič, Marián Valovič, Milan Vanko, Igor Vavro, Martin Vodrážka, Juraj Wallner, Ivan Závadský, Miroslav Zeman

4.C</br>
Triedny profesor: Vladimír Jodas</br>
Ľubomír Barták, Viktor Birnstein, Matúš Búci, Peter Čačala, František Čech, Milan Deščík, Miroslav Drotár, Dušan Dudík, Darina Feková, Iveta Gavaľová, Juraj Gavora, Vladimír Gaži, Elena Gregorová, Blanka Gusková, Dušan Hirjak, Vladimír Hučko, Ján Kadera, Jozef Kiss, Zdena Kochanová, Igor Kossaczký, Marián Kostolányi, Viera Kovačovičová, Gustáv Martinček, Vladimír Martinček, Peter Miklovič, Pavol Minárik, Ivan Ostrovský, Dana Ottová, Peter Polena, Ľubomír Moncoľ, Juraj Schwartz, Milan Stančík, Zuzana Szüczová, Ivan Šramko, Soňa Švidroňová, Jana Tiklová, Monika Tomíková, Václav Urban, Vladimír Votruba, Michal Zíka

4.D</br>
Triedna profesorka: Magda Zaťková</br>
Klára Bajcsyová, Štefan Čakvari, Ľudovít Čistý, Eva Dobrotková, Stanislav Duba, Eva Fabianová, Elena Fulmeková, Mária Gálisová, Boris Gergel, Zuzana Hergottová, Ivan Hlavatý, Vlasta Hôrčiková, Michal Hudec, Milica Illeková, Elena Janíková, Vladimír Janský, Mária Kotlebová, Jana Kreháková, Daniela Krcheňová, Zuzana Kudláčková, Štefan Letenay, Peter Lukeš, Anna Májeková, Juraj Murín, Ctirád Oravec, Beata Pavlíková, Kristína Pechová, Iveta Pillichová, Peter Polačko, Ján Pospíšil, Jana Prušková, Ján Rizman, Silvia Šebelová, Miroslav Šimkovic, Zuzana Šimkovicová, Peter Škutil, Desana Ulehlová, Miloš Valko, Ľubica Vanáková, Dagmar Vranová

4.E</br>
Triedny profesor: Ján Koval</br>
Eduard Baláž, Liliana Bártová, Jana Boledovičová, Ján Bíly, Vladimír Brychta, Peter Cesnak, Elena Fuseková, František Gogora, Tomáš Gurský, Igor Haruštiak, Peter Hering, Eva Horská, Jana Horváthová, Miloš Hricovíni, Juraj Hutník, Ivica Chorvátová, Libor Chyla, Libor Janský, Beáta Kedrovičová, Jana Kocúrová, Katarína Kovácsová, Ján Kriška, Veronika Kováliková, Alica Majerníková, Darina Markuseková, Mária Máliková, Gabriel Minďaš, Ivan Moro, Ivan Olšina, Marián Peciar, Ján Perlac, Igor Prieložný, Stanislav Ščepán, Peter Sýkora, Štefan Vlčák

AK - nadstavbové štúdium so zameraním na programovanie a obsluhu počítacích strojov</br>
Triedna profesorka: Oľga Grmanová</br>
Daniela Bednáriková, Dezider Cseh, Etela Csehová, Ida Darabošová, Štefan Domonkos, Ľubica Drahošová, Gabriela Farkasová, Erika Fodorová, Ružena Floriánova, Miroslava Garajová, Ivan Halanda, Rudolf Hladík, Anna Horváthová, Katarína Hrušková, Oľga Janovičová, Karol Kállay, Štefan Kasza, Ivan Klíma, Margita Kobydová, Elena Kosibová, Ferdinand Kovács, Heribert Krafčík, Zuzana Lepulicová, Viliam Maňúch, Pavol Matis, Viliam Marcibányi, Helena Máziková, Anna Pogádyová, Ján Pap, Alena Reicherová, Maria Románová, Daniela Sklenková, Július Smutniak, Peter Šimečka, Eva Takáczová, Mária Valterová, Ružena Vargová, Dušan Vereš, Silvia Višňovcová, Vladimír Vorobjov