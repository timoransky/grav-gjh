---
title: 1992/1993
---

Školský rok 1992 - 1993

4.A</br>
Triedna profesorka: Eva Hanulová</br>
Lenka Bartoňová, Ivana Brnáková, Fedor Čiampor, Michaela Feriančíková, Ladislav Guller, Daniel Hassan, Richard Janč, Anna Korčeková, Martin Kovačič, Michal Krištúfek, Alexandra Kučerová, Dušan Lašák, Marián Lukačka, Katarína Mačáková, Mariola Martišková, Lenka Maťašová, Martin Molnár, Adriana Némová, Vladimíra Ondrušová, Katarína Parajková, Ondrej Petrík, Pavol Petrovič, Lucia Poláková, Rudolf Priečinský, Daniel Scheber, Karol Slanina, Ľubica Slovjaková, Barbora Sotníková, Marek Šimko, Matej Štrbák, Vladimír Tomašovič, Dušan Tomka, Katarína Vajteršicová, Karol Vavrovič, Petra Virágová, Ján Záhorec

4.B</br>
Triedna profesorka: Anna Jankovičová</br>
Silvia Baranová, Boris Bartl, Monika Bartošová, Andrej Belák, Ivan Bojna, Kamil Budinský, Jakub Čech, Peter Čunderlík, Martin Drozda, Roman Dvoran, Michal Hammel, Róbert Janošták, Andrea Juráková, Milan Kabát, Vlastimil Kátlovský, Silvia Kompoltová, Katarína Krištiaková, Vladimír Krno, Zuzana Meszárošová, Natália Muchová, Anna Nemcová, Soňa Nováková, Matej Ondrušek, Martin Pelikán, Pavel Petrovič, Eleonóra Potúčková, Jarmila Präsensová, Ivan Pružinec, Peter Ret, Zuzana Ružická, Marek Schwendt, Branislav Šťepánek, Janette Tkáčiková, Radoslav Volný, Rastislav Žirko

4.C</br>
Triedna profesorka: Elena Zelenayová</br>
Pavol Bacigál, Patrícia Bálintová, Martina Ballová, Gabriela Bartoscheková, Boris Baumgartner, Vladimír Bednárik, Ivan Behunek, Zuzana Cucorová, Jana Dobrovodská, Veronika Fajkusová, Zuzana Füleová, Marta Grňová, Eva Hanušová, Renáta Hinnerová, Rastislav Hlubocký, Aleš Horváth, Mário Hrapko, Marek Jankovič, Pavla Jelínková, Peter Kožka, Barbara Lesná, Karin Lexmannová, Anna Lunterová, Peter Majerský, Marek Mentel, Sylvia Millová, Igor Mišík, Slavomír Monte, Miroslav Ondrej, Marek Orgoň, Daniela Pavlišinová, Marek Poledna, Jaroslava Popelková, Peter Šiffalovič, Branislav Tkáčik, Stanislav Vanek, Martin Zrubec

4.D</br>
Triedna profesorka: Dana Laučeková</br>
Rami Abbas, Ľubomír Balog, Martin Beneš, Dávid Beran, Barbora Bořutová, Branislav Božek, Jana Condíková, Vladimír Dekan, Karol Depta, Jana Dočkalová, Alexander Donáth, Peter Dubravík, Martin Ducho, Zuzana Fiedlerová, Svetozár Gálik, Miroslav Jablonský, Mária Jamrišková, Martin Klamo, Alexandra Klčová, Jana Kocmundová, Jozef Krajčovič, Zuzana Kubasáková, Mária Kusá, Lucia Labovská, Eva Lásková, Katarína Luchavová, Renáta Matlovičová, Petra Ottisová, Tomáš Pašmík, Marián Pissko, Gabriela Polčicová, Zuzana Reuterová, Martina Roháľová, Karol Ruman, Helena Vagačová, Vladimír Vaník, Martin Verčík

4.E</br>
Triedny profesor: František Kosper</br>
Peter Banás, Peter Barica, Peter Bobro, Tomáš Bocán, Róbert Bojda, Zuzana Buzinkaiová, Milan Csaplár, Ľuboš Frolkovič, Petra Furbecková, Peter Grančič, Ivana Határová, Martin Hollý, Radoslav Holzmann, Marek Horvát, Matej Hulej, Katarína Chrenová, Daniela Juroleková, Andrej Kállay, Petra Kičová, Iveta Križalkovičová, Soňa Lacušková, Stanislav Laufik, Zora Lehotská, Martin Lukáč, Martin Marušinec, Silvia Mitterpachová, Michaela Molnárová, Zora Mončeková, Martina Ondrejková, Ivan Országh, Karol Ostrovský, Mária Pospíšilová, Dávid Smrtič, Miroslava Škodová, Lucia Šuchová, Petra Vavríková, Xénia Vicková, Jakub Wiedermann