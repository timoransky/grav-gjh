---
title: 'Ročenka - 50 rokov GJH'
process:
    markdown: true
    twig: false
cache_enable: true
visible: true
---

**Vitajte v ročenke GJH. Nájdete tu všetkých žiakov ktorí zmaturovali na GJH v rokoch 1959 - 2009.**