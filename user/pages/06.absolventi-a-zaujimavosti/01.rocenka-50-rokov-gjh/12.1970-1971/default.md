---
title: 1970/1971
---

Školský rok 1970 - 1971

3.A</br>
Triedna profesorka: Viera Hánová</br>
Pavol Fabianek, Dušan Godár, Tibor Hiank, Darina Holúbková, Július Hudec, František Chebeň, Zenóbia Jurzová, Karol Köppel, Ján Kosár, Marián Krčmár, Karol Kučera, Ján Kysel, Miloš Lazar, Beata Magyaričová, Mikuláš Meluš, Karol Nemoga, Eduárd Pračko, Pavol Pospíšil, Katarína Poláčková, Alžbeta Puškášová, Peter Romančík, Katarína Rokuzová, Ľudovít Širokay, Beata Šoltyová, Peter Štefula, Adriana Štroucerová, Gejza Tarič, Ľubomír Vyskoč, Anna Wendlová, Ján Zelenák

3.B</br>
Triedna profesorka: Zuzana Šimkovicová</br>
Eva Ambrošová, Mária Ambrošová, Anton Černý, Štefan Fecko, (Dušan Fogta), Jan Franců, Juraj Glos, Pavol Hanula, Štefan Janiga, Zuzana Kadlecová, Karol Kleinert, Oľga Koronthályová, Juraj Kostra, Karol Kotraba, Peter Kováčik, Lubor Kubica, Viera Kundráková, Mária Magurová, Eva Maťašová, Ladislav Mišík, Martin Mosný, Tibor Nanáši, Július Oravec, Peter Pál, Daniela Penerová, Ján Petrovič, Roman Prokop, Eugen Rehorovský, Stanislav Sýkora, Karol Šafařík, Marián Šesták, Vladimír Šimkovič, Zuzana Špániková, Peter Tamaškovič, Dušan Valachovič, Anton Vyskoč

3.C</br>
Triedna profesorka: Libuše Kostková</br>
Ivan Beseda, Hana Bubeničková, Milan Candrák, Edita Drotárová, Fedor Gömöry, Ján Grunt, Peter Halanda, Tamara Hamalová, Stanislav Hejátko, Danica Hýblová, Ján Chovan, Zuzana Jassingerová, František Jelenčiak, Stanislav Jokel, Jozef Jurkovič, Peter Kmeťo, Boris Kocúr, Štefan Kočí, Edmund Kozel, Jana Kozubová, Ľubomír Lašán, Mária Magdolenová, Anna Mäsiarova, Jana Novotná, Peter Obdržálek, Dana Obetková, Dana Páleníková, Jana Partlová, Ladislav Pavelka, Dimitrij Spišiak, Andrej Svatík, Jana Szombathová, Andrej Šuba, Jana Taldová, Ladislav Turecký, Margita Veličová, Konštantín Vika, Michaela Vírostková, Štefan Wimmer, Alžbeta Zvalová

3.D</br>
Triedna profesorka: Helena Kotzigová</br>
Ajša Bartoňová, Milan Beljak, Igor Blanárik, Rudolf Blažek, Darina Bródyová, Vlasta Cvečková, Viera Čechová, Katarína Domčeková, Jozef Gross, Miroslav Hajach, Mária Heldtová, Katarína Henkeľová, Andrej Holič, Jozef Hupka, Eduárd Jakubovie, Štefan Klokner, Ján Konôpka, Natália Krajčírovičová, Milan Kuchyňár, Ján Labuda, Ján Lidaj, Katarína Lidajová, Zdenek Malík, Vladimír Mihál, Pavel Milec, Peter Návrat, Helena Pašková, Zuzana Pelcnerová, František Ravinger, Eva Riganová, Anna Rosíková, Bibiána Sarnová, Jarmila Sládeková, Milan Synek, Vladimír Šikura, Drahomír Šišovič, Libuše Šulcová, Andrea Vozárová, Marián Zervan, Jozef Znášik

3.E</br>
Triedna profesorka: Edita Borutová</br>
Beáta Bezáková, Martin Brezina, Michal Čeppan, Marián Ďurica, Mária Džongová, Anna Flašková, Milan Gargulák, Eva Gedeiová, Jozef Gonda, Lýdia Hanicová, Ľubomír Hermann, Michal Hirjak, Eva Hrušovská, Ján Jamriška, Helena Jurštíková, Milan Karovič, Anna Kellová, Mária Klenovičová, Emília Klubalová, Viera Kmečová, Daniel Kordík, František Krchnák, Eva Kubeková, Ján Kubiš, Gustáv Mosendz, Vojtech Kúkoľ, Ivan Mrlian, Mária Mutňanská, Juraj Németh, Zuzana Némethyová, Štefan Neuschl, Štefan Ondríšek, Vladimír Pitrun, Jana Plešková, Pavol Rác, Vladimír Suchomer, Ján Špaček, Ladislav Špaček, Pavol Štefánik, Alena Valentová, Vojtech Winter, Karol Zelenka

3.F</br>
Triedna profesorka: Elena Šimkovičová</br>
Alena Babulicová, Biljana Bartošová, Mária Bratová, Štefan Bočkay, Dana Fábryová, Márgita Farkašová, Eva Fraňová, Alena Fratričová, Jana Gábelová, Soňa Grundajová, Soňa Hagarová, Jana Hajná, Ján Havlát, Dana Hrabárová, Jaromír Chorváth, František Jászberényi, Dana Illeková, Katarína Klačanská, Viera Korecká, Tamara Krnáčová, Jana Klenovičová, Mária Kropiláková, Eva Kupkovičová, Pavol Lešták, Katarína Lukáčová, Darina Lukačovičová, Iveta Magulová, Nadežda Mayerová, Terézia Michalíková, Anna Mitrová, Zuzana Nerádová, Ľudmila Pažická, Valéria Poláková, Katarína Popaďáková, Mária Rosenbachová, Soňa Rusnáková, Jana Schneiderová, Oľga Schwarzbacherová, Darina Skotnická, Karol Školár, Elena Šubíková, Viera Uhrová

3.G</br>
Triedna profesorka: Anna Gavorová</br>
Eva Adamkovičová, Ján Baláž, Ivona Baranová, Mária Böhmová, Eva Brádňanová, Martin Brucháč, Ľubomír Fifik, Pavol Horník, Jozef Chudík, Viera Churová, Ivan Illáš, Mária Ivanova, Tatiana Kalinová, Zoltán Kálay, Nadežda Kmiťová, Eva Kočišová, Miroslav Krajňák, Jela Kresáková, Nataša Maniačková, Danka Mihálková, Monika Oravcová, Ladislav Pastucha, Klement Pravotiak, Dagmar Príkazska, Marta Puškášová, Zina Reháková, Dorota Szantová, Elena Šimková, Vladimír Tichý, Alena Trepáčová, Imelda Véghová, Irena Viktorínová, Jaroslava Zábojníková

AK - nadstavbové štúdium so zameraním na programovanie a obsluhu počítacích strojov</br>
Triedna profesorka: Alica Zadubanová</br>
Viera Babeková, Mária Bednárová, Rudolf Boczek, Marián Braunecker, Miroslav Brestovanský, Eva Bučeková, Katarína Debrecká, Ľudovít Estelyi, Mária Fialová, Mária Fönödová, Mária Havlíková, Emília Horňáková, Edita Horváthová, Vladimír Kolenič, Helena Komorníková, Viera Koppová, Božena Krupová, Jolana Lovecká, František Mikloš, Rita Mokrá, Eva Navrkalová, Jana Nemečkayová, Viera Neufeldová, Zuzana Ondráčková, Alžbeta Opečková, Peter Palušiak, Eva Peterská, Jana Polomská , Pavel Prossek, Mária Rybárová, Mária Rybecká, Želmíra Surová, Mária Šipošová, Pavel Škoda, Ján Štekláč, Alena Štibrániová, Júlia Tomanová, Pavel Jurský, Mária Vlčková, Ján Zorkóczy