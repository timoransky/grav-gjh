---
title: 1989/1990
---

Školský rok 1989 - 1990

4.A</br>
Triedna profesorka: Anna Akácsová</br>
Alena Bányaiová, Katarína Bendová, Zuzana Burgerová, Eva Čambalová, Martin Červenák, Miroslav Čevela, Juraj Čunderlík, Andrea Ďuriačová, Róbert Gabányi, Juraj Chalmovský, Marek Janič, Eva Kalivodová, Andrea Kuhajdová, Tibor Lappy, Roman Lauček, Erika Lindaeurová, Miriam Lukoťková, Peter Melich, Sylvia Pastorková, Juraj Redeky, Martina Rubešová, Daniela Selčanová, Martina Šárovecká, Branko Šefránek, Július Šubík, Svorad Trnovec, Miroslav Trnovský, Martin Valach, Bohdan Valentovič

4.B</br>
Triedna profesorka: Zuzana Lukačková</br>
Peter Baráth, Karin Boryová, Tomáš Ciran, Martin Dindoš, Branislav Ďurajka, Vladimír Ďuračka, Barbora Fajkusová, Zuzana Ferkova, Veronika Hanulová, Zuzana Husárová, Gabriela Janglová, Juraj Jankovič, Alexandra Kirthová, Dávid Koronthály, Róbert Králik, Martin Marko, Juraj Marušic, Ľuboš Obernauer, Monika Obrancová, Jana Paligová, Milan Patrovič, Juraj Pivarč, Ján Repiský, Rudolf Sedmina, Jana Solotruková, Jana Šárová, Milan Šišák, Gabriela Takácsová, Martin Vozár, Zuzana Zmajkovičová, Lucia Žalmanová, Renáta Žirošová

4.C</br>
Triedny profesor: Igor Vojtela</br>
Martin Benovič, Daniel Buchta, Miriam Čamajová, Marek Čerňanský, Dana Čupková, Martin Floch, Matej Gedeon, Hana Hančoková, Patrik Hasaj, Janette Havelská, Martin Chynoradský, Peter Kapusta, Eva Kohútová, Miroslava Krčmárová, Alexandra Kyšková, Mária Lenczová, Gabriela Palková, Matúš Paulíny, Rudolf Pelikán, Daniela Pelikánová, Martin Polakovič, Ivana Poršeková, Katarína Pospíšilová, Veronika Pristašová, Monika Sokolová, Andrej Stolárik, Stanislava Tancerová, Alena Tučková, Marek Vladár, Marek Zelman

4.D</br>
Triedny profesor: Jozef Varga</br>
Miroslava Bilačičová, Rastislav Božek, Juraj Brabec, Denis Dobiš, Martin Granec, Mária Harichová, Dagmar Holá, Zuzana Hudecová, Branislav Kalužný, Daniela Kerestešová, Michaela Košťálová, Tomáš Kováč, Adrián Kubík, Marián Lipták, Martina Machalová, Daniela Ostrovská, Zuzana Poláčiková, Pavol Polák, Zuzana Repaská, Alexander Schill, Alan Sitár, Ingrid Stredlová, Hana Svobodová, Karol Šimunič, Patrik Šolc, Andrea Štefankovičová, Pavel Vágner, Jana Vorlíčková, Ida Želinská