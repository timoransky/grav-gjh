---
title: 1986/1987
---

Školský rok 1986 - 1987

4.A</br>
Triedna profesorka: Elena Zelenayová</br>
Július Alexy, Dušan Blaško, Andrea Dicsöová, Peter Drgon, Dana Gmitrová, Lucia Gogelová, Michal Grajcar, Peter Haluška, Silvia Horváthová, Ľubomíra Hradiská, Marek Jakubík, Sylvia Kalvodová, Iveta Koreňová, Jozef Mikloško, Daniela Mikuličová, Dominika Navarová, Zuzana Nemčeková, Jana Pavelková, Zuzana Pissková, Pavol Povinec, Peter Púček, Nora Szendreyová, Ľubomír Šikula, Eva Trtíková, Svätopluk Vágner, Stanislav Vencel, Ludvík Vybíral, Marcel Zajac, Lýdia Zvolencová

4.B</br>
Triedna profesorka: Oľga Chovanová</br>
Richard Baumgartner, Radoslav Böhm, Richard Borovanský, Xénia Budinská, Anna Číčelová, Pavol Frešo, Ľubica Hudecová, Richard Hunák, Gabriela Kacvinská, Jana Koncová, Miroslav Kotek, Alexander Kováč, Adriana Kováčiková, Andrej Kráľ, Martin Krššák, Ján Kupecký, Martin Lamačka, Martin Lipták, Peter Majerník, Andrea Pavlovská, Katarína Pišútová, Viera Pivarčová, Alžbeta Poláková, Marcel Rebro, Richard Schultz, Marián Szabó, Ján Šimo, Matúš Škvarka, Veronika Tarábková, Roman Valo, Vladimír Veselý, Danica Zongorová

4.C</br>
Triedna profesorka: Mária Ondriašová</br>
Erik Bittner, Patrik Blecharž, Róbert Eckert, Slávka Garajová, Andrea Hanuliaková, Marián Hanzalík, Karin Havelková, Ján Hollý, Peter Ivanusyk, Zuzana Jánošíková, Nora Kačániová, Janette Kakašová, Katarína Királyová, Peter Kortus, Martin Krakovský, Peter Mačinga, Marián Mališ, Dáša Maronová, Denisa Matušíková, Lenka Porubská, Dušan Prívozník, Jaroslav Sadloň, Marián Spišiak, Terézia Šajgalíková, Marián Šandor, Luciana Švecová, Roman Talaš, Ľubomír Tancer, Leonard Topľanský, Norbert Vaňo, Jaroslav Vávra, Iveta Zbončáková

4.D</br>
Triedna profesorka: Dana Laučeková</br>
Peter Brieda, Peter Bubelíny, Vladimír Čambál, Mária Farbová, Lucia Franková, Monika Gavorová, Karol Grohman, Anton Halas, Katarína Hricíková, Michal Hroch, Júlia Hučková, Mojmír Jankovič, Peter Kadubec, Martin Kalináč, Daniel Klimo, Hana Kovačičová, Martin Kráľ, Peter Krehák, Jana Kudláčová, Daniela Levická, Helena Lojschová, Ľubomíra Mikolášová, Zuzana Mináriková, Monika Mitášová, Xénia Oláhová, Martin Pospíšil, Peter Strunga, Marián Šauša, Miloslav Švajdlenka, Roman Veber, Pavel Vodák, Veronika Zámečníková