---
title: '1969 /1970'
---

Školský rok 1969 - 1970

3.A</br>
Triedny profesor: Jozef Balala</br>
Valéria Bošanská, Kristína Čelková, Jaroslava Črepová, Vladimír Čunderlík, Marián Dobrík, Zuzana Feketeová, Lýdia Griešová, Štefan Horárik, Marek Hôrčík, Albín Hrušovský, Roman Inovecký, Pavel Kacvinský, Alexander Kirschner, Igor Kolek, Danica Kovačovská, Emília Kubrická, Tamara de Lemény - Makedonová, Dušan Malík, Zuzana Matulová, Milan Medek, Vladimíra Pašková, Jarmila Peštuková, Jana Polomská, Pavla Sedláčková, Peter Sedlák, Peter Steiner, Ivan Škultéty, Mária Škvareninová, Gustáv Thomas, Igor Trepáč, Róbert Truben, Ivan Trúsik, Juraj Velič, Mária Vrablicová, Ondrej Zoltán

3.B</br>
Triedny profesor: Július Šoltés</br>
Kamil Bárta, Róbert Beňačka, Miroslav Bořuta, Pavol Černek, Vladimír Černý, Peter Fordinál, Filip Guldan, Marián Hargaš, Pavol Haulík, Vladimír Heržo, Zoltán Ivanička, Juraj Jarošek, Daniela Kabinová, Slavomír Kontriš, Mária Kosseyová, Eva Kroupová, Angela Leitmannová, Oľga Martinčeková, Pavol Matejko, Nadežda Mináriková, Štefan Oravec, Adriana Pospiechová, Peter Prónay, Juraj Šafařík, Ivan Šebök, Elena Šeböková, Juraj Šurka, Juraj Tekuš, Ivan Tomašovič, Jaromír Uher, Eduard Vyskoč, Michal Zajac, Daniel Žuffa

3.C</br>
Triedna profesorka: Tatiana Lorincová</br>
Roman Ančina, Monika Badáková, Marián Bátovský, Alžbeta Biskupičová, Darina Čierna, Ivan Hargaš, Juraj Holič, Viera Chudáčková, Ján Ižo, Jana Javorková, Pavol Kahay, Pavol Kaiser, Juraj Kayser, Ladislav Kostolný, Bohumil Kováč, Miroslav Kováč, Milan Križan, Dušan Livář, Pavol Malovič, Teodór Mittermayer, Vladimír Pastirčák, Jana Petrášková, František Poráznik, Ladislav Prekop, Jana Prelovská, Pavol Prikryl, Vladimír Proksa, Juraj Rakovský, Juraj Richter, Eva Scholzová, Elena Strašíková, Ivan Ševčík, Jana Šulcová, Mikuláš Tekeľ, Alena Vágnerová, Bohumil Vida, Peter Vodrážka, Peter Weiss, Katarína Žirková

3.D</br>
Triedna profesorka: Viera Vavrová</br>
František Alena, Rastislav Bakoš, Jaroslava Bächerová, Elena Belišová, Stanislav Bilačič, Peter Bobro, Vladimír Celko, Daniel Dick, Karol Dobrovodský, Eduard Duda, Nora Dubrovayová, Dagmar Hoďovská, Uvia Hofferková, Marta Horáková, Jana Janíková, Marián Ježek, Vladimír Jorík, Gabriela Kalavská, Miroslav Kavický, Ivan Kepko, Jana Koritsanszká, Ivan Kostolný, František Krchnák, Vladimír Kvassay, Štefan Lečko, Štefan Mráz, Zuzana Májovská, Pavol Návrat, Vladimír Nemec, Ivan Petrůj, Mária Pichlerová, Stanislav Pilárik, Milan Profant, Vincent Sádovský, Dušan Srnec, Ľubomír Vlčák, Eleonóra Winklerová, Pavol Závadský

3.E</br>
Triedna profesorka: Oľga Völgyiová</br>
Dušan Adam, Rudolf Botek, Pavol Böhm, Rudolf Brenner, Pavel Filípek, Darina Fischerová, Mária Gajdošová, Rastislav Glézl, Dana Hajachová, František Holler, Samuel Janec, Jana Kádenová, Peter Kern, Eva Klimáčková, Viera Kmetyová, Milan Kostka, Juraj Lipka, Milan Martinovič, Dušan Mitterpach, Vladimír Páleník, Igor Papp, Katarína Pavlíková, Vlasta Pavlíková, Juraj Pechan, Peter Pritz, Juraj Prokeš, Ľubomír Sapák, Dušan Slezák, Roman Staník, Ján Svetlík, Peter Šimeček, Michaella Šipková, Marián Šolty, Daniela Šlauková, Ivan Štefánik, Ružena Vagovičová, Imrich Zalupský, Márius Žitňanský, Jozef Žucha

3.F</br>
Triedna profesorka: Anna Akácsová</br>
Renáta Beljaková, Peter Borovský, Ladislav Brat, Juraj Braun, Eva Bučeková, Alžbeta Čelesová, Mária Horváthová, Zuzana Horváthová, Mária Jadrná, Terézia Jakubcová, Daniela Jelínková, Dušan Kasanický, Alena Knotková, Duňa Kollárová, Ladislav Konrád, Lucia Kopaničáková, Eva Kovalčíková, Valéria Krásna, Alexandra Krnáčová, Lýdia Kupkovičová, Alena Marková, Klára Marková, Terézia Melková, Ľubomír Miček, Michailo Miškovič, Darina Nikolajová, Ivana Nesselmanová, Alžbeta Nováková, Helena Pagáčová, Katarína Pindúrová, Viera Rúriková, Vladimír Stanislav, Alena Strelková, Alžbeta Stankovičová, Viera Suchomerová, Elena Šimkovicová, Božena Takáčová, Anna Tóthová, Izabella Virsíková, Kartarína Zemanová

3.G</br>
Triedny profesor: Imrich Nemec</br>
Danka Aujeská, Peter Babál, Ľubica Bakalárová, Soňa Bächerová, Katarína Birnsteinová, Mária Bogárová, Darina Danišová, Nina Dovinová, Ján Flaška, Tatiana Friedová, Daniela Hnilická, Mária Hohošová, Marta Hrebíčková, Pavol Klimo, Vladimír Kmec, Jana Kosibová, Nadežda Kostrecová, Zlatica Kováčová, Zuzana Krónerová, Tibor Kružliak, Ivan Letaši, Helena Májeková, Jana Martišová, Dušan Pavlovič, Anna Pišútová, Branislav Potančok, Marta Rácová, Katarína Rizmanová, Klára Rotbauerová, Peter Slivka, Adriana Sýkorová, Stanislav Vrba, Katarína Wendlová, Katarína Zabudlá

AK - nadstavbové štúdium so zameraním na programovanie a obsluhu počítacích strojov</br> 
Triedny profesor: Zdeno Vlachynský</br>
Viliam Aufricht, Alžbeta Babeková, Eva Beláková, Mária Bošková, Anna Cabadajová, Jarmila Čechvalová, Klaudia Halová, Božena Kromerová, Magda Krúpová, Zuzana Kutruczová, Jozef Leczkési, Miroslava Marenčíková, Oľga Miklašová, Gabriela Ňémethová, Monika Ogilovičová, Vladimír Ondrejkovič, Anna Petríková, Ľudmila Porubská, Ján Repa, Renáta Rovanová, Zora Sitárová, Ľudmila Slimáková, Vlasta Tomášiková, Viera Valaštiaková, Božena Vasilenková, Marián Puškár