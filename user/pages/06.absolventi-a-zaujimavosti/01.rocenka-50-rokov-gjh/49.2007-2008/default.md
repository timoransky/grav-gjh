---
title: 2007/2008
---

Školský rok 2007 – 2008

4. B</br>
Triedna profesorka: Mária Križanová</br>
Peter Brichta, Michal Čajko, Andrej Dittrich, Michal Ďorďa, Peter Hlavatý, Alena
Humajová, Andrej Chovan, Jarmila Jančigová, Michaela Janošková, Tomáš Jarábek,
Jakub Karovič, Daniel Kis, Peter Kontsek, Anna Macháčová, Miroslav Martinovič,
Milan Mészáros, Štefan Mikuš, Dominika Niňajová, Michaela Pereiová, Juraj Petro,
Natália Prelecová, Lukáš Priecel, Peter Rogoţník, Michal Sabo, František Schiffer, Filip
Schwarz, Martina Stankovičová, Dominika Stráňavová, Andrej Svitek, Simona
Šimková, Daniela Trnovcová, Martin Truben, Dana Zálešáková

4. C</br>
Triedna profesorka: Beata Pecková</br>
Zuzana Bencová, Miroslav Bimbo, Pavol Blaho, Eva Brhlíková, Kristína Brhlíková,
Martina Čaplovičová, Martin Čavoj, Eva Danielová, Nina Duchoňová, Veronika
Ďuricová, Marek Fekete, Nikita Fesenko, Marián Gálik, Adam Hudec, Patrik Janáč,
Peter Kopačka, Martin Košdy, Michal Kovaľ, Štefan Lesňák, Tomáš Matula, Andrej
Paška, Branislav Petrovič, Adam Poldauf, Ronald Ruţička, Adam Saleh, Zuzana
Soldánová, Martina Staruchová, Peter Stovíček, Ľubica Strelcová, Martin Sucha, Boris
Svoboda, Alexander Tóth, Ivan Trančík, Marek Vnuk

Oktáva</br>
Triedny profesor: Samuel Vagaský</br>
Michal Boška, Pavol Ďurina, Rudolf Goga, Jonáš Gruska, Oliver Hamid, Nikola Jajcay,
Samuel Kalugerov, Ondrej Kedrovič, Matej Kohút, Peter Kostolányi, Ľuboš Kuboň,
Andrea Kubovčíková, Marko Kubrický, Igor Kvasnička, Michal Makúch, Miriam
Malíčková, Dominika Mayerová, Gabriela Michaličková, Kristián András Nagy, Marek
Patarák, Matej Milko, Otto Pilák, Jozef Semjan, Veronika Suchobová, Peter Šimončič,
Karol Šutý, Tomáš Tatara, Igor Vilček, Jakub Vojtko, Barbora Volentičová

4. IB A</br>
Triedna profesorka: Alena Polakovičová</br>
Martina Antošová, Tamara Augustínyová, Oľga Babošová, Miloš Bella, Marta
Dravecká, Miriama Fáberová, Zuzana Gavorová, Martina Hoľuková, Veronika
Janečková, Matej Karaba, Daniel Komadel, Tamara Košíková, Michal Kováč, Paulína
Kožuchová, Michaela Lesayová, Diana Odrobináková, Veronika Ostrihoňová, Janka
PetŊczová, Lukáš Platinský, Zuzana Rendlová, Michal Rybár, Marek Slobodník, Peter
Synak, Miroslav Tomášik, Eva Vaculíková, Soňa Zemanová