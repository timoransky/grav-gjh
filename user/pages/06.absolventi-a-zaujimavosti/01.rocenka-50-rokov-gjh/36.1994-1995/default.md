---
title: 1994/1995
---

Školský rok 1994-1995

4.A</br>
Triedny profesor: Marián Slušný</br>
Štefan Báthory, Peter Bulák, Daniel Buzinkay, Martin Čaprnda, Peter Čunderlík, René Ďurík, Gabriel Gajdoš, Bystrík Gallo, Katarína Gallová, Zuzana Hautová, Martin Hronec, Vladimír Hudek, Anton Jevčák, Juraj Králik, Peter Kresánek, Matej Kubičár, Boris Lilov, Peter Macák, Richard Magyar, Peter Marko, Anna Matulová, Marek Máťuš, Zora Mistríková, Lukáš Mocko, Peter Novák, Peter Ondrej, Matúš Petrík, Lenka Podlucká, Matej Poliak, Andrej Probst, Tatiana Prónayová, Braňo Rusnák, Tomáš Slanina, Tomáš Szarka, Ján Ťapuška, Vladimír Vittgrúber

4.B</br>
Triedna profesorka: Elena Kalašová</br>
Štefan Artner, Tomáš Blaško, Andrej Fabianek, Roman Filkorn, Peter Galko, Barbora Gandlová, Eva Guldanová, Jaroslav Hanúsek, Pavel Haulík, Peter Homola, Zuzana Ježovičová, Viliam Klimo, Martin Koleda, Ivan Kovár, Branislav Kátlovský, Peter Lalík, Martin Makúch, Michal Marendiak, Martin Markovič, Peter Maťaš, Richard Mátéffy, Michal Ondrušek, Martin Pál, Peter Palušák, Peter Paštrnák, Katarína Pirchanová, Rudolf Poruban, Miroslav Ravas, Michal Stanislav, Radoslav Tausinger, Dana Tomanóczyová, Katarína Vlasková, Martin Záhora, Peter Žilinek, Daniela Žitňanská

4.C</br>
Triedny profesor: Ján Mayer</br>
Stanislav Antalic, Peter Baran, Martin Bernard, Petra Borsíková, Michal Cagalinec, Ľubomír Čulen, Lenka Denková, Viktor Dubec, Eva Eližerová, Martin Gáplovský, Juraj Horváth, Michal Hrabovec, Zuzana Hvožďarová, Fedor Jaško, Marián Kaššovic, Gabriela Kopálková, Samuel Kytka, Michal Muravský, Vladimír Novotný, Andrej Ondriaš, Martin Palkovič, Martin Palla, Ján Pavličko, Štefan Polhorský, Michal Praženka, Miroslav Pšenko, Gabriela Schieberová, Tomáš Slezák, Peter Stankoviansky, Pavol Strapoň, Jozef Šoltés, Jana Štorová, Róbert Tomiš, Ivan Troščák, Zuzana Trubíniová, Martin Vadovič, Peter Valach

4.D</br>
Triedna profesorka: Ľubomíra Kovalčíková</br>
Mária Bartkovjaková, Peter Bíro, Marek Bleho, Peter Borbély, Dávid Bořuta, Mária Číčelová, Boris Dekan, Rastislav Hatala, Marián Havlíček, Andrea Heldová, Rudolf Jahelka, Alexandra Kubíková, Ivana Kuníková, Marián Lalik, Martin Matula, Miroslav Milán, Michal Molnár, Michal Moravec, Olívia Némethová, Zuzana Olšová, Katarína Paligová, Igor Petrík, Peter Pivarči, Richard Pospíšil, Filip Puchert, Juraj Rehák, Peter Sabo, Andrej Slávik, Radoslav Slovák, Michal Staňo, Peter Svoboda, Martin Ťupek, Samuel Vališ, Ján Vlach, Nadežda Zadžorová, Martin Závodský, Svetozár Žarnovický