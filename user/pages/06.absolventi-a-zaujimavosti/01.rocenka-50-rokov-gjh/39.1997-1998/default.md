---
title: 1997/1998
---

Školský rok 1997 - 1998

4.A</br>
Triedna učiteľka: Renata Monošíková, CSc.</br>
Peter Bažány, Branislav Brachtl, Radoslav Dobiáš, Zuzana Ferenčíková, Katarína Gajdošová, Michal Gramblička, Martin Hrnko, Nataša Húsková, Lucia Chudíková, Marek Jakubec, Ondrej Javorka, Jana Kadlecová, Martin Kostolanský, Róbert Margetin, Michal Marko, Lukáš Medlen, Jana Miklušová, Matúš Mitana, Michal Molnár, Martin Mýtny, Mário Oleš, Martin Ondrejíček, Jana Onufráková, Veronika Pavolová, Marek Piváček, Miroslav Procházka, Juraj Roščák, Ján Rusnák, Katarína Sedláková, Tatiana Sládečková, Tomáš Šedivý, Peter Šimonovič, Ján Tóth, Michal Učník, Tomáš Žáček

4.B</br>
Triedny učiteľ: Ondrej Demáček, (Ivana Pichaničová)</br>
Zuzana Boďová, Marek Cielontko, Juraj Fiľak, Juraj Földes, Jana Fraasová, Aleš Guldan, Katarína Holečková, Michal Hron, (Jan Jamrich), Tomáš Janvars, Michal Krajčovič, Peter Lacko, Ľubica Laššáková, Lenka Litváková, Jana Marčáková, Terézia Mišíková, Ján Ožváth, Roman Pauer, Martin Pěkov, Pavol Perďoch, Katarína Poláková, Vojtech Potočný, Matej Pukančík, Štefan Slezák, Vladimíra Staňová, Jana Svetlíková, Martin Šmigura, Vladimír Tužinský, Štefan Urbánek, Tomáš Záhradník

4.C</br>
Triedna učiteľka: Silvia Mészárošová</br>
Jozef Balko, Juraj Bartosiewicz, Vanda Benkovičová, Katarína Brežná, Michal Dedinský, Gabriel Domšitz, Daniela Ferenčíková, Peter Fülöp, Daniel Gerek, Matej Gyárfáš, Roman Hajach, Natália Hattalová, Peter Horňák, Bronislava Hudcovská, Tomáš Chvostek, Marcela Kabeláčová, Tomáš Kaličiak, Judita Kobzová, Kristína Kovácsová, Miroslav Krajčír, Lucia Kučová, Barbara Lisá, Juraj Mosendz, Katarína Olšová, Peter Ondráška, Ján Petrjánoš, Lukáš Rúčka, Martin Šimlaštík, Róbert Štefanec, Ľudovít Vachálek, Martin Váross, Soňa Vilímová, Filip Záhradník

4.D</br>
Triedna učiteľka: Daniela Ďurajková</br>
Anton Bracjun, Michaela Csukásová, Martina Červenková, Michal Dekánek, Jana Forróová , Ján Fratrič, Juraj Hrbatý, Dávid Jablonovský, Marián Jančár, Ján Kriška, Joachim Kučera, Ondrej Kútik, Zdenka Lajová, Martina Machová, Silvia Miklošovičová, Michal Milko, Marián Nikš, Otmar Olšina, Alexandra Prejsová, Michal Ries, Vladisláv Řikovský, Ján Seydov, Marek Skalák, Daniela Sklenárová, Eva Šareková, Samuel Štuller, Martin Švík, Jana Tomovičová, Zdenek Tubel, Martin Turi Nagy, Peter Višváder, Tomáš Zeman

4.IB</br>
Triedna učiteľka: Alena Polakovičová</br>
Martin Aksamit, Pavol Černý, Vít Fargaš, Mária Fehérová, Jana Hanulová, Miloš Hašan, Silvia Kovaríková, Zuzana Labašová, Ľubomír Lanátor, Hana Lukačková, Vladimír Magula, Milan Majerník, Daniel Mašek, Juraj Mlynár, Zuzana Nieplová, Miroslava Ondrášová, Linda Růžičková, Tomáš Šandor, Daniela Uličná, Igor Urbančík, Slávka Valachovičová, Dušan Vavrek, Zuzana Zajacová, Gabriela Selecká