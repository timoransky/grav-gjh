---
title: 1987/1988
---

Školský rok 1987 - 1988

4.A</br>
Triedny profesor: Ľubomír Krištof</br>
Ľubica Baráthová, Marek Belluš, Katarína Beňová, Dana Dorotková, Peter Fehér, Róbert Földeš, Romana Hambálková, Magda Hlaváčová, Jana Hrdličková, Zuzana Jusková, Vladimír Juščák, Zuzana Klimková, Branislav Kostka, Dana Kováčiková, Andrea Kurtišová, Dana Leščenková, Katarína Liptáková, Emanuel Lörinc, Beáta Mareková, Miroslava Mináriková, Juraj Rebro, Pavol Slameň, Peter Slameň, Miroslav Smida, Allan Sobihard, Martin Solotruk, Daniel Strasser, Štefan Šebesta, Martin Šimůnek, Soňa Škodová, Jana Šteffelová, Boris Tichý, Marek Turňa

4.B</br>
Triedna profesorka: Anna Dorčáková</br>
Mária Barnášová, Hana Bócová, Vladimíra Bukerová, Jozef Dobias, Katarína Erentová, Michal Fabrici, Adriana Horínková, Richard Illáš, Katarína Koladová, Paulína Koršňáková, Martin Krištofič, Martin Kurdel, Tatiana Lexová, Ivana Lukáčová, Peter Lukšic, Eva Medveďová, Peter Mitura, Jana Muchová, Klaudia Nižňanská, Martin Olejček, Juraj Oravský, Roman Pekárek, Peter Povinec, Eva Rácová, Peter Ripka, Miriam Rukovanská, Zuzana Ružeková, Richard Sirota, Branislav Stríženec, Marián Šalát, Martina Šepáková, Miroslav Šrol, Peter Uhrík, Marián Václavík, Martin Veľas, Oľga Vollárová, Jana Zbuňáková

4.C</br>
Triedna profesorka: Ľubomíra Kovalčíková</br></br>
Pavol Bobula, Peter Bocán, Juraj Cecko, Radoslav Derka, Henrich Eisner, Marián Flassik, Radko Gecik, Zuzana Heliová, Denisa Hodermarská, Natália Košvancová, Adriana Kráľovičová, Marcela Majnová, Martina Mistríková, Naďa Mitanová, Andrea Mihaliková, Peter Parkáni, Darina Pauliková, Mária Pobehová, Rastislav Rázus, Katarína Rejtová, Zuzana Ružičková, Milan Schwarz, Tomislav Stena, Peter Šimon, Juraj Škvarka, Stanislav Urbanovič, Zuzana Vavríková, Lucia Zelenáková

4.D</br>
Triedny profesor: Zuzana Fraasová, František Kosper</br>
Martina Beránková, Andrea Braunová, Marek Čambal, Michal Ďurkovič, Peter Erdelský, Dana Fabulová, Michal Hatrák, Radovan Horváth, Peter Hoťka, Martina Hybká, Silvia Jankovičová, Michal Kadlec, Pavel Kollár, Miloslav Korienek, Anna Kostrová, Michal Kuklica, Juraj Majer, Patrik Mozola, Marta Nevřelová, Adriana Ondrušová, Miroslav Orlovský, Marek Pinter, Natália Schillová, Jana Smidová, Dávid Sulík, Katarína Szalayová, Eduard Szittay, Vladimír Tomeček, Róbert Tonyka, Richard Voda, Andrea Zacharová