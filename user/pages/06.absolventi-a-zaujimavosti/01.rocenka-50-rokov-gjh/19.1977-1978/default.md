---
title: 1977/1978
---

Školský rok 1977 - 1978

4.A</br>
Triedny profesor: Jozef Balala</br>
Elena Bosáková, Veronika Brezovská, Igor Buc, Ľubica Daňková, Juraj Celler, Ľubica Greisingerová, Ladislav Hrotko, Ľubica Chmeľová, Peter Kačír, Jana Hačundová, Jana Kotúčová, Miloš Koudela, Peter Križanský, Soňa Makúchová, Ivana Mihalovičová, Miroslav Mikulecký, Elena Pašková, Ivan Polko, Zuzana Pukalovičová, Martin Režucha, Milan Schmidt, Viera Slávikova, Anna Stehlíkova, Miloš Staňo, Elena Šafařová, Vladimír Šatura, Zdena Šovanová, Ivica Takáčová, Darina Tencerová, Alena Vrbová, Eva Zachardová, Miroslav Žiak

4.B</br>
Triedny profesor: Július Šoltés</br>
Rudolf Bárta, Štefan Bendžala, Jana Bíliková, Ľubomír Gabriel, Marta Holečková, Vladislav Chaloupka, Igor Ilavský, Peter Kosák, Viera Kosseyová, Zuzana Kubálová, Kamil Kyselica, Juraj Laco, Ján Lakota, Ivan Mamrilla, Milan Mišík, Ľuboš Mitáš, Ivan Mizera, Róbert Oberländer, Patrik Ostrožlík, Agáta Pešková, Magdaléna Petrášková, Renáta Repková, Igor Stepanov, Fedor Šimkovic, Mojmír Šťasný, Alena Tallová, Jana Tresová, Darina Tuchscherová, Igor Travěnec, Štefan Varga, Juraj Vavra, Peter Velič, Ján Zelman, Jaroslav Žember

4.C</br>
Triedna profesorka: Emília Hrnčiríková</br>
Ľubomír Bartošovič, Soňa Drobná, Miroslav Gajdošík, Peter Gomolčák, Ida Hrušovská, Stanislav Kacvinský, Ľubica Kapustová, Miloš Kedrovič, Eva Kreháková, Marián Krempaský, Martin Kubánka, Peter Kubaš, Vojtech Kubek, Ján Lác, Ľibuša Lukšanová, Juraj Macko, Alan Marko, Branislav Maťátko, Darina Matrková, Darina Némethová, Jozef Oravec, Jana Poliaková, Jozef Potisk, Ján Prokop, Pavol Skalák, Miloš Škoda, Ivan Štich, René Vanek, Roman Vavrík

4.D</br>
Triedna profesorka: Zora Pašková</br>
Marián Baján, Karol Bartoš, Ľubomír Bátora, Martin Buza, Karol Everling, Peter Fekete, Miroslav Fekete, Marek Fila, Alica Gajarská, Tibor Godány, Ján Greguš, Martin Hlásnik, Karol Hudec, Michal Hudec, Elena Hykischová, Klára Janská, Karol Kandráč, Miloš Konček, Jozef Kostelničák, Pavol Králik, Vladimír Krátky, Michal Májek, Pavol Minárik, Zuzana Mlčuchová, Peter Nevický, Beata Panáková, Angelika Poláčková, Zuzana Ráhlová, Viola Slováková, Beata Srnová, Juraj Sýkora, Dagmar Trandžíková, Katarína Vagáčová, Alena Valová, Viera Valovičová, Oľga Vančová

4.E</br>
Triedna profesorka: Blažena Rovanová</br>
Juraj Babál, Alena Bačíková, Katarína Búciová, František Deák, Šimona Dorkovičová, Anton Dvořák, Roman Filák, Elena Hájková, Lucia Ivicová, Ivan Jakubis, Lida Kalamárová, Karol Karolus, Milan Kopčok, Ladislav Košecký, Darina Kovalčíková, Ľubica Lacinová, Pavol Mihaliček, Milan Mondok, Eva Nedeľková, Jana Nemčoková, Vladimír Nikulin, Peter Pastierik, Mária Poláková, Viliam Schwarz, Iveta Srnánková, Roman Struhár, Alena Šlapanská, Vladimír Šuchma, Pavol Šutý, Kamila Tomková, Ivan Trnečka, Jana Vermanová, Ivan Žilipský

AK - nadstavbové štúdium so zameraním na programovanie a obsluhu počítacích strojov</br>
Triedna profesorka: Oľga Grmanová</br>
Jana Baloghová, Rudolf Belička, Anna Dušková, Emília Federičová, Gabriela Gučiková, Jozef Hasák, Edita Hasalová, Petronela Chupáčová, Daniela Januríková, Jana Jurovčaková, Veronika Karpinská, Mária Kožuchová, Elena Latáková, Eva Lelovská, Marián Lichnovský, Gerhard Loj, Ján Maukš, Ľudmila Miklenčičová, Katarína Ondrejková, Karol Pomšár, Daniel Procházka, Magda Rosiarová, Marta Rybová, Tatiana Spišiaková, Alexander Tóth