---
title: 1963/1964
---

Školský rok 1963 - 1964

3.A</br>
Triedny profesor: Božena Tadlánková</br>
Alexander Balogh, Eva Bednárová, Ladislav Černý, Yvonna Dillnbergerová, Marcela Drobilová, Katarína Gregorová, Marián Hodáň, Dušan Kabina, Miroslav Kajzr, Peter Keltoš, Alexandra Kirilinová, Ľubomír Kočiš, Ľubomír Kotzig, Zlatica Krajčovičová, Viera Kupkovičová, Marta Lesayová, Marika Menšíková, Viera Metzlová, Soňa Mičková, Vasil Nikolov, Vladimír Novotný, Dagmar Ondrušová, Jana Revendová, Mária Susková, Božena Sýkorová, Milan Šuster, Renáta Švehlíková, Tibor Tomanovič, Veronika Valášková, Cecília Veškrnová, Eleonóra Virsíková, Adriana Žilinková

3.B</br>
Triedna profesorka: Lenka Šnstrová</br>
Vladimír Balogh, Juraj Bečer, Jaroslav Divok, Ľubica Dobrová, Dagmar Dusíková, Marta Feketová, Katarína Gregorová, Jozef Hornák, Ladislav Hudáč, Marta Hudecová, Marta Kiretová, Eva Kňážeková, Jana Korenková, Jolana Králová, Nora Kupčová, Anna Lazoríková, Viera Lorinová, Karol Machánek, Soňa Majerská, Mária Miháliková, Božena Molnárová, Ladislav Müller, Jarmila Müllerová, Lívia Poncová, Mária Rauková, Rudolf Slabej, Viera Smažáková, Ľudovít Snopko, Stanislav Svítek, Dana Števurková, Milan Tatranský, Iva Uhríková, Juraj Vozár, Dagmar Zlatovská, Milan Žúži

3.C</br>
Triedna profesorka: Rozália Kotuličová</br>
Amon Baláž, Zlatica Balogová, Eva Blablová, Lýdia Blechová, Jana Bolková, Viera Cvoligová, Alžbeta Hanáková, Milan Faltinovič, Daniela Hanáčková, Gabriela Hamarová, Ján Chudík, Eva Jánošíkova, Jozef Jaroš, Pavol Jurčo, Jana Kocianová, Ružena Kotočová, Viera Kovačiková, Pavol Kovačovský, Helena Lichtneckerová, Viktor Masarovič, Jozef Maxián, Danica Medeková, Peter Mehr, Jana Miklovičová, Alojzia Nagyova, Gabriela Podhradská, Antónia Prekopová, Petronela Stenchláková, Darina Strieborná, Angela Šandriková, Oľga Šemeláková, Jarmila Šotolová, Alžbeta Stallingerová, Anna Thalmeinerová, Margita Tomášková, Magda Trnková, Marta Vojtíšková, Ivan Vulev

3.D</br>
Triedny profesor: František Mikulka</br>
Viera Berošová, Viera Cieslarová, Milan Drobný, Ivan Haverla, Jarmila Hrčová, Zuzana Hudcová, Zdeno Husička, Gejza Ivanič, Zlatica Jonášová, Eleonóra Klimešová, Dagmar Kočišová, Eva Kováčová, Peter Kšiňan, Eduard Marko, Ľudmila Matúšková, Štefan Medzihorský, Mária Pániková, Ľudmila Polónyiová, Klára Psotová, Mária Robotná, Zuzana Sklenková, Anna Sľúková, Jozef Stanislav, Maja Stanislavová, Milan Sušeň, Miroslav Šimko, František Štefko, Stanislava Švadlenová, Eva Tomašovičová, Ondrej Turek

3.E</br>
Triedny profesor: Jozef Balala</br>
Alena Batelková, Mišela Beranová, Milan Brtva, Anna Donovalová, Milan Dindoš, Soňa Jurkovičová, Eva Kollárová, Lýdia Kršáková, Igor Kubík, Oľga Kučerová, Dušan Kuklovský, Jozef Lipták, Štefan Ložek, Vladimír Mago, Gabriel Mikulec, Margita Molnárová, Tatiana Myjavcová, Anna Neubrandtová, Ľudovít Nosko, Anna Paukovičová, Darina Pudmerová, Marta Šarmírová, Ladislav Škotta, Peter Šubert, Anna Terlandová, Oľga Tóthová, Richard Vančo, Vladimír Wänke, Marián Žákovic, Nikolaj Žunko, Fedor Žužič

3.F</br>
Triedna profesorka: Emília Sitárová</br>
Anna Bartovičová, Oľga Dalmanegová, Anna Davidová, Darina Divková, Júlia Ďurčeková, Helena Hederová, Marta Hovorková, Anna Hrnčírová, Narcisa Jarošová, Vlasta Jašová, Eva Koláčkovská, Silvia Kotlárová, Blažena Kováčová, Vlasta Kováčová, Ľubica Lisá, Margita Ludvigová, Eva Mocová, Dana Mrázková, Anna Paulenová, Anna Pelikánová, Valéria Príkazská, Jana Režnáková, Ľubomíra Rybáková, Eva Slovincová, Marta Ščepánová, Zdena Zvolenská, Anna Zúbriková

3.G</br>
Triedny profesor: Július Šoltés</br>
Mária Alakšová, Marián Andrejkovič, Milan Béreš, Viktor Cigánek, Marián Čerňanský, Ján Gros, Peter Hanuš, Gabriel Hauptvolg, Ľubica Intribusová, Bohuš Ježík, Dušan Králik, Jaroslav Králik, Ján Krištofovič, Peter Lachký, František Lipka, Mikuláš Lipták, Eva Mateová, Milan Materák, Peter Molnár, Mária Pavčová, Branislav Pleskot, Anton Pouš, Milan Repiský, Marta Rosová, Štefan Rudzan