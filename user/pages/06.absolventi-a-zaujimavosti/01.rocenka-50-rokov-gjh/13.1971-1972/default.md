---
title: 1971/1972
---

Školský rok 1971 - 1972

3.A
Triedny profesor: Ľubomír Krištof
Jozef Adam, Peter Ardo, Eva Balajová, Mária Bencková, Dušan Bôrik, Miroslav Brutenič, Soňa Farská, Barbora Gömöryová, Miroslav Grieš, Zdeno Hala, Vladimír Halaša, Dušan Hájek, Zuzana Hubová, Peter Husák, Vladimír Ivan, Ivan Jarolín, Juraj Kováč, Dušan Laky, Mariana Leitmannová, Lada Longauerová, Alexander Machan, Peter Mariányi, Eleonóra Mitterpachová, Ján Móži, Jozef Mucha, Ferdinand Pál, Zdenka Peštuková, Ján Reguli, Stanislav Reguli, Eva Revúcka, Ľudovít Rohus, Zuzana Svetlíková, Ľubomíra Šaušová, Eva Škrovinová, Aurélia Škultétyová, Andrej Špánok, Igor Tomašovič, Vladimír Válok, Mária Veselá, Jozef Veselý, Mária Vírostková, Hana Valentová

3.B
Triedna profesorka: Kornélia Kropiláková
Miroslav Almer, Mikuláš Arendáš, Martin Dudák, Andrej Hanzel, Alexander Horváth, Igor Kľačanský, Daniel Klein, Juraj Kobza, Magdaléna Kosseyová, Jaroslav Košťál, Miluše Králiková, Milan Lehotský, Alena Macková, Ján Malovec, Michal Maťáš, Daniel Mažári, Ján Mráz, Ján Novák, Peter Panák, Ľubica Pastirčáková, Stanislav Pastor, Ján Pekár, Martin Peterich, Emanuel Petrík, Ľubomír Piják, Peter Pogády, Dušan Prcúch, Ivan Prcúch, Ľubomír Reháček, Marián Sakál, Mikuláš Sedlák, Jozef Skákala, Jozef Sojka, Mária Straková, Vladimír Sčepán, Peter Šefc, Július Šoltés, Vlastimil Šubr, Milan Šubrt, Ján Valášek, Mojmír Váross

3.C
Triedna profesorka: Anna Horáková
Milan Buc, Viera Drahošová, Michal Dzuriak, Jozef Fabián, Dušan Frint, Ľudmila Gajdošíková, Miroslav Galbavý, Pavel Grünner, Ján Hanuš, Pavol Hríň, Karol Illý, Eduard Jakubovie, Ladislav Jedenástik, Matej Jergel, Oľga Juraščíková, Lívia Kalmanová, Viliam Kalčok, František Kmiť, Anna Krpelanová, Peter Kružliak, Mária Kubrická, Peter Kukuča, Karol Moravčík, Imrich Moro, Jarmila Pašková, Katarína Slovákova, Pavol Spielböck, Emil Stračár, Dušan Stanislav, Oľga Šebestová, Vladimír Štrba, Štefan Šurka, Jana Tomaškovičová, Ľubor Tuček, Ivan Turský, Dora Ulrychová, Mária Urbanová, Ivan Vanca, Oľga Vodová, Boris Vrškový

3.G
Triedna profesorka: Oľga Heribanová
Ľudmila Arpášová, Katarína Bednárová, Terézia Brimichová, Mária Feketeová, Dragan Ferko, Hana Gábrišová, Alena Gajdošíková, Helena Horská, Mária Chovancová, Jarmila Kubišová, Marta Líšková, Marián Mikula, Anton Modrovič, Viera Mužíková, Jozefína Nováková, Kristína Offermannová, Katarína Podfajová, Oľga Potfajová, Eva Pupáková, Gabriela Puškášová, Soňa Reháková, Dušan Rehánek, Anna Smažáková, Jozef Šesták, Igor Šimúnek, Viera Štefunková, Soňa Suchančoková, Kristína Tarjanyiová, Katarína Tarová, Juraj Vajda, Magda Vozárová, Milan Zlocha

AK - nadstavbové štúdium so zameraním na programovanie a obsluhu počítacích strojov Triedny profesor: Jozef Balala
František Ács, Edita Adamkovičová, Ivan Barica, Alžbeta Belančíková, Imrich Bertók, Boris Boleček, Drahomíra Ciglánová, Eva Denková, Edita Drotárová, Ján Dužek, Štefánia Féderová, Viera Gbelcová, Jana Hořejší, Miroslav Höger, Anna Ivaničová, Marcela Janíčková, Ján Kaba, František Kolarovič, Danica Kolarovičová, Marián Lobotka, Anton Ludrovský, Ľudovít Magyarics,
Zlatica Malinová rod. Pistová, Vftazoslav Repáš, Blanka Rujbrová, Ján Szabó, Terézia Svobodníková, Alena Šimončičová, Ján Štekláč, Anna Takáscová, Milan Vojtech, Svetozár Lacko, Marián Crha, Peter Valtíny, Miroslav Samaš, Peter Bobro