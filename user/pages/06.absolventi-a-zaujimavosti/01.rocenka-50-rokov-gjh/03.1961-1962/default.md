---
title: 1961/1962
---

Školský rok 1961 - 1962

3.A</br>
Triedna profesorka: Helena Kotzigová</br>
Marta Balážová, Daniela Bartová, Alica Budincová, Eva Delejová, František Fančovič, Zora Filadelfiová, Peter Hatala, Marína Hexnerová, Slavomíra Janeková, Mariana Jelínková, Soňa Kášová, Tatiana Kazimírová, Peter Kinzel, Róbert Kitt, Tamara Levická, Peter Luby, Svetlana Mrlianová, Elena Ondrejkovičová, Katarína Pašková, Emília Repková, Valéria Rybaničová, Hana Skovajsová, Andrej Sabad, Marta Schneiderová, Jana Ševčíková, Jozef Špaček, Ľudmila Tvrdoňová, Anton Valia, Jason Žbirka, Jozef Žibritovský

3.B</br>
Triedna profesorka: Alica Beňová</br>
Bohumír Čapkovič, Alexandra Danková, Jozef Farfel, Viera Fejdiová, Slavomír Gavora, Daniela Grohmannová, Marta Hrdličková, Jarmila Chudíková, Alica Juldašová, Daniela Kardošová, Anna Kasanická, Lýdia Kozárová, Eva Kubincová, Aurélia Kunáková, Marta Labašová, Júlia Lázslová, Otto Máchal, Anna Malíková, Anna Malíková, Ada Marková, Anna Melichová, Vlasta Mikulášková, Marián Mišík, Boris Dabrinov Nikolov, Viera Paľovčíková, Mária Paluchová, Marianna Praeslerová, Rado Rässel, Zdena Štefíková, Alexander Thurza, Bohumír Tóth, Lýdia Vaňová, Veronika Véghová, Miroslav Zicha

3.C</br>
Triedna profesorka: Ema Peitlová</br>
Eva Blahová, Vladimír Buchholcer, Eva Čambalová, Viera Čatajová, Mária Drobilová, Tatiana Gregorová, Valéria Holecová, Kamil Jureník, Ján Juríček, Zuzana Kacorová, Eva Klimanová, Jarmila Kolesíková, Veronika Kopečná, Peter Kukumberg, Zlatica Menďan-Lakatošová, Helena Mihálková, Juraj Mihályka, Marián Michalov, Alena Mišíková, Ladislav Omelka, Viera Petrášová, Oľga Pilátová, Jaroslava Pochmanová, Magda Popálená, Anna Repčíková, Július Rozložník, Tatiana Stražanová, Peter Šmigáň, Karol Šrobár, Mikuláš Šumichrast, Dobroslav Šiška, Zdenka Tekelová, Eleonóra Tomasovišová, Peter Vičislík

3.D</br>
Triedna profesorka: Mária Selecká</br>
Anna Baková, Adriana Baranová, Margita Bencková, Oľga Bencová, Oľga Brežná, Gabriela Donerová, Stanislav Doranský, Marta Farkašová, Andrej Galvánek, Erika Hrušková, Milan Kotian, Oľga Krkošková, Margita Kupková, Tatiana Laskovičová, Peter Lesyk, Mária Michalíková, Jarmila Nováková, Peter Novotný, Mária Petríková, Peter Petříček, Jela Piatková, Zlatica Potemriová, Ľubomír Rehák, Viktor Rozložník, Hermína Selnekovičová, Mária Sirotová, Zdena Skalníková, Jaroslav Sloboda, Milan Srneček, Zdena Strnádková, Zuzana Suchá, Rudolf Tolnay, Daniela Ungerová, Juraj Vlachovský