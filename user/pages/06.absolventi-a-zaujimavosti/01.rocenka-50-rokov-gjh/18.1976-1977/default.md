---
title: 1976/1977
---

Školský rok 1976 - 1977

4.A</br>
Triedna profesorka: Elena Šimkovičová</br>
Roman Babák, Miroslav Baxa, Gabriela Bocková, Jitka Buriánková, Ľuboš Čulen, Boris Ďuriš, Mária Fialová, Jozef Harvan, Zuzana Hasáková, Mária Hasenöhrlová, Rozália Hatalová, Karol Hierweg, Juraj Hromkovič, Andrea Husárová, Viera Ivančíková, Alena Jentnerová, Ľudmila Krištofová, Ľubomír Kríž, Ľudovít Ľupták, Oľga Majerčíková, Soňa Michalcová, Soňa Obuchová, Kvetoslava Ondrejkovičová, Róbert Novan, Edita Pažitná, Kamil Pecho, Július Pelcner, Miroslav Přikryl, Miroslav Prokeš, Peter Rutkay, Eva Rybanská, Ladislav Skokan, Stanislav Stohl, Pavol Švantner, Lubomíl Tatara, Oľga Tiefenbacherová, Anna Voderková, Jozef Zigo

4.B</br>
Triedna profesorka: Elena Ivančíková</br>
Pavel Bizoň, Ľubica Černíková, Juraj Červeň, Boris Deák, Jozef Fekiač, Jozef Frlička, Štefan Gabriel, Beata Glosová, Terézia Gonová, Robert Handlovič, Milan Horniak, Vladimír Hrčka, Branislav Jozefíni, Dagmar Knapcová, Jana Kořinková, Ján Košúth, Peter Kováčik, Juraj Koza, Katarína Kozlíková, Ivan Krátky, Šarlota Krištofová, Milan Kučera, Roman Kukuča, Ľubica Lošáková, Miroslav Molitoris, Sviatoslav Molnár, Alena Mortingerová, Augustín Mrázik, Anna Otrubová, Jurja Papšo, Peter Pavlásek, Mária Pravotiaková, Erik Schvartz, Miroslav Šrámek, Stanislav Šuster, Zuzana Traindlová, Magda Trebatická

4.C</br>
Triedna profesorka: Viera Vavrová</br>
Eva Adzimová, Martin Barto, Ivan Beseda, Peter Bolf, Ján Bosý, Katarína Dobiášová, Alena Ďurčeková, Július Fraňo, Tomáš Gregor, Ivan Grund, Ján Hrivňák, Ivan Huraj, Ján Ilavský, Juraj llavský, Henrich Jakeš, Klaudia Jureníková, Jaroslav Kachlík, Pavol Kalina, Jozef Konc, Ivan Kováčik, Daniel Kuzmík, Ján Langfelder, Otto Pernecký, Michal Poláček, Igor Remža, Miroslav Skrúcaný, Juraj Šimkovic, Dušan Táborský, Vladimír Talaš, Iveta Trepáčová, Rudolf Velich, Andrej Zachar

4.D</br>
Triedna profesorka: Llbuše Kôstková</br>
Oľga Ághová, Ervin Barta, Kornélia Bartošová, Monika Čunderlíková, Jana Dedičová, Miloš Halmi, Viera Hanzlová, Eva Haringová, Kvetoslav Hečko, Mária Hupková, Slavomír Jakubek, Milan Jankovič, Jozef Jelenčiak, Miloš Karhánek, Tibor Klein, Jozef Kollár, Juraj Majtán, Dana Mišovičová, Vladimír Mosný, Valentína Nackinová, Ivo Novák, Marián Paulik, Milica Poliaková, Vladimír Porvan, Beáta Roľková, Branislava Šaturová, Eva Toldyová, Želmíra Vadovičová, Vladimír Váross

AK - nadstavbové štúdium so zameraním na programovanie a obsluhu počítacích strojov</br>
Triedna profesorka: Oľga Grmanová</br>
Tomáš Balla, Katarína Baranyaiová, Eva Belákova, Magdaléna Bubenková, Anna Budinská, Alžbeta Bugárová, Helena Csjerníková, Gabriel Czere, Zuzana Časárová, Viera Čuneková, Margita Dóková, Michal Doms, Anna Dudžáková, Mária Fülöpová, Eugen Gašparík, Tomáš Gavalčík, Eva Horňáková, Ľudovít Hruška, Eva Kekišová, Jozef Kmeť, Milada Kolláriková, Ľubomír Kosík, Peter Krajčír, Alena Krajčová, Anna Lukáčová, Marta Martinkovičová, Daniela Medvecká, Pavol Mišák, Mária Mondeková, Anna Mordáčová, Ivan Musil, Mariana Oravcová, Ondrej Pazdera, Alžbeta Petrová, Marián Porubský, Marta Puhalová, Juraj Schwartz, Jana Sulavová, Helena Szutyányiová, Alena Matiašková, Estera Tkáčiková, Eva Vanková