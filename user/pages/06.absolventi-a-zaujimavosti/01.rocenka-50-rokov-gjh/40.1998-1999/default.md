---
title: 1998/1999
---

Školský rok 1998 - 1999

4.A</br>
Triedna profesorka: Elena Zelenayová</br>
Branislav Belas, Ján Bielik, Peter Bluska, Martin Čeppan, Martin Dratva, Katarína Gáborová, Filip Hlubocký, Michal Hulena, Mária Klaučová, Ondrej Kliment, Martin Kočan, Alžbeta Krajčíková, Soňa Lajová, Hana Machová, Peter Mandl, Barbora Marčáková, Peter Mathes, Michal Meško, Martin Michalík, Erik Neuhold, Henrich Offermann, Tomáš Páleník, Juraj Pätoprstý, Andrea Pěková, Tomáš Pícha, Michal Poštrk, Oliver Profant, Zuzana Prónayová, Miroslav Roško, Zuzana Skákalová, Ivan Štefánik, Peter Štibrany, Michal Tóth, Miroslav Vajaš, Juraj Vaško, Andrej Vrábel, Jana Zelinková, Marek Zeman

4.B</br>
Triedny profesor: Vladimír Jodas, Marián Slušný</br>
Ivana Balážová, Lucia Bodnárová, Peter Brnčík, Anna Feriancová, Branislav Gajdoš, Jana Gajdošiková, Miroslav Hajach, Rastislav Hajach, Zuzana Holičová, Marek Hrabčák, Barbora Hrivnáková, Peter Kolesár, Milan Konečný, Vladimír Koutný, Richard Kráľovič, Zdenka Kratochvíľová, Jana Lachová, Ľubica Martauzová, Ivan Masaryk, Ivan Nemčovský, Juraj Olejník, Peter Onderčin, Dávid Pál, Barbora Paučová, Martin Potočný, Marián Ret, Rastislav Slavkovský, Juraj Sukop, Martin Širáň, Ján Široký, Tomáš Vavrák, Katarína Vrťová, Marek Wiesenganger, Andrea Zererová

4.C</br>
Triedna profesorka: Hana Mlynarčiková</br>
Tomáš Alaxin, Peter Bečár, Juraj Benetin, Alexandra Borsíková, Natália Deáková, Michaela Dugovičová, Miriana Feketová, Lukáš Fila, Anton Forró, Peter Halický, Nina Húsková, Martin Chlebo, Zuzana Janíková, Michal Józsa, Adrián Kočiš, Lenka Lenická, Michal Macák, Henrieta Mrázová, Matúš Németh, Martin Nosko, Igor Plávka, Zuzana Pompurová, Dušan Prepira, Jakub Richter, Martin Rublík, Róbert Rybár, Vladimír Schmidt, Boris Smažák, Diana Šajdová, Zuzana Švitelová, Alena Vančíková, Matúš Tarjanyi

4.D</br>
Triedna profesorka: Zuzana Mináriková</br>
Tibor Bajzík, Hana Balážová, Juraj Bezručka, Ondrej Binder, Róbert Bokor, Tomáš Bujňák, Katarína Ciglanová, František Čech, Martin Čema, Marek Doršic, Zdenka Fajkusová, Tomáš Gála, Peter Herc, Martin Hrúzik, Stanislav Kadáš, Daniel Kováč, Igor Kúdeľa, Jozef Kutej, Tomáš Kvasnica, Marián Marcinčák, Martin Maťaš, Ján Matejko, Matej Mišík, Miriam Nehajová, Martin Pernecký, Tomáš Potfaj, Barbora Praščáková, Vladimír Ravinger, Martin Rendoš, Tomáš Rusňák, Barbora Stankovičová, Zlatica Šubrová, Matej Varga, Lucia Vozárová

4.IB</br>
Triedna profesorka: Ivana Pichaničová</br>
Andrea Číková, Alexander Čopák, Peter Daubner, Dana Fajmonová, Jozef Figur, Marián Formánko, Barbora Gábelová, Michal Hanula, Iveta Havranová, Jana Hrivnáková, Zora Kátlovská, (Peter Kozák), Alexander Krištofčák, Jana Krištofová, Daniela Lavčáková, Boris Lupták, Matej Maceáš, Marek Maďar, Terézia Macháčová, Katarína Machálková, Ivana Moravčíková, Lucia Najšlová, Peter Ostrica, Michal Osuský, Lukáš Polakovič, Andrea Purdeková, Paul Martin Putora, Zuzana Slosarčíková, Andrea Slováková, Zuzana Vojteková