---
title: 1974/1975
---

Školský rok 1974 - 1975

4.A</br>
Triedna profesorka: Anna Akácsová</br>
Viera Balážová, Martin Bertók, Nataša Běliková, Eva Bojarová, Peter Bouška, Igor Braxatoris, Ľubomír Cibák, Emília Farkašová, Stanislav Fialík, Emília Grossová, Vladimír Horváth, Tatiana Kalašová, Jana Kilianová, Anna Klajbanová, Adriana Kohoutová, Ľudovít Kovačovský, Margita Kováčová, Eva Kovárová, Dana Krajčovičová, Jozef Králik, Beáta Kszelová, Ľubor Lazár, Ivan Lepey, Vladimír Mašan, Katarína Mešterová, Zuzana Osuská, Peter Sedílek, Vladimír Straka, Peter Štefko, Silvia Taldová, Viliam Vanák, Iveta Volčková, Luděk Vrtík, Jozef Vyskoč, Dušan Šindelár

4.B</br>
Triedny profesor: Ján Faber</br>
Helena Bielková, Martin Blaškovič, Monika Človiečková, Peter Farkaš, Rudolf Hudec, Ivan Janetka, Ján Janovič, Tibor Jávor, Milan Kiaček, Dušan Kolesár, Peter Krajči, Ján Krajčík, Peter Krchnák, Svetozár Krno, Vladimír Kubiš, Ján Kučera, Vladimír Laco, Juraj Lopatník, Mária Makulová, Karol Martinka, Dušan Miklánek, Branislav Molitoris, Ingrid Moravcová, Boris Pietrzyk, Oto Polačko, Dana Popelková, Peter Roško, Ján Slodička, Július Šréter, Jozef Uherko, Ľubomír Uhnák, Martin Vojtko-Kubanda, Andrej Záhorčák

4.C</br>
Triedna profesorka: Elena Šimkovičová</br>
Oliver Arpáš, Andrej Belica, Jozef Bezák, Jozef Binder, Ondrej Červený, Miroslav Čislák, Jana Demianová, Helena Ferenčíková, Ivan Filadelfi, Jana Geržová, Ivan Halanda, Václav Haluza, Juraj Hric, Eva Hrivnáková, Karol Hučko, Miroslav Husák, Dagmar Jamrichová, Elena Jureníková, Radomil Kachlík, Ľudovít Kalina, Dalibor Klima, Oľga Malachovská, Ján Malík, Ivan Mikula, Peter Mikuš, Gabriela Mosná, Eva Pastuchová, Ľubica Paulíková, Dušan Polónyi, Ivan Plencner, Jela Rusnáková, Ján Serátor, Igor Sobocký, Karol Stračár, Ján Sýkora, Stanislav Šándor, Miroslav Uhrík, Nadežda Vorobjovová

4.D</br>
Triedna profesorka: Edita Borutová</br>
Halina Bartfayová, Jiří Bartoš, Milota Beňová, Elena Čeppanová, Katarína Drugová, Magdaléna Engelmannová, Pavol Gregor, Mária Grenčíková, Ján Hájek, Ľubomír Haraksim, Mária Horáková, Magdaléna Jentnerová, Ľubomír Jirásek, Michal Kandráč, Natália Karlinská, Eva Keresztessyová, Jozef Klenovič, Pavol Kóňa, Anton Korauš, Elena Kosibová, Viera Kozová, Ľubomír Kráľ, Ľubomír Krivý, Peter Lučenič, Dana Masaryková, Klaudia Mišúthová, Martin Mosný, Branislav Mucha, Helena Nováková, Pavol Paroulek, Elena Prachařová, Katarína Prúnyiová, Alena Reicherová, Tomáš Richter, Alexander Rusko, Viera Rýznarová, Vladimír Synek, Igor Škarpišek, Dionýz Štroncer, Milan Vacval, Milota Vašková, Sylvia Višňovcová, Libor Zendulka

4.E</br>
Triedna profesorka: Oľga Chovanová</br>
Ján Baláž, Anna Baľová, Stanislav Bella, Juraj Bielik, Jarmila Boršová, Zuzana Brteková, Igor Calpaš, Hilda Danišová, Dagmar Diererová, Peter Drobný, Viera Dudášová, Milan Fiedler, Jozef Greguš, Dušan Halmi, Karol Husár, Elena Hovančíková, Katarína Jeleneková, Juraj Jonáš, Ján Juck, Ľubor Kabina, Valéria Némethyová, Eva Ondrejková, Viera Papíková, Iveta Parkányová, Mária Pavlíková, Oľga Piatrová, Šarlota Porubčanová, Dana Rádyová, Dana Slávikova, Soňa Svíteková, Ľubica Šimkovicová, Jana Široká, Luboslava Trúsiková, Veronika Vestenická, Peter Winter

4.F</br>
Triedny profesor: Juraj Pribula</br>
Emil Barlok, Eva Buchtová, Peter Ciesar, Daniela Dávidková, Soňa Dedičová, Libuša Dočkalová, Viera Dvoráková, Irena Fonodová, Kveta Furáková, Eva Gazárková, Elena Gerthoferová, Peter Greppel, Peter Gróf, Jelica Gruntová, Darina Hagarová, Ivana Havlíčková, Pavol Kmeč, Ivan Košalko, Dana Križánková, Lubor Kuracina, Milica Lauková, Darina Lörincová, Ľubomír Lubina, Viliam Magula, Eva Markova, Katarína Maršálková, Karol Mičko, Martin Miklánek, Oľga Molnárová, Miloš Novák, Mária Obuchová, Miroslav Ondráček, Vladimír Ožvolda, Alica Poschová, Ivan Považanec, Andrea Reimannová, Marián Sapák, Rudolfína Soboličová, Jana Šándorová, Olívia Švolíková, Veronika Valentová, Alena Zdvíhalová

4.G</br>
Triedny profesor: Jaroslav Švach</br>
Barbora Alexyová, Edita Belešová, Dana Berkyová, Jana Bordáková, Otília Csölleiová, Kristína Čápová, Imrich Danko, Ľubomír Drobný, Mária Dubíková, Dagmar Fabiánová, Eva Hlaváčová, Viera Homolková, Vladimír Horváth, Katarína Hradská, Jana Chrenková, Dana Janušková, Jana Kalmanová, Zlatica Kollárová, Viera Koleničová, Alica Kubániová, Zuzana Lepulicová, Štefan Litomerický, Miroslava Lubojacká, Renáta Martišová, Viera Mrvová, Mária Palušová, Gabriela Poláková, Beáta Pravotiaková, Oľga Pukalovičová, Imrich Rutkovský, Jozef Satina, Víťazoslav Sedlák, Milan Slezák, Eva Stobodová, Oľga Solánková, Monika Sýkorová, Ľubica Szabová, Izabela Šipošová, Blanka Široká, Katarína Veselová, Želmíra Zelenáková, Michal Zeman

AK - nadstavbové štúdium so zameraním na programovanie a obsluhu počítacích strojov</br>
Triedny profesor: Ondrej Demáček</br>
Elena Alexovitšová, Anna Bednaričová, Vincencia Bizoňová, Veronika Boháčova, Marián Čimo, Vincent Domonkos, Antónia Dzuríková, Karol Gajarský, Ivan Gaňor, Mária Heimlichová, Alena Jungmayerová, Ľudmila Karenovičová, Elena Kramaričová, Pavol Komadel, Mária Anna Kuchta, Jozef Kučeravý, Anna Legáthová, Juraj Lipka, Alena Lukáčová, Alexandra Martiniková, Pavel Maňásek, Arnold Novák, Terézia Potašová, Ján Sliacky, Silvia Schlosserová, Jana Slezáková, Ružena Szakóova, Jolana Szazová, Mária Ševčíková, Lýdia Škoríková, Ondrej Števka, Elizabetha Šujanská, Magdaléna Takácsová, Pavol Tereni, Eva Tóthová, Jolana Vajdová, Jozef Veselký, Mária Vodná, Oľga Zlochová