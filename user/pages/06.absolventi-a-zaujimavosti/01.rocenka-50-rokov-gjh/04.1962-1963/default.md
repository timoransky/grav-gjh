---
title: 1962/1963
---

Školský rok 1962 - 1963

3.A</br>
Triedny profesor: Karol Rovan</br>
Ivan Albert, Pavol Baroš, Ladislav Batik, Elemer Berlin, Ivan Čadený, Marián Dollinger, Jaroslav Filip, Eva Franeková, Oľga Gajdošová, Eva Grébertová, Štefan Heuger, Vladimír Horov, Jozefína Hrdlovičová, Viera Jančušková, Ľubica Javorská, Pavol Jelenek, Marta Kapustová, Elena Košová, Anna Kovačovičová, Mikuláš Kováčik, Gabriela Krmářová, Miloslav Luther, Anna Masaryková, Daniela Mihoková, Ľubica Pázmanová, Oľga Píscová, Alena Srbecká, Norbert Strak, Katarína Szoldová, Elena Šebová, Eva Takácsová, Michal Uher, Oľga Valášková, Imrich Végh, Vladimír Virsík

3.B</br>
Triedny profesor: Fedor Palevič</br>
Svetlana Bahnová, Marta Beňovičová, Táňa Borguľová, Ľuba Brindzová, Lida Donnebaumová, Marta Ďuržová, Ivan Frýdecký, Mária Glasnáková, Kata Hajdušíková, Dana Horská, Priska Huttnerová, Jarmila Jankelová, Eva Kianičková, Eva Konštiaková, Ivan Koritšan, Táňa Kovačevičová, Brigita Krajčovičová, Milan Labuda, Marcela Marková, Viera Mojžišova, Marta Nádašiová, Anna Neubauerová, Ján Pánik, Ivan Piroško, Oskar Pišoft, Želmíra Pražienková, Miroslav Rákovský, Ivan Skalinský, Jozef Stankovič, Viera Šabíková, Ivan Šesták, Gabriel Špendla, Milan Tomašovič, Oľga Ursínyová, Ivan Vesel, Ladislav Vlachovský

3.C</br>
Triedna profesorka: Zora Pašková</br>
Eva Bachníčková, Štefan Bellus, Mária Bogdanová, Edita Feilerová, Miroslava Feldsamová, Tibor Fiala, Zuzana Hanulová, Ján Hoffman, Eva Holecová, Július Chlebovič, Eleonóra Chvalná, Juraj Jellinek, Miroslav Kajzr, Dušan Kern, Darina Klagová, Jaroslav Klíč, Peter Koleda, Ivan Kopas, Peter Kunštek, Alžbeta Labudová, Darina Lauková, Vojtech Mucha, Milan Nosko, Judita Nová, Margita Pódová, Drahomír Purgina, Soňa Reháková, Mária Slováčková, Eva Szmicseková, Igor Studňář, Tibor Tomanovič, Martina Turzáková, Valéria Weberová, Alexej Zlocha, Eva Žákovičová

3.D</br>
Triedna profesorka: Elena Weberová</br>
Eva Bašťovanská, Soňa Belanová, Peter Božek, Lida Buttková, Jozef Gazdík, Kamila Haulišová, Marta Holúbková, Zuzana Horáková, Jaroslav Jelenek, Štefan Klimo, Eva Kočí, Ján Lazár, Lýdia Letovancová, Jozef Malovec, Gusta Michaličková, Gabriela Molnárová, Otto Nopp, Eva Paulíková, Bohumil Pichler, Vladimír Piontek, Eva Podhorová, Miloš Randák, Viera Richterová, Zuzana Spevárová, Pavel Šiška, Ľubomír Šumichrast, Helena Tejbusová, Alena Uhrová, Gustáv Ušiak, Václav Viták, Júlia Volentičová, Eva Voskárová, Erika Zapletalová

3.E</br>
Triedny profesor: Július Šoltés</br>
Zdena Baranovičová, Mária Bašťovanská, Mária Bezáková, Stanislav Černák, Viera Čorbová, František Dubecký, Emília Dráčková, Vlado Ďurman, Milan Geguš, Jozef Kállay, Ivan Katriak, Mária Kelcová, Eva Kollárova, Elena Korbelová, Soňa Králiková, Milina Levková, Kamila Lukáčová, Hermína Měkutová, Bohuš Oplt, Ivan Ozábal, Vlado Pohánka, Adela Sabová, Oľga Sádovská, Ján Schulz, Ľuba Stojková, Zora Štyndlová, Pavol Takáč, Eliška Tichá, Viera Tomeková, Marika Uhrínová, Dana Vážna, Hela Vojčeková, Viera Vojtaššáková, Mária Zábojníková

3.F</br>
Triedna profesorka: Ema Haraksimová</br>
Ľubica Bugárová, Fedor Bzdúch, Katarína Čemická, Peter Černáček, Vladimír Dohňanský, Pavel Dráč, Oľga Flőglová, Peter Gregor, Eva Haimová, Štefan Horváth, Roman Hudec, Martin Chovan, Zora Ivaničová, Anna Jakubovičová, Ľubica Kočvarová, Martin Kostolný, Jozef Laclav, Pavel Lesný, Oľga Marleová, Soňa Miklová, Viola Molnárová, Štefánia Pálfiová, Štefan Raninec, Mária Sandtnerová, Marta Strauszová, Martin Šebesta, Miroslav Štrba, Elena Valentová, Marta Velgosová, Peter Zajac, Zdeno Zajac, Alena Zuzánková

3.G</br>
Triedna profesorka: Margita Vlchová</br>
Milan Andrejko, Kamila Baníková, Milan Botka, Andrej Brogyányi, Viera Bystrianska, Helena Cenká, Eva Červeňanská, Dagmar Danišová, Jozef Ehn, Milan Erban, Zdena Figurová, Ján Gazdík, Mária Gombkőtőová, Elena Grešáková, Zlatica Győrffyová, Karol Holomáň, Miroslav Hysko, Peter Karaffa, Eleonóra Klimešová, Mária Kimleová, Tomáš Kraus, Rozáli Lebedynská, Pavel Lizoň, Peter Magdolen, Zuzana Mandáková, Jana Michálková, Oľga Mirilovičová, Soňa Ondreášová, Mária Sabová, Eva Spišáková, Katarína Sýkorová, Tamara Šermerová, Taťjana Škrabáková, Jozef Trenčanský, Margita Vadovičová, Ondrej Valek, Elena Vysocká