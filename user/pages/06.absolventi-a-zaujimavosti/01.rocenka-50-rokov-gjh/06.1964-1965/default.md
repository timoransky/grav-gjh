---
title: 1964/1965
---

Školský rok 1964 - 1965

3.A</br>
Triedna profesorka: Edita Bořutová</br>
Alžbeta Barešová, Viera Cichrová, Pavel Dub, Katarína Ďurčová, Mária Gráčiková, Milada Halamíčková, Anna Hamáčeková, Marta Havelková, Viola Hlaváčová, Vladimír Hlavenka, Marián Kadlečík, Božena Kantorová, Eva Krabáčová, Peter Lampart, Andrej Litavec, Michal Lörincz, Dušan Malina, Peter Mocko, Emília Mikócziová, Kristína Neubrandtová, Zdeňka Nováková, Jana Olšová, Jana Ondrišová, Dušan Ondrkal, Dana Ozábalová, Zuzana Pavolová, Jozefína Rakická, Darina Samborová, Ján Slovák, Alžbeta Syrová, Mária Špendlová, Albert Török, Magda Valentovičová, Marta Valentovičová, Viera Valková, Ivica Valkovcová, Viera Vaňková, Peter Váš, Jana Zábojníková, Zdena Zednikovičová, Dagmar Zimmerová

3.B</br>
Triedna profesorka: Elena Šimkovičová</br>
Angela Abrahámová, Danka Bajerová, Ida Blaškovičová, Igor Bock, Želmíra Bottová, Ján Bočák, Viera Cíbiková, Andrej Danielis, Jarmila Fišerová, Judita Fuchsová, Jozef Gbelský, Jana Habarová, Katarína Heretiková, Valéria Hillerová, Vladimír Húska, Veronika Hrková, Eva Jaklová, Eva Koupanová, Viera Kubínska, Marta Labudová, Daniela Labušová, Dušan Luchava, Soňa Lenoráková, Danuša Luknišová, Zuzana Maláriková, Yvonna Orosová, Marián Palla, Zuzana Pántiková, Peter Pipan, Barbara Plichtová, Zlatica Potančoková, Margita Rovanová, Mária Šafárikova, Jana Šimeková, Milan Valášek, Dušan Vrana, Cecília Zvonárova, Yvetta Žuchová

3.C</br>
Triedna profesorka: Anna Gavorová</br>
Zuzana Bakošová, Renée Bernátová, Viera Brucháčová, Viera Bučková, Gabriela Bujnová, Zuzana Cigáňova, Marianna Danielisová, Ľubica Dedeková, Yvetta Ejemová, Ivan Fabo, Klára Gabčová, Taťjana Gajdošová, Katarína Grébertová, Tomáš Guttman, Natália Ivančová, Katarína Jariabková, Dagmar Jaššová, Miriam Jónová, Anna Karbonyiková, Alena Keltošová, Silvia Knorrová, Klaudia Kolláriková, Marta Kollárová, Ľubomíra Koršepová, Melánia Krajčovičová, Monika Kunová, Brigita Linderová, Eva Lišková, Eva Lošonská, Taťjana Lukáčová, Sylva Lukášová, Zuzana Mezníková, Angela Nemčíková, Marta Ripperová, Peter Róbert, Boris Sobolovský, Jana Stryhalová, Peter Vlachovský, Eva Zvončeková

3.D</br>
Triedna profesorka: Zuzana Šimkovicová</br>
Daniela Borová, Taťjana Bušinská, Jana Buntová, Jaroslav Gažík, Karol Gróh, Milan Grosser, Jozef Horňáček, Stanislav Hrda, Richard Kanya, Pavol Kedro, Mária Kosibová, Iveta Kosová, Iveta Kováčová, Igor Križan, Pavol Lajda, Viera Livorová, Vlasta Lošáková, Tamara Marcisová, Milan Mlynár, Augustín Paulík, Boris Plachý, Branislav Rovan, Július Sedláček, Július Selecký, Oľga Slabihoudová, Oľga Sówková, Eduard Szittay, Beáta Šimková, Bohumil Šťastný, Milan Šulkovský, Roman Torma, Rastislav Vilím

3.E</br>
Triedna profesorka: Kornélia Kropiláková</br>
Miroslav Andreánsky, Peter Bauer, Daniela Blablová, Juraj Böhm, Ľudovít Daniš, Dušan Deák, Peter Demjén, Peter Haško, Gabriel Hrankovič, Anna Horváthová, Eleonóra Hyrossová, Ľudmila Chmelová, Dušan Jamrich, Ján Karnas, Martin Klein, Oľga Kozlíková, Vladimír Kuhn, Daniela Langová, Miroslava Lorenzová, Mária Lubuškyová, Anna Mistríková, Marta Müllerová, Dušan Nemec, Ľudovít Paškovič, Jaroslav Ríha, Jana Rošková, Sylvia Svitková, Ľudmila Synková, Vratislav Schwarz, Marián Šupka, Desana Tatarková, Viliam Thuringer, Darina Tordajiová, Peter Valach, Anna Verešová, Eva Wollnerová, Alena Zábojníková, Alžbeta Zajánošová, Vladimír Zukal

3.F</br>
Triedna profesorka: Alica Beňová</br>
Rozália Árpová, Daniela Babulicová, Eva Bjeľová, Vladimír Bořík, Igor Dráč, Helena Drgonová, Anna Ďurišová, Jozef Fedeleš, Alexander Foit, Viera Fröhlichová, Viera Huťová, Božena Jančárová, Drahuša Janeková, Ľubomír Jurík, Jaromír Kašparec, Zdena Kľúčiková, Silvia Kočí, Eva Korbeľová, Vladimír Kosnáč, Ružena Krpeľanová, Peter Kučera, Marián Kulka, Lívia Liptáková, Igor Lukáč, Marta Maarová, Alžbeta Malacká, Milan Mazák, Jaroslav Podolák, Juraj Považan, Alena Rothová, Marián Slovák, Pavol Sojka, Alexander Stahl, Irena Sulmová, Jana Vališová

3.G</br>
Triedna profesorka: Helena Kotzigová</br>
Helena Blahová, Milan Frič, Mária Grácová, Miloš Grandtner, Vilma Grünwaldová, Mária Gubriczová, Ladislav Hlobeň, Daniel Janota, Zuzana Karpišová, Mária Kičková, Marta Konečná, Marta Krajčovičová, Soňa Kusá, Eleonóra Ľachová, Elena Maholanyiová, Peter Malach, Oľga Mikušková, Igor Nopp, Lýdia Ondrušková, Jarmila Ormandyová, Eva Petrovská, Eva Prekopová, Anna Prihelová, Emília Svitková, Lýdia Svobodová, Richard Šediba, Helena Šipošová, Darina Švedová, Iwar Treskoň, Milan Turza, Dušan Valihora, Vlasta Vallnerová, Eva Vašková, Oľga Vidová, Darina Vontorčíková, František Žák, Miroslav Žiak, Ľudmila Živicová

3.H</br>
Triedna profesorka: Oľga Ottingerová</br>
Jaroslav Alexejenko, Eva Bernátová, Ľudmila Betákova, Jana Bistáková, Miroslav Borguľa, Nora Harkabusová, Peter Hlavačka, Anna Holécyová, Viera Hupková, Elena Chmelová, Kristína Chudíková, Ján Jankela, Dana Kántorová, Angela Kleinová, Mária Kraslová, Daro Malo, Sandra Kuboňová, Viliam Kundlák, Eva Labudová, Milan Májek, Eva Majerová, Anna Matychová, Gizela Miháliková, Miroslav Nemec, Jana Omelková, Jana Páleniková, Jaroslav Pantoflíček, Dušan Piontek, Mária Rosenbergerová, Soňa Slouková, Alica Suchá, Angela Šandríková, Peter Telek, Peter Uličný, Valerián Vadovič, Helena Vaňurová, Pavel Vlček, Anna Varhaníková, Júlia Zajacová, Peter Zaťka, Jaroslav Ždán

3.Š</br>
Triedny profesor: Július Šoltés</br>
Gabriela Andrejková, Ľubica Bodecká, Elena Čárska, Ján Čižmárik, Jozef Dravecký, Ján Gal, František Goliaš, Ján Janík, Marcela Kiaciova, Viera Krňanová, Ladislav Magdolen, Antónia Mimránková, Karol Mitura, Ján Mydliar, Ján Novotný, Milan Petrovský, Jozefína Rončáková, Štefan Šujan, Ľudmila Václavíková, Jozef Vančík, Branislav Zágoršek
AK - Nadstavbové štúdium pre absolventov SVŠ so zameraním na programovanie a obsluhu počítacích strojov Tr. profesor: Zdeno Vlachynský,Anna Bartalská, Rozina Böhmerová, Elena Erhardtová, Ladislav Fodor, Pavol Fukasz, Róbert Hölcz, Gustáv Choreň, Jozef Klein, Oľga Ličková, Juraj Maďar, Juraj Nováček, Anna Petrášová, Ján Profota, Mária Švábiková, Margita Uhrová, Ondrej Vallo, Viera Žiklová