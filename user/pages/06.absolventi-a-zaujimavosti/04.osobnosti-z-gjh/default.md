---
title: 'Osobnosti z GJH'
---

### Významné osobnosti medzi absolventmi GJH

**Politici:** Ján Čarnogurský, Brigita Schmögnerová, Peter Weiss, Peter Zajac, Rudolf Zajac, Pavol Kubovič, Vladimír Bajan, Štefan Hudec, Ján Počiatek , Ján Kubiš, Pavol Frešo...

**Vysokoškolskí pedagógovia a vedci:** František Gahér, Branislav Rovan, Pavol Zlatoš, Ivan Kalaš - vedúci Katedry základov a vyučovania informatiky na FMFI UK ( na tejto fakulte pracuje ďalších najmenej 25 absolventov GJH), Vladimír Repáš, Pavol Černek, Vladimír Černý, Ladislav Mišík, Karol Šafařík, Jiří Šafařík, Marián Slodička (profesor na univerzite v Gente), Ivan Mizera (profesor na univerzite v Alberte)

**Umelci:** Dušan Jamrich, Zuzana Krónerová, Boris Filan, Pavol Hammel, Juraj Nvota, Vladimír Bartoň, Zuzana Mauréry, Zuzana Cigáňová...

**Členovia kapiel:**
Para (Tomáš Šedivý alias Lasky, Matej Kubicár, Daniel Buzinkay, Juraj Marikovič)
Sako (Juraj Marikovič, Martin Marikovič, Peter Hrabe)
Longital (Daniel Salontay)
Hex (Peter Dudák)

**Podnikatelia a manažéri:** Peter Prónay, Peter Babinec, Viktor Birnstein, Róbert Šimončič, Martin Drobný, Ivan Šramko, Martin Barto

**Športovci:** bratia Vencelovci – reprezentanti vo futbale, tenistka Daniela Hantuchová , Ján Markoš – majster Slovenska v  šachu, Samuel Grečner - Biketrial, Igor Prieložný - volejbal, reprezentant, olympionik, tréner