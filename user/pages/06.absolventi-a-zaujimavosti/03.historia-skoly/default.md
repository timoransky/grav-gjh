---
title: 'História školy'
process:
    markdown: true
    twig: false
cache_enable: true
visible: true
---

### Významné medzníky v histórii školy

1.9.1959 - Otvorenie 1. školského roka na Jedenásťročnej strednej škole /JSŠ/ na Novohradskej ul. Škola vznikla zo zrušených JSŠ na Kulíškovej a Jelenej ul. a JSŠ v Trnávke. Riaditeľom sa stal pán Juraj Švarc.

1.1.1961 - Nadobudol platnosť zákon z 15.12.1960, ktorým sa zrušili JSŠ, povinná školská dochádzka sa predĺžila o 1 rok, vznikli dvanásťročné stredné školy, ktoré sa postupne rozpadali na školy I. cyklu - základné deväťročné školy /ZDŠ/ a školy II. cyklu - stredné všeobecnovzdelávacie školy /SVŠ/.

1.9.1962 - Odchodom 1.-9. ročníka z Novohradskej vznikla ZDŠ Košická a zlúčením vyšších ročníkov s vyššími ročníkmi dvanásťročnej strednej školy P. Jilemnického na Palisádoch a dvanásťročnej strednej školy na ul. Červenej armády vznikla SVŠ Novohradská. Riaditeľom sa stal PhDr. Zdeněk Obdržálek, CSc. 

1963/64 - V jednej triede 1. ročníka /1.A/ sa začalo vyučovanie podľa špeciálneho učebného plánu so zameraním na numerickú matematiku a obsluhu počítacích strojov. Praktické vyučovanie prebiehalo vo výpočtových strediskách niekoľkých bratislavských podnikov a na Vysokej škole ekonomickej. V priebehu školského roku 1963/64 bola na škole zriadená strojnovýpočtová stanica, predvoj neskoršieho školského výpočtového laboratória. Triedy s rovnakým zameraním sa otvárali aj v nasledujúcich školských rokoch. Postupne sa z nich vyvinuli triedy so zameraním na programovanie. 

1964/65 - Otvorila sa špeciálna trieda tretieho ročníka s celoslovenskou pôsobnosťou / III.Š / pre žiakov nadaných na matematiku a fyziku. Aj v ďalších dvoch školských rokoch sa otvorila III.Š. Na škole vznikol abiturientský kurz / jednoročné nadstavbové štúdium /, špecializované na obsluhu počítacích strojov. Jedna trieda nadstavbového štúdia sa otvárala každoročne až do šk r. 1981/82 vrátane. Absolventi tohto štúdia získali úplné stredné odborné vzdelanie. Uplatňovali sa najmä ako operátori vo výpočtových strediskách. SVŠ Novohradská sa stala bázovou školou Prírodovedeckej a Filozofickej fakulty UK. 

1967/68 - Otvorila sa trieda prvého ročníka /1.B/ pre žiakov nadaných na matematiku a fyziku , v ktorej vyučujúci využívali svoje skúsenosti z práce v špeciálnych triedach /III.Š/. Tu sa teda založila tradícia B-čok, tried žiakov s hlbším záujmom o matematiku a fyziku, v ktorej pokračujeme dodnes s výnimkou niekoľkých školských rokov. Žiaci práve týchto tried a tried zameraných na programovanie každoročne úspešne reprezentovali a dodnes reprezentujú školu v matematických, fyzikálnych a programátorských súťažiach. 

Otvorila sa taktiež trieda /1.G/ s celoslovenskou pôsobnosťou s rozšíreným vyučovaním latinčiny a gréčtiny. Triedy s vyučovaním klasických jazykov sa otvorili aj v nasledujúcich dvoch školských rokoch. 

1969/70 - Došlo k ďalšej zmene školskej sústavy - vzniká gymnaziálny typ stredoškolského štúdia, SVŠ Novohradská sa mení na Gymnázium Jura Hronca. V priebehu troch rokov postupne zanikajú triedy SVŠ a sú nahradené štvorročnými gymnaziálnymi triedami. Do kolekcie tried GJH pribúdajú triedy zo žiakov ZDŠ Košická, ktoré pokračujú podľa experimentálneho učebného plánu pokus v matematike matematického ústavu československej akadémie vied v Prahe. 

1970/71 - Riaditeľa Obdržálka, ktorý odišiel na nové pôsobisko na vysokú školu, vo funkcii vymenil RNDr. Víťazoslav Repáš. Hoci riaditeľoval v čase normalizácie, podarilo sa mu pracovnú atmosféru školy, ktorú po svojom predchodcovi zdedil, obohatiť o vtedy vzácny liberálny duch. 

1975/76 - V systéme tried sa zvýšil počet tried so zameraním na programovanie. V nasledujúcich školských rokoch až dodnes sa základy programovania vyučujú aj v neprogramátorských triedach. 

1979/80 - Do inventáru školy pribudol počítač ADT, ktorý uľahčil vyučovanie programovania. 

V jednej triede prvého ročníka sa začalo vyučovať podľa experimentálneho učebného plánu gymnázia. Experiment v priebehu nasledujúcich štyroch rokov intenzívne sledovalo MŠ SR. 

Uskutočnil sa prvý ročník súťaže o putovný pohár Jura Hronca, ktorej organizátorom bola naša škola spolu s JSMF. 

1982/83 - Škola získala štatút výpočtového laboratória, ktoré sa obohatilo o novú výpočtovú techniku. Do laboratória nastúpili noví nepedagogickí pracovníci. Vo februári 1983 odišiel riaditeľ Repáš do dôchodku a do funkcie riaditeľa nastúpil RNDr. Marián Hanula. 

Nasleduje obdobie tvrdej a obetavej práce na zveľaďovaní GJH - ako materiálneho vybavenia, tak i obsahu výchovno-vzdelávacej práce. Učebné osnovy prechádzajú zmenami, ktoré si kladú za cieľ nájsť optimálny prístup ku žiakom a ich vzdelávaniu. V minulosti sa často stávalo, že ani dva roky za sebou sa vo vyučovaní v jednotlivých triedach nepostupovalo podľa rovnakých učebných plánov. 

1989/90 - Zmena politických pomerov priniesla pre školu väčšie možnosti samostatného rozhodovania. Vďaka bývalým vedeniam školy a celému učiteľskému kolektívu nás novo sa otvárajúce spoločensko-politické pomery nezastihli nepripravených. 

Voľby riaditeľa školy, ktoré sa uskutočnili začiatkom roku 1990, priniesli zmenu vo vedení školy - riaditeľom sa stal RNDr. Vladimír Jodas a jeho zástupom doterajší riaditeľ RNDr. Marián Hanula. 

1990/91 - Na základe výsledkov, ktoré naše gymnázium v oblasti výchovy a vzdelávania dosahovalo, zaradilo MŠ SR našu školu medzi gymnáziá podieľajúce sa na experimente s tzv. voľným učebným plánom. Vedenie školy týmto dostalo právomoc navrhnúť po prerokovaní s pedagogickou radou školy, zástupcami rodičov a žiakov učebný plán platný pre Gymnázium Jura Hronca. Pri zostavovaní učebného plánu sme brali do úvahy pozitíva i negatíva plánov pre zameranie na programovanie a zameranie na matematiku a fyziku, podľa ktorých sme dovtedy vyučovali. Ich najväčší nedostatok spočíval v tom, že nerešpektovali prirodzený vývoj profesijného zamerania žiakov. Všetci žiaci museli absolvovať zvýšený počet vyučovacích hodín matematiky, fyziky resp. programovania a všetci museli aspoň z jedného z týchto predmetov maturovať. Študenti, ktorí v priebehu štúdia zmenili soje záujmy /budúci medici, právnici, žurnalisti, filozofi atď./, nemali možnosť intenzívnejšie sa vzdelávať v predmetoch, ktoré boli pre ich budúce vysokoškolské štúdium profilové. Pre žiakov študujúcich na škole od 1.9.1990 sme zostavili preto učebný plán so spoločným základom /aj výuky programovania/ pre všetkých žiakov, ale nechali sme priestor pre voliteľné predmety, najmä v treťom a štvrtom ročníku. 

Podľa tohoto plánu, ktorý sme postupne vylepšovali a aktualizovali v súvislosti so zavedením povinného vyučovania etickej alebo náboženskej výchovy a pridaním ďalšej hodiny dejepisu v treťom a štvrtom ročníku, vyučujeme ešte v terajších tretích ročníkoch. V júni 1998 MŠ SR zrušilo našu výnimku, dalo škole na výber organizovať vyučovanie alebo podľa všeobecného učebného plánu, alebo podľa tzv. alternatívneho učebného plánu. Terajší prváci a druháci preto študujú podľa všeobecného učebného plánu, v ktorom rozširujúce hodiny v druhom ročníku využívame na informatiku. Oproti nášmu učebnému plánu majú o dve hodiny informatiky menej a v štvrtom ročníku iba štyri, namiesto piatich voliteľných predmetov. 

1992/93 - MŠaV SR vypísalo konkurz na IB školu (gymnázium, na ktorom sa v jednej triede bude vyučovať podľa medzinárodného programu International Baccalaureate). Riaditeľ Jodas, ako jeden zo štyroch odvážnych riaditeľov, školu do konkurzu prihlásil. regionálna riaditeľka IB organizácie Monika Flodman sa po rozhovoroch s vedeniami a učiteľmi prihlásených škôl rozhodla pre nás s podmienkou, že do roka zriadime na škole knižnicu so študovňou a dovybavujeme laboratóriá biológie, chémie a fyziky. 

17.5.1993 riaditeľstvo GJH, Rada školy pri GJH a Združenie rodičov pri GJH zriaďujú účelový fond - nadáciu s názvom Novohradská. Prvým správcom nadácie sa stal RNDr. Vladimír Jodas. Programovým cieľom nadácie je financovanie a materiálne zabezpečenie výchovno-vzdelávacieho procesu na GJH, starostlivosť o ďalší odborný rast učiteľov, podpora všestranného rozvoja vzdelávacích, kultúrnych a športových aktivít žiakov, s prihliadnutím na rozvoj talentov a na podporu sociálne slabších žiakov 

1993/94 - Z prostriedkov nadácie a Phare sme zariaďovali školu, aby sa na nej dal realizovať IB program. 

Otvorila sa prvá prima osemročného gymnázia so zameraním na informatiku a programovanie. Žiaci prvé štyri roky študujú na ZŠ a Gymnáziu Košická, v kvinte až oktáve pokračujú u nás na GJH. 

Založili sme tradíciu korešpondenčných seminárov pre žiakov končiacich ročníkov základných škôl, v ktorej s výnimkou školského roka 1996/97 pokračujeme dodnes. Úspešných riešiteľov domácich kôl pozývame na záverečné školské kolo súťaže. Najúspešnejším riešiteľom školského kola ponúkame prijatie na GJH bez prijímacej skúšky. 

1994/95 - Z poverenia MŠaV SR sa otvorila IB trieda s celosvetovou pôsobnosťou pre žiakov posledných dvoch predmaturitných ročníkov. Prvým koordinátorom IB programu sa stal RNDr. Daniel Salontay. Vďaka jeho mimoriadnemu úsiliu IB program vynikajúco odštartoval. V náročných IB skúškach, ktoré sú externe hodnotené v zahraničí, naši študenti dosahujú dodnes vynikajúce výsledky. 

1995/96 - Po rezignácii riaditeľa Jodasa do funkcie riaditeľky školy od 1.7.1995 Školská správa BA VI menovala /na základe výberového konania/ RNDr. Zuzanu Munkovú. 

Prvý júlový týždeň sme na vyzvanie IB organizácie úspešne zorganizovali na škole IB workshop, na ktorom sa zúčastnili IB učitelia z celého sveta. 

1996/97 - Inšpirovaní IB programom sme vypracovali Program rozvoja osobnosti žiaka GJH, ktorého súčasťou je medzitriedna súťaž O putovný džbán Jura Hronca. Žiaci sa zúčastňujú množstva dielčích súťaží a získavajú tak body pre svoje triedy. Súťaž každoročne vyhodnocujeme na záver šk. roka na Športovom dni školy, ktorý je vyvrcholením celoročných žiackych aktivít. 

1998/99 - Spolu s Gymnáziom v Piešťanoch, taktiež v minulosti zameraným na programovanie, sme vypracovali návrh učebného plánu so zameraním na informatiku a požiadali sme MŠ SR o jeho schválenie. Naše aktivity sa v období realizácie vládneho programu INFOVEK stretli na MŠ SR s porozumením. 

1999/2000 - 17.12.1999 sa v dome lodníkov uskutočnila slávnostná recepcia pri príležitosti 40. výročia GJH, na ktorej sa zúčastnili súčasní i bývalí učitelia školy a okolo 50 absolventov, dnes úspešných osobností v oblasti vedy, kultúry, politiky atď. 

13.1.2000 - MŠ SR schválilo učebný plán študijného zamerania 7902 5 05 informatika pre gymnáziá so štvorročným štúdiom s účinnosťou od 1.9.2000 počínajúc 1. ročníkom. Hneď sme požiadali o zaradenie nášho gymnázia do siete škôl s týmto učebným plánom a od 1.9.2000 začneme podľa neho vyučovať v dvoch triedach 1. ročníka. Učebný plán vznikol ako kompromis medzi v súčasnosti platným všeobecným učebným plánom a úzko špecializovaným zastaralým učebným plánom pre zameranie na programovanie. Popri zvýšenej dotácii hodín informatiky umožní žiakovi hlbšie štúdium ďalších štyroch predmetov podľa vlastného uváženia. 

1.1.2005 Zanikajú ZŠ a G Košická a GJH ako samostatné právne subjekty, vzniká Spojená škola na Novohradskej ulici – právny subjekt, ktorého organizačnými zložkami sa stali pôvodné školy Počas rozsiahlej rekonštrukcie v lete 2005 sa aj budova školy stavebne prepojila: na druhom a treťom poschodí vznikli spojovacie chodby a riaditeľkou Spojenej školy sa stáva Zuzana Munková. 

2005/2006 Vznik medzinárodných programov PYP a MYP pre mladšie vekové kategórie 

2008/2009 Do platnosti vstúpil nový školský zákon 245/2009 Z.z. o výchove a vzdelávaní Všetky tri medzinárodné programy sa v tomto zákone považujú za ekvivalentné k vzdelávaniu v národnom programe, IB maturita je ekvivalentná maturite podľa tohto zákona. V prvom ročníku ZŠ, piatom ročníku ZŠ a prvom ročníku štvorročného aj osemročného štúdia na GJH sa začína vyučovať podľa nových školských vzdelávacích programov. Prebehla tiež úspešná autorizácia programov PYP a MYP. 

30.11.2009 - Oslava 50. výročia založenia oboch škôl v činohernej sále Slovenského národného divadla 
